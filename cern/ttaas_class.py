# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os
import inspect
import sys
import datetime
import io
import ntpath
import shutil
from json import dumps, loads
from urllib.parse import quote
from datetime import date, datetime
import ast

from cern.opencast_class import Opencast
from .rest_functions import get_call_server 

import hashlib
import hmac
import time
import requests
try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode

class TtaaS:
    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        self.headers=['Accept: application/v1.1.0+json']

        ttaas_url = config.get('TtaaS', 'ttaas_url')
        ttaas_bearer = config.get('TtaaS', 'ttaas_bearer')
        user = ''
        passwd = ''

        self.server_config = {}
        self.server_config['server_url'] = ttaas_url
        self.server_config['authorization_bearer'] = ttaas_bearer
        self.server_config['user'] = user 
        self.server_config['passwd'] = passwd

        self.opencast_object = Opencast(config_file)

    def ingest_event(self, media, params, headers='yes'):
        endpoint = 'api/public/v1/uploads/ingest/'
        url = "%s/%s"%(self.server_config['server_url'],endpoint)
        form_data = ast.literal_eval(params)
        #print(form_data)
        #print(type(form_data))

        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        #print(headers)
        #return get_call_server(self.server_config, endpoint, 
        #                    headers=headers, params='', 
        #                    json=True, digest=False, verbose=True)
        #print (media)
        #print (url)
        with open(media, "rb") as f:
            files=dict(
                mediaFile=f
            )
            r = requests.post(url=url, files=files, data=form_data, headers=headers)
            #print(r.status_code, r.content)
            #print(r.request.url)
            #print(r.request.body)
            #print(r.request.headers)
        error_msg = 'Return code: %d' % (r.status_code)
        if (r.status_code=='502'):
            return {'value' : 'Ingestion error', 'error' : True, 'error-msg' : 'Bad gateway'}
        elif (r.status_code!=201):
            return {'value' : 'Ingestion error', 'error' : True, 'error-msg' : error_msg, 'http-code': error_msg}
        else:
            return {'value' : r.content, 'error' : False, 'error-msg' : ''}
    def update_event(self, id, params, headers='yes'):
        endpoint = 'api/public/v1/uploads/'
        url = "%s/%s/%s"%(self.server_config['server_url'],endpoint,id)
        form_data = ast.literal_eval(params)
        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        r = requests.patch(url=url, data=form_data, headers=headers)
        error_msg = 'Return code: %d' % (r.status_code)
        if (r.status_code=='502'):
            return {'value' : 'Update error', 'error' : True, 'error-msg' : 'Bad gateway'}
        elif (r.status_code!=200):
            return {'value' : 'Update error', 'error' : True, 'error-msg' : error_msg, 'http-code': error_msg}
        else:
            return {'value' : r.content, 'error' : False, 'error-msg' : ''}
    def request_translation(self, id, headers='yes'):
        endpoint = 'api/public/v1/uploads/translation/'
        url = "%s/%s/%s"%(self.server_config['server_url'],endpoint,id)
        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        r = requests.patch(url=url, headers=headers)
        error_msg = 'Return code: %d' % (r.status_code)
        if (r.status_code=='502'):
            return {'value' : 'Update error', 'error' : True, 'error-msg' : 'Bad gateway'}
        elif (r.status_code!=200):
            return {'value' : 'Update error', 'error' : True, 'error-msg' : error_msg, 'http-code': error_msg}
        else:
            return {'value' : r.content, 'error' : False, 'error-msg' : ''}            
    def get_event(self, id, headers='yes'):
        endpoint = 'api/public/v1/media-files/%s/'%(id)
        url = "%s/%s"%(self.server_config['server_url'],endpoint)
        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        r = requests.get(url=url, headers=headers)
        return {'value' : r.content, 'error' : False, 'error-msg' : ''}

    def check_event(self, id, headers='yes'):
        endpoint = 'api/public/v1/uploads/%s/'%(id)
        url = "%s/%s"%(self.server_config['server_url'],endpoint)
        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        r = requests.get(url=url, headers=headers)
        return {'value' : r.content, 'error' : False, 'error-msg' : ''}

    def set_state(self, indico_id, state, headers='yes'):
        #state = [notify|ready-for-cutting|cutting-finished|update-acl-finished|processing-finished]
        endpoint = 'api/v1/opencast/%s?contribution_id=%s' % (state,quote(indico_id))        
        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params='', 
                            json=True, digest=False, verbose=True)