from io import StringIO as bio
from io import BytesIO
from json import dumps
from json import loads
from urllib.parse import urlencode

import requests
from requests.auth import HTTPDigestAuth
from requests.auth import HTTPBasicAuth

from pprint import pprint

_TIMEOUT_REST_FUNCTIONS = 20
cafile = '/etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt'

def get_call_server(conf, endpoint, headers=[], params=[], json=False, digest=True, verbose=False):
    port = ''
    if 'port' in conf:
        port = ':%s' % (conf['port'])
    full_url = conf['server_url'] + port + endpoint

    user = conf['user']
    passwd = conf['passwd']
    
    if json:
        return get_call_json(full_url, headers, params, user, passwd, digest, verbose)
    else:
        return get_call(full_url, headers,  params, user, passwd, digest, verbose)

def get_call_json(full_url, headers=[], params=[], user=None, passwd=None, digest=True, verbose=False):
    response = get_call(full_url, headers, params, user, passwd, digest, verbose)

    if response['error'] == True:
        return response

    value = {}
    try:
        value = loads(response['value'])

    except ValueError:
        return {'value' : '', 'error' : True, 'error-msg' : 'Could not decode json.'} 

    return {'value' : value, 'error' : False, 'error-msg' : 'Ok'}

def post_call_json(full_url, headers=[], params=[], user=None, passwd=None, digest=True, verbose=False):
    response = post_call(full_url, headers, params, user, passwd, digest, verbose)

    if response['error'] == True:
        return response

    value = {}
    try:
        value = loads(response['value'])

    except ValueError:
        return {'value' : '', 'error' : True, 'error-msg' : 'Could not decode json.'} 

    return {'value' : value, 'error' : False, 'error-msg' : 'Ok'}

def put_call_json_request(full_url, data, headers=[], params=[], user=None, passwd=None, digest=True, verbose=False):

    if (len(params) > 0):
        full_url = full_url + "?" + urlencode(params)

    json_data = dumps(data)

    try:
      headers = {}
      if user and passwd:
        if digest:
            auth=HTTPDigestAuth(user, passwd)
            headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Digest', 'Content-Length' : str(len(json_data))}
        else:
            auth=HTTPBasicAuth(user, passwd)
            headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Basic','Content-Length' : str(len(json_data))}
      #if verbose:
      #      print(verbose)      
      response = requests.put(full_url,data=data,headers=headers,auth=auth)

      error_msg = 'Return code: %d' % (response.status_code)
      if (response.status_code!=204):
        return {'value' : response.text, 'error' : True, 'error-msg' : error_msg, 'http-code': error_msg}
      else:
         return {'value' : 'acl updated', 'error' : False, 'error-msg' : error_msg, 'http-code': error_msg}	
    except ValueError:
        return {'value' : '', 'error' : True, 'error-msg' :str(ValueError)}

def get_call(full_url, headers=[], params=[], user=None, passwd=None, digest=True, verbose=False):
    if (len(params) > 0):
        full_url = full_url + "?" + urlencode(params)
    try:
    
      if user and passwd:
        if digest:
            headers = {}
            auth=HTTPDigestAuth(user, passwd)
            headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Digest'}
        else:
            headers = {}
            auth=HTTPBasicAuth(user, passwd)
            headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Basic'}

        response = requests.get(full_url,headers=headers,auth=auth, verify=False)
      else:          
        response = requests.get(full_url,headers=headers, verify=False)

      #print(response.text)
      error_msg = 'Return code: %d' % (response.status_code)
      if (response.status_code!=200):
        return {'value' : '', 'error' : True, 'error-msg' : error_msg, 'http-code': error_msg}
      else:
        return {'value' : response.text, 'error' : False, 'error-msg' : error_msg, 'http-code': error_msg}	
    except ValueError:
      return {'value' : '', 'error' : True, 'error-msg' :str(ValueError)}

def post_call(full_url, headers=[], params=[], user=None, passwd=None, digest=True, verbose=False):

    try:
      if user and passwd:
        if digest:
            headers = {}
            auth=HTTPDigestAuth(user, passwd)
            headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Digest'}
        else:
            headers = {}
            auth=HTTPBasicAuth(user, passwd)
            headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Basic'}

        response = requests.post(full_url,headers=headers,json=params, auth=auth, verify=False)
      else:          
        response = requests.post(full_url,headers=headers, json=params, verify=False)

      #print(response.text)
      error_msg = 'Return code: %d' % (response.status_code)
      if (response.status_code!=200 and response.status_code!=201):
        return {'value' : '', 'error' : True, 'error-msg' : error_msg, 'http-code': error_msg}
      else:
        return {'value' : response.text, 'error' : False, 'error-msg' : error_msg, 'http-code': error_msg}	
    except ValueError:
      return {'value' : '', 'error' : True, 'error-msg' :str(ValueError)}

def get_list_items(rest_method, params, parse_list_items, parse_list_args):
    items = []
    finished = False
    offset = 0
    limit = 100

    while (finished is False):
        # create params
        call_params = [('offset', offset), ('limit', limit)]
        call_params.extend(params)

        response = rest_method(call_params)
        if response['error'] == True:
            return response

        value = {}
        try:
            value = loads(response['value'])
        except ValueError:
            return {'value' : '', 'error' : True, 'error-msg' : 'Could not decode json.'}

        total, new_items = parse_list_items(value, *parse_list_args)
        items.extend(new_items)

        if total == -1 and len(new_items) < limit:
            finished = True
        elif total != -1 and  len(items) >= total:
            finished = True
        else:
            offset = offset + limit

    return {'value' : items, 'error' : False, 'error-msg' : ''}

# POST calls

def post_call_server(conf, endpoint, headers=[], params=[], json=True, digest=True, verbose=False):
    port = ''
    if 'port' in conf:
        port = ':%s' % (conf['port'])
    full_url = conf['server_url'] + port + endpoint
    user = conf['user']
    passwd = conf['passwd']
    #print(full_url)
    if json:
        return post_call_json(full_url, headers, params, user, passwd, digest, verbose)
    else:
        return post_call(full_url, headers, params,  user, passwd, digest, verbose)


# PUT calls

def put_call_server(conf, endpoint, data, headers=[], params=[], json=True, digest=True, verbose=False):
    port = ''
    if 'port' in conf:
        port = ':%s' % (conf['port'])
    full_url = conf['server_url'] + port + endpoint
    user = conf['user']
    passwd = conf['passwd']
    
    if json:
        return put_call_json_request(full_url, data, headers, params, user, passwd, digest, verbose)
    else:
        return put_call(full_url, data, headers, params,  user, passwd, digest, verbose)


def download_file(full_url, headers=[], filepath=None, user=None, passwd=None, digest=True, verbose=False):    
    try:
      headers = {}
      if user and passwd:
        if digest:
            auth=HTTPDigestAuth(user, passwd)
            headers = {'X-Requested-Auth': 'Digest','Content-Type': 'charset=UTF-8'}
        else:
            auth=HTTPBasicAuth(user, passwd)
            headers = {'X-Requested-Auth': 'Basic','Content-Type': 'charset=UTF-8'}

      response = requests.get(full_url,headers=headers,auth=auth, stream=True, verify=cafile)
      with open(filepath,"wb") as obj: 
            for chunk in response.iter_content(chunk_size=1024): 
                if chunk: 
                    obj.write(chunk) 
    except:
      return {'value' : '', 'error' : True, 'error-msg' :'Error downloading file from Opencast Server:'+full_url}
    return {'value' : 'ok', 'error' : False, 'error-msg' : '', 'http-code': 'retcode'}
    
def upload_file(full_url, headers=[], filepath=None, user=None, passwd=None, digest=True, verbose=False):    
    try:
        headers = {}
        #form_data = dict(
        #    flavor="presenter/source"
        #)
        # headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Digest', 'Content-Length' : str(len(json_data))}
        if user and passwd:
            print(filepath)
            with open(filepath, 'rb') as f:
                print("started upload of video file")
                files=dict(
                    BODY=f
                )
                if digest:
                    auth=HTTPDigestAuth(user, passwd)
                    headers = {'X-Requested-Auth': 'Digest','Content-Type': 'application/x-www-form-urlencoded'}
                else:
                    auth=HTTPBasicAuth(user, passwd)
                    headers = {'X-Requested-Auth': 'Basic','Content-Type': 'application/x-www-form-urlencoded'}

                response = requests.put(full_url,headers=headers,auth=auth, files=files, verify=cafile)
                error_msg = 'Return code: %d' % (response.status_code)
                if (response.status_code!=200):
                    return {'value' : '', 'error' : True, 'error-msg' : error_msg, 'http-code': error_msg}
                else:
                    return {'value' : response.text, 'error' : False, 'error-msg' : error_msg, 'http-code': error_msg}	
    except:
      return {'value' : '', 'error' : True, 'error-msg' :'Error uploading file to Server:'+full_url}
    return {'value' : 'ok', 'error' : False, 'error-msg' : '', 'http-code': 'retcode'}