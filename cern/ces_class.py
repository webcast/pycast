# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os
import inspect
import sys
import datetime
import io
import ntpath
import shutil
from json import dumps
from urllib.parse import quote
from datetime import date, datetime

from cern.opencast_class import Opencast
from .rest_functions import get_call_server 

import hashlib
import hmac
import time
import requests
try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode

class CES:
    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        self.headers=['Accept: application/v1.1.0+json']

        ces_url = config.get('CES', 'ces_url')
        ces_bearer = config.get('CES', 'ces_bearer')
        user = ''
        passwd = ''

        self.server_config = {}
        self.server_config['server_url'] = ces_url
        self.server_config['authorization_bearer'] = ces_bearer
        self.server_config['user'] = user 
        self.server_config['passwd'] = passwd

        self.opencast_object = Opencast(config_file)


    #https://ces-rene.web.cern.ch/api/v1/indico/acl?contribution_id=949975c1sc2
    def get_acl(self, indico_id, headers='yes'):
        endpoint = 'api/v1/indico/acl?contribution_id=%s' % (quote(indico_id))
        #print("%s%s"%(self.server_config['server_url'],endpoint))
        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params='', 
                            json=True, digest=False, verbose=True)

    def set_state(self, indico_id, state, headers='yes'):
        #state = [notify|ready-for-cutting|cutting-finished|update-acl-finished|processing-finished]
        endpoint = 'api/v1/opencast/%s?contribution_id=%s' % (state,quote(indico_id))        
        if headers:
            auth="Bearer %s"%(self.server_config['authorization_bearer'])
            headers={'Authorization': auth}
        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params='', 
                            json=True, digest=False, verbose=True)
