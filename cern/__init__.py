# -*- coding: utf-8 -*-
'''
    python-opencast-tools
    ~~~~~~~~~~~~~~~~~~~~~

    :copyright: 2020, Migue Angel Valero <mvaleron@cern.ch>
    :license: MIT – see LICENSE.txt for details.
'''

__all__ = [ "libtlp" ]