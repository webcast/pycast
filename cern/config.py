# -*- coding: utf-8 -*-
'''
Default configuration for PyCast.
'''

import configobj
import logging
import logging.handlers
import os
import socket
import sys
from validate import Validator
from configparser import RawConfigParser

logger = logging.getLogger(__name__)

__CFG = '''
#CERN Python Scripts Config File
[Logging]
path = logger.yaml
hostname = string(default='ocworker04-test2.cern.ch')

[Opencast]
server_admin_url= string(default='https://ocweb01-test2.cern.ch/')
server_engage_url = string(default='https://ocweb01-test2.cern.ch/')
user            = string(default='opencast_system_account')
passwd          = string(default='opencast_system_account_pass_key')
download_folder = string(default='/mnt/opencast_share/opencast_data/downloads/mh_default_org/engage-player/')
archived_folder = string(default='/mnt/opencast_share/opencast_data/archive/mh_default_org/')
insecure        = boolean(default=False)
certificate     = string(default='')
#port           = integer(min=0, default=8080)
flavors         = force_list(default=list('presenter/source'))
auth_method     = option('basic', 'digest', default='digest')

[Media]
org_dir = string(default='/mnt/import_share')
dest_dir = string(default='/mnt/media_share/media_data')
template_dir = string(default='/etc/pycast//templates')
apps = apo:0000:1976-2019 bakony:2020:2020-2025

[Indico]
server_url      = string(default='https://indico.cern.ch')
user            = string(default='webcast')
passwd          = string(default='CHANGE_ME')
path            = string(default='/mnt/opencast_downloads/')
org_dir         = string(default='/mnt/opencast_downloads')
dest_dir        = string(default='/mnt/media_share/media_data')
conf_dir        = string(default='/mnt/media_share/media_data/config')
master_dir      = string(default='/mnt/master_share/master_data')
template_dir    = string(default='/etc/pycast/templates')
api_key         = string(default='035064dd-73dd-47cb-9961-08eb8eb28588')
api_secret      = string(default='035064dd-73dd-47cb-9961-08eb8eb28588')

[Oid]
secret_opencast = string(default='035064dd-73dd-47cb-9961-08eb8eb28588')
secret_0000 = string(default='035064dd-73dd-47cb-9961-08eb8eb28588')
secret_2020 = string(default='035064dd-73dd-47cb-9961-08eb8eb28588')
secret_2021 = string(default='035064dd-73dd-47cb-9961-08eb8eb28588')
secret_2022 = string(default='035064dd-73dd-47cb-9961-08eb8eb28588')

[OpencastInternal]
server_admin_url = string(default='http://ocweb01-test2.cern.ch:8080')
server_engage_url = string(default='http://ocweb01-test2.cern.ch:8080')
user = string(default='opencast_system_account')
passwd = string(default='opencast_system_account_pass_key')

[CES]
ces_url   = string(default='http://ces-rene.web.cern.ch/')
ces_bearer= string(default='bearer')
user = string(default='admin')
passwd = string(default='CHANGE_ME')


[MLLP]
mllp_url   = string(default='https://ttp.mllp.upv.es/api/v3/')
user       = string(default='cern')
api_key    = string(default='CHANGE_ME')

[PyCA]
hostname = string(default='ocworker04-test2.cern.ch')
port = string(default='8000')
user = string(default='webcast')
passwd = string(default='CHANGE_ME')

[Mail]
server = string(default='smtp.cern.ch')
user = string(default='')
password =string(default='')
subject_tag = string(default='Opencast')
sender = string(default='Opencast <opencast@cern.ch>')
others_receivers = string(default='["Weblectures support" <weblectures-support@cern.ch>"]')

[logging]
syslog           = boolean(default=False)
stderr           = boolean(default=True)
file             = string(default='pycast-ocweb01-test2.cern.ch.log')
level            = option('debug', 'info', 'warning', 'error', default='info')
format           = string(default='[%(name)s:%(lineno)s:%(funcName)s()] [%(levelname)s] %(message)s')

'''  # noqa

cfgspec = __CFG.split('\n')

__config = None


def configuration_file(cfgfile):
    '''Find the best match for the configuration file.
    '''
    # Check the file existence
    if cfgfile is not None:
        if os.path.isfile(cfgfile):
            return cfgfile
    # If no file is explicitely specified or it doesn't exist, probe for the configuration file
    # location.
    cfg = '../pycast.cfg'
    if not os.path.isfile(cfg):
        return '/etc/pycast/pycast.cfg'
    return cfg


def update_configuration(cfgfile=None):
    '''Update configuration from file.

    :param cfgfile: Configuration file to load.
    '''
    configobj.DEFAULT_INTERPOLATION = 'template'
    cfgfile = configuration_file(cfgfile)
    cfg = configobj.ConfigObj(cfgfile, configspec=cfgspec, encoding='utf-8')
    validator = Validator()
    val = cfg.validate(validator)
    if val is not True:
        raise ValueError('Invalid configuration: %s' % val)
    if len(cfg['capture']['files']) != len(cfg['capture']['flavors']):
        raise ValueError('List of files and flavors do not match')
    if not cfg['Logging']['hostname']:
        cfg['Logging']['hostname'] = 'pycast@' + socket.gethostname()
    globals()['__config'] = cfg
    logger_init()
    if cfg['Opencast'].get('server_admin_url', '').endswith('/'):
        logger.warning('Base URL ends with /. This is most likely a '
                       'configuration error. The URL should contain nothing '
                       'of the service paths.')
    logger.info('Configuration loaded from %s', cfgfile)
    check()
    return cfg


def check():
    '''Check configuration for sanity.
    '''
    if config('Opencast')['insecure']:
        logger.warning('HTTPS CHECKS ARE TURNED OFF. A SECURE CONNECTION IS '
                       'NOT GUARANTEED')
    if config('Opencast')['certificate']:
        # Ensure certificate exists and is readable
        open(config('Opencast')['certificate'], 'rb').close()
    if config('Opencast')['port']:
        logger.info('Default port changed to : config(Opencast)[port]')


def config(*args):
    '''Get a specific configuration value or the whole configuration, loading
    the configuration file if it was not before.

    :param key: optional configuration key to return
    :type key: string
    :return: Part of the configuration object containing the configuration
             or configuration value.
             If a part of the configuration object (e.g. configobj.Section) is
             returned, it can be treated like a dictionary.
             Returning None, if the configuration value is not found.
    '''
    cfg = __config or update_configuration()
    for key in args:
        if cfg is None:
            return
        cfg = cfg.get(key)
    return cfg


def logger_init():
    '''Initialize logger based on configuration
    '''
    handlers = []
    logconf = config('logging')
    if logconf['syslog']:
        handlers.append(logging.handlers.SysLogHandler(address='/dev/log'))
    if logconf['stderr']:
        handlers.append(logging.StreamHandler(sys.stderr))
    if logconf['file']:
        handlers.append(logging.handlers.WatchedFileHandler(logconf['file']))
    for handler in handlers:
        handler.setFormatter(logging.Formatter(logconf['format']))
        logging.root.addHandler(handler)

    logging.root.setLevel(logconf['level'].upper())
    logger.info('Log level set to %s', logconf['level'])

def getApp(config_file, year, type='app'):
    # read logging configuration file path
    config = RawConfigParser()
    config.read(config_file)
    apps =  config.get('Media', 'apps').split() # apo:0000:1976-2019 bakony:2020:2020-2025
    # default values
    oid_app = '2021'
    balancer = 'montblanc' # Oprional param. Not needed
    for app in apps:
        min=app.split(':')[2].split('-')[0]
        max=app.split(':')[2].split('-')[1]
        if (year>=min and year<=max):
            balancer=app.split(':')[0]
            oid_app=app.split(':')[1]
    if type=='app':
        return(oid_app)
    else:
        return(balancer)

def getBalancer(config_file, year):
    return 'https://ocmedia-'+getApp(config_file,year,'balancer')+'.cern.ch/'+year