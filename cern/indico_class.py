# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os
import inspect
import sys
import datetime
import io
import ntpath
import shutil
import json
from urllib.parse import quote
from datetime import date, datetime, timedelta

from cern.opencast_class import Opencast
from .rest_functions import get_call_server, post_call_server
from .config import getApp, getBalancer

import hashlib
import hmac
import time
import re

import subprocess
import shlex

try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode

from xml.dom import minidom
import xml.etree.ElementTree as ET
from jinja2 import Template, Environment, FileSystemLoader


class Indico:
    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        filepath = config.get("Indico", "path")
        server_url = config.get("Indico", "server_url")
        template_dir = config.get("Indico", "template_dir")
        master_dir = config.get("Indico", "master_dir")
        dest_dir = config.get("Media", "dest_dir")
        master_path = config.get("DFS", "master_path")
        public_path = config.get("DFS", "public_path")
        legacy_dirs = config.get("Media", "legacy_dirs")
        user = ""
        passwd = ""

        api_key = config.get("Indico", "api_key")
        api_secret = config.get("Indico", "api_secret")
        api_token = config.get("Indico", "api_token")
        self.server_config = {}
        self.server_config["filepath"] = filepath
        self.server_config["api_key"] = api_key
        self.server_config["api_secret"] = api_secret
        self.server_config["api_token"] = api_token
        self.server_config["server_url"] = server_url
        self.server_config["template_dir"] = template_dir
        self.server_config["user"] = user
        self.server_config["passwd"] = passwd
        self.server_config["media_url"] = ""
        self.server_config["dest_dir"] = dest_dir
        self.server_config["master_dir"] = master_dir
        self.server_config["master_path"] = master_path
        self.server_config["public_path"] = public_path
        self.server_config["legacy_dirs"] = legacy_dirs
        self.opencast_object = Opencast(config_file)

        template_dir = config.get("Indico", "template_dir")

        self.config = {}
        self.config["template_dir"] = template_dir
        self.config["server_apo"] = "https://ocmedia-apo.cern.ch"
        self.config["server_bakony"] = "https://ocmedia-bakony.cern.ch"

    def __build_indico_request(
        self,
        path,
        params,
        api_key=None,
        secret_key=None,
        only_public=False,
        persistent=False,
    ):
        items = list(params.items()) if hasattr(params, "items") else list(params)
        if api_key:
            items.append(("apikey", api_key))
        if only_public:
            items.append(("onlypublic", "yes"))
        if secret_key:
            if not persistent:
                items.append(("timestamp", str(int(time.time()))))
            items = sorted(items, key=lambda x: x[0].lower())
            # print(items)
            url = "%s?%s" % (path, urlencode(items))
            signature = hmac.new(
                secret_key.encode("utf-8"), url.encode("utf-8"), hashlib.sha1
            ).hexdigest()
            items.append(("signature", signature))
        if not items:
            return path
        return "%s?%s" % (path, urlencode(items))

    def __ensure_dir(self, dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    def __get_video_metadata(self, pathToInputVideo):
        cmd = "ffprobe -v quiet -print_format json -show_streams"
        args = shlex.split(cmd)
        args.append(pathToInputVideo)
        # run the ffprobe process, decode stdout into utf-8 & convert to JSON
        ffprobeOutput = subprocess.check_output(args).decode("utf-8")
        ffprobeOutput = json.loads(ffprobeOutput)

        # prints all the metadata available:
        # import pprint
        # pp = pprint.PrettyPrinter(indent=2)
        # pp.pprint(ffprobeOutput)

        # for example, find height and width
        length = ffprobeOutput["streams"][0]["duration"]
        try:
            width = ffprobeOutput["streams"][0]["width"]
        except:
            try:
                width = ffprobeOutput["streams"][1]["width"]
            except:
                width = None

        try:
            height = ffprobeOutput["streams"][0]["height"]
        except:
            try:
                height = ffprobeOutput["streams"][1]["height"]
            except:
                height = None

        return round(float(length)), width, height

    # methods
    def get_metadata(self, event, metadatatype, data):
        try:
            event_id = event["identifier"]
            # print(event_id)
            # print(metadatatype+":"+data)
            value = None
            if "metadata" in event:
                for metadata in event["metadata"]:
                    if metadata["flavor"] == metadatatype + "/episode":
                        # print(metadata['flavor'])
                        if "fields" in metadata:
                            for field in metadata["fields"]:
                                if field["id"] == data:
                                    value = field["value"]
                                    # print(value)
                if len(metadata) == 0:
                    msg = (
                        "Metadata:" + data + " not found in dublincore/episode flavor."
                    )
                    return msg
            return value
        except ValueError:
            msg = "error getting data from event"
            return {"value": "", "error": True, "error-msg": msg}

    def get_media(self, asset, mediatype, mediasubtype, data, quality="1080p-quality"):
        try:
            lecdoc = minidom.parseString(asset)
            items = lecdoc.getElementsByTagName("mediapackage")
            # title = items[0].getElementsByTagName('title')[0].firstChild.nodeValue
            # print(title)
            # title = asset['mediapackage']['title']
            # print(mediatype+":"+data)
            tracks = lecdoc.getElementsByTagName("track")
            value = None
            for track in tracks:
                if track.attributes["type"].value == mediatype + "/" + mediasubtype:
                    # For delivery subflavor, check the queality desired
                    if mediasubtype == "delivery":
                        tags = track.getElementsByTagName("tags")[0]
                        track_quality = tags.getElementsByTagName("tag")[
                            0
                        ].firstChild.nodeValue
                        if track_quality == quality:
                            value = track.getElementsByTagName("url")[
                                0
                            ].firstChild.nodeValue
                            # print(quality)
                    else:
                        value = track.getElementsByTagName("url")[
                            0
                        ].firstChild.nodeValue
            if value == None:
                msg = (
                    "Media:"
                    + data
                    + " not found in "
                    + mediatype
                    + "/"
                    + mediasubtype
                    + " type/subtype."
                )
                return {"value": value, "error": True, "error-msg": msg}
            return {"value": value, "error": False, "error-msg": "ok"}
        except ValueError:
            msg = "error getting data from asset"
            return {"value": "", "error": True, "error-msg": msg}

    def get_year(self, eventdate):
        format = "%Y-%m-%dT%H:%M:%S.%fZ"
        dt = datetime.strptime(eventdate, format)
        # d = date(dt.year,dt.month,dt.day)
        # print(d)
        year = str(dt.year)
        return year

    def get_legacy_dir(self, event_type):
        legacy_dirs = self.server_config["legacy_dirs"].split(" ")
        # Default not found action is do nothig
        legacy_dir = "Unknown"
        unknown_dir = "None"
        for dir in legacy_dirs:
            if dir.split(":")[0] == event_type:
                legacy_dir = dir.split(":")[1]
                action = "Export"
                # print('--->%s'%(legacy_dir))
            if dir.split(":")[0] == "Unknown":
                unknown_dir = dir.split(":")[1]
        if legacy_dir == "Other":
            action = "Export to Other directory as event_type=None"
        elif legacy_dir == "None":
            action = "Do not export"
        elif legacy_dir == "Unknown":
            # Decide if Export when event_type is Unknown
            if unknown_dir == "None":
                legacy_dir = "None"
                action = "Do not export."
            elif unknown_dir == "Other":
                legacy_dir = "Other"
                action = "Export to dir: Other"
            else:
                if event_type != "":
                    legacy_dir = event_type
                    action = "Export to dir: event_type"
                else:
                    legacy_dir = "None"
                    action = "Empty event_type"
        else:
            action = "Export to a found directory"
        msg = "Legacy dir = %s --> %s " % (legacy_dir, action)
        return {"value": legacy_dir, "error": False, "error-msg": msg}

    def export_json(self, config_file, indico_id, event, asset, data=False):
        # try:
        event_id = event["identifier"]
        id = self.get_metadata(event, "indico", "contribution_id")
        event_type = self.get_metadata(event, "indico", "event_type")
        title = (
            self.get_metadata(event, "dublincore", "title")
            .replace('"', "'")
            .replace("\\", "\\\\")
        )  # Replace double quote by simple quote
        startdate = self.get_metadata(event, "dublincore", "startDate")
        duration = self.get_metadata(event, "dublincore", "duration")
        contributor = self.get_metadata(event, "dublincore", "contributor")
        presentation_sources = []
        presenter_sources = []
        video_sources = []
        captions_sources = []
        jframes = []
        eventdate = startdate.split("T")[0]
        format = "%Y-%m-%dT%H:%M:%S.%fZ"
        dt = datetime.strptime(startdate, format)
        d = date(dt.year, dt.month, dt.day)
        year = str(dt.year)
        h, m, s = duration.split(":")
        seconds = int(h) * 3600 + int(m) * 60 + int(s)
        # Get App and balancer from config file based in the year
        self.server_config["media_url"] = getBalancer(config_file, year) + "/" + id
        main_dir = self.server_config["dest_dir"] + "/" + year
        dest_dir = self.server_config["dest_dir"] + "/" + year + "/" + id
        file_dir = year + "/" + id
        msg = ""
        count = 0
        slides_filename = None
        presentation_cover = None
        presenter_cover = None
        presenter_legacy = None
        presentation_legacy = None
        cover = "cern-cover.jpg"
        legacy = "cern-legacy.jpg"
        realseconds = 0
        # Check the main dir (year)
        self.__ensure_dir(main_dir)
        # print("Balancer located:%s"%(self.server_config['media_url']))
        if "publications" in event:
            for publication in event["publications"]:
                # print(publication,'\n')
                if "media" in publication:
                    for media in publication["media"]:
                        # print(media,'\n')
                        if "tags" in media:
                            for tag in media["tags"]:
                                count = count + 1
                                # print("%s-%s"%(count,tag))
                                if tag.endswith("-quality") and media[
                                    "flavor"
                                ].endswith("delivery"):
                                    head, tail = ntpath.split(media["url"])
                                    file = os.path.splitext(tail)[0]
                                    extension = os.path.splitext(tail)[1]
                                    filename = (
                                        indico_id
                                        + "-"
                                        + media["flavor"].split("/")[0]
                                        + "-"
                                        + tag
                                        + extension
                                    )
                                    # Get the server url (apo|bakoni) from the year
                                    if (
                                        (media["flavor"] == "presentation/delivery")
                                        or (media["flavor"] == "presenter/delivery")
                                    ) and (publication["channel"] == "engage-player"):
                                        obj = (
                                            '{"src":"%s","type":"%s","baudrate":"%s","w":"%s","h":"%s"}'
                                            % (
                                                filename,
                                                media["mediatype"],
                                                str(int(media["bitrate"])),
                                                str(media["width"]),
                                                str(media["height"]),
                                            )
                                        )
                                        download = True
                                        if media["flavor"] == "presentation/delivery":
                                            presentation_sources.append(json.loads(obj))
                                        else:
                                            presenter_sources.append(json.loads(obj))
                                    if media["flavor"] == "composite/delivery":
                                        composite = (
                                            '{"src":"%s","type":"%s","baudrate":"%s","w":"%s","h":"%s"}'
                                            % (
                                                filename,
                                                media["mediatype"],
                                                str(int(media["bitrate"])),
                                                str(media["width"]),
                                                str(media["height"]),
                                            )
                                        )
                                        download = True
                                        # We can export the file just here, no need to wait (for iter in export:)
                                    if data == False and download == True:
                                        # Get the original file from URL
                                        # print("Download: %s-%s"%(count,tag))
                                        # msg = msg + "\n Get the original file from URL:%s"%(media['url'])
                                        response = self.opencast_object.get_file(
                                            media["url"],
                                            filedir=file_dir,
                                            filename=filename,
                                        )
                                        if response["error"] is True:
                                            msg = (
                                                "Can not export file %s: Error %s."
                                                % (
                                                    media["url"],
                                                    dest_dir + "/" + filename,
                                                )
                                            )
                                            return {
                                                "value": msg,
                                                "error": True,
                                                "error-msg": id + ":" + title,
                                            }
                                        else:
                                            msg = msg + "\n Video file copied:%s" % (
                                                file_dir + "/" + filename
                                            )
                                            # print("File %s copied to %s"%(media['url'], file_dir+'/'+filename))
                                            # Get the 'real'duration of the 1080p video
                                            if (
                                                realseconds == 0
                                                and tag == "1080p-quality"
                                            ):
                                                length, w, h = (
                                                    self.__get_video_metadata(
                                                        dest_dir + "/" + filename
                                                    )
                                                )
                                                realseconds = length
                                                info = (
                                                    "Get the real duration for the 1080p video (%s): %s"
                                                    % (
                                                        dest_dir + "/" + filename,
                                                        realseconds,
                                                    )
                                                )
                                                msg = msg + info
                                                # print(info)
                                        download = False
                                        filename = "NewFile:%s-%s" % (count, tag)
                                # elif (media['flavor']=='composite/tagged-download' or media['flavor']=='composite/tagged-trimmed'):

                # Get the "flavor": "mpeg-7/text" -> slidetext.xml
                if "metadata" in publication:
                    for metadata in publication["metadata"]:
                        if metadata["flavor"] == "mpeg-7/text":
                            count = count + 1
                            head, tail = ntpath.split(metadata["url"])
                            file = os.path.splitext(tail)[0]
                            extension = os.path.splitext(tail)[1]
                            slides_filename = indico_id + "-slidetext" + extension
                            if data == False:
                                response = self.opencast_object.get_file(
                                    metadata["url"],
                                    filedir=file_dir,
                                    filename=slides_filename,
                                )
                                # ToDo: Replace <MediaLocator><MediaUri>https://ocweb01-test2.cern.ch/files/mediapackage/.../video.mp4</MediaUri></MediaLocator>
                                # in id-slidetext.xml
                                if response["error"] is True:
                                    msg = "Can not export file %s: Error %s." % (
                                        media["url"],
                                        dest_dir + "/" + slides_filename,
                                    )
                                    return {
                                        "value": msg,
                                        "error": True,
                                        "error-msg": id + ":" + title,
                                    }
                                else:
                                    msg = msg + "\n Slides text file copied:%s" % (
                                        file_dir + "/" + slides_filename
                                    )
                # Get the "flavor": "flavor": "presentation/segment+preview",
                if "attachments" in publication:
                    for attachment in publication["attachments"]:
                        head, tail = ntpath.split(attachment["url"])
                        file = os.path.splitext(tail)[0]
                        extension = os.path.splitext(tail)[1]
                        # print(attachment['flavor']+':'+file+':'+extension+'-'+attachment['url'])
                        if attachment["flavor"] == "presentation/player+preview":
                            filename = (
                                indico_id + "-" + "presentation-cover" + extension
                            )
                            presentation_cover = filename
                            cover = filename
                            download = True
                            dest = file_dir
                        if attachment["flavor"] == "presenter/player+preview":
                            filename = indico_id + "-" + "presenter-cover" + extension
                            presenter_cover = filename
                            cover = filename
                            download = True
                            dest = file_dir
                        if attachment["flavor"] == "presentation/legacy+preview":
                            filename = (
                                indico_id + "-" + "presentation-legacy" + extension
                            )
                            presentation_legacy = filename
                            legacy = filename
                            download = True
                            dest = file_dir
                        if attachment["flavor"] == "presenter/legacy+preview":
                            filename = indico_id + "-" + "presenter-legacy" + extension
                            presenter_legacy = filename
                            legacy = filename
                            download = True
                            dest = file_dir
                        if attachment["flavor"] == "presentation/segment+preview":
                            ref = attachment["ref"].split(";")[1]
                            pattern = "time=T(.*?)F1000"
                            start = re.search(pattern, ref).group(1)
                            h, m, s, ms = start.split(":")
                            preview_seconds = int(h) * 3600 + int(m) * 60 + int(s)
                            filename = (
                                indico_id
                                + "-"
                                + "preview"
                                + "-"
                                + str(preview_seconds)
                                + extension
                            )
                            # Get the server url (apo|bakoni) from the year
                            obj = (
                                '{"id":"%s","url":"%s","thumb":"%s","type":"%s","start":"%s","seconds":"%s","duration":"%s","fduration":"%s"}'
                                % (
                                    attachment["id"],
                                    filename,
                                    filename,
                                    attachment["mediatype"],
                                    start,
                                    preview_seconds,
                                    "0",
                                    "0",
                                )
                            )
                            jframes.append(json.loads(obj))
                            download = True
                            dest = file_dir + "/thumbs"
                        # Subtitles captions

                        count = count + 1
                        # Ignore other attachments that are not listed above
                        # For example: presentation/timeline+preview
                        if data == False and download == True:
                            # print("Filename:%s Flavor:%s"%(filename,attachment['flavor']))
                            response = self.opencast_object.get_file(
                                attachment["url"], filedir=dest, filename=filename
                            )
                            if response["error"] is True:
                                msg = "Can not export file %s: Error %s." % (
                                    media["url"],
                                    dest_dir + "/" + filename,
                                )
                                return {
                                    "value": msg,
                                    "error": True,
                                    "error-msg": id + ":" + title,
                                }
                            else:
                                msg = msg + "\n Preview file copied:%s" % (
                                    dest + "/" + filename
                                )
                                download = False
            # PArse XML to get assets where: <mediapackage> <attachments> <attachment id="security-policy-episode" type="security/xacml+episode">
            doc = minidom.parseString(asset)
            attachments = doc.getElementsByTagName("attachment")
            for attachment in attachments:
                lang = "en"
                type = attachment.attributes["type"].value.split("/")[1]
                if attachment.attributes["type"].value.split("/")[0] == "captions":
                    format, lang = type.split("+")
                    if format == "vtt":
                        mimetype = "text/vtt"
                    if format == "dfxp":
                        mimetype = "text/xml"
                    else:
                        mimetype = "text/xml"
                    if lang == "en":
                        text = "English"
                    elif lang == "fr":
                        text = "French"
                    elif lang == "es":
                        text = "Spanish"
                    elif lang == "de":
                        text = "German"
                    else:
                        text = "English"
                    src = attachment.getElementsByTagName("url")[0].firstChild.nodeValue
                    filename = indico_id + "_" + lang + "." + format
                    # print('%s %s %s %s %s'%(text, lang,format,filename, src))
                    count = count + 1
                    if (
                        data == True
                    ):  # Change to false. True only for testing withour copying media
                        response = self.opencast_object.get_file(
                            src, filedir=file_dir, filename=filename, backup=True
                        )
                        if response["error"] is True:
                            msg = "Can not export file %s: Error %s." % (
                                media["url"],
                                dest_dir + "/" + filename,
                            )
                            return {
                                "value": msg,
                                "error": True,
                                "error-msg": id + ":" + title,
                            }
                        else:
                            msg = msg + "\n Captions file copied:%s" % (
                                file_dir + "/" + filename
                            )
                    obj = '{"type":"%s", "lang":"%s", "text":"%s"}' % (
                        format,
                        lang,
                        text,
                    )
                    captions_sources.append(json.loads(obj))

        if count == 0:
            msg = "Missing publications the event: %s: with contribution id: %s." % (
                event_id,
                id,
            )
            return {
                "value": msg,
                "error": True,
                "error-msg": "Missing publications" + id + ":" + title,
            }

        # print(jsources)
        if presentation_cover:
            cover = presentation_cover
        elif presenter_cover:
            cover = presenter_cover
            presentation_cover = cover
        else:
            cover = "cern-cover.jpg"
            presenter_cover = cover
            presentation_cover = cover
            # Copy also the default cern.jpg img file from templates folder.
            shutil.copyfile(
                self.config["template_dir"] + "/" + cover, dest_dir + "/" + cover
            )
            msg = msg + "\n Cover file copied:%s" % (dest_dir + "/" + cover)

        if presentation_legacy:
            legacy = presentation_legacy
        elif presenter_legacy:
            legacy = presenter_legacy
            presentation_legacy = legacy
        else:
            legacy = "cern-legacy.jpg"
            presenter_legacy = legacy
            presentation_legacy = legacy
            # Copy also the default cern.jpg img file from templates folder.
            shutil.copyfile(
                self.config["template_dir"] + "/" + legacy, dest_dir + "/" + legacy
            )
            msg = msg + "\n Cover file copied:%s" % (dest_dir + "/" + legacy)

        # captions.append(json.loads('{"type":"vtt", "lang":"en", "text":"English"}'))
        # Open the Paella Player data.json template and parse the export array
        env = Environment(loader=FileSystemLoader(self.config["template_dir"]))
        # This should be replaced by a loop ['data.json','lecture.json','info.xml']
        template = env.get_template("data.json")
        data = template.render(
            id=id,
            title=title,
            seconds=realseconds,
            cover=cover,
            captions=captions_sources,
            slides=slides_filename,
            presentation_cover=presentation_cover,
            presenter_cover=presenter_cover,
            url=self.server_config["media_url"],
            presentation_sources=presentation_sources,
            presenter_sources=presenter_sources,
            frames=jframes,
        )
        # Open the Theo Player lecture.json template and parse the export array
        # Ordered frames
        # oframes = sorted(jframes, key=lambda k: k['seconds'], reverse=False)
        # print("Sorting frames")
        oframes = sorted(jframes, key=lambda k: k["start"], reverse=False)
        # We must also calculate the duration of each frame after ordering
        #
        # output result
        n = 0
        for oframe in oframes:
            # print("(%s) Start:%s - Duration:%s"%(n, oframe['start'],oframe['duration']))
            if n > 0:
                oframes[n - 1]["duration"] = int(oframes[n]["seconds"]) - int(
                    oframes[n - 1]["seconds"]
                )
                # To hh:mm:ss
                oframes[n - 1]["fduration"] = str(
                    timedelta(seconds=oframes[n - 1]["duration"])
                )
                # print("(%s) Start:%s - Duration n-1:%s - FDuration:%s"%(n-1,oframes[n-1]['start'],oframes[n-1]['duration'],oframes[n-1]['fduration']))
            n = n + 1
        # The last one duration is the total duration (in seconds) minus the last one start
        # We must take into account that Conferences/Presentations doesn't have frames, so:
        if n > 0:
            oframes[n - 1]["duration"] = int(
                realseconds - int(oframes[n - 1]["seconds"])
            )
            # To hh:mm:ss
            oframes[n - 1]["fduration"] = str(
                timedelta(seconds=oframes[n - 1]["duration"])
            )
        realduration = str(timedelta(seconds=realseconds))

        # Order video_sources json by quality (h). As it is a string, 1080p goes first
        presenter_sources = sorted(
            presenter_sources, key=lambda k: k["h"], reverse=True
        )
        presentation_sources = sorted(
            presentation_sources, key=lambda k: k["h"], reverse=True
        )

        if (event_type == "simple_event") or (event_type == "web_lecture"):
            template = env.get_template("lecture.json")
            lecture = template.render(
                id=id,
                eventdate=eventdate,
                title=title,
                duration=realduration,
                seconds=realseconds,
                cover=cover,
                captions=captions_sources,
                slides=slides_filename,
                presentation_cover=presentation_cover,
                presenter_cover=presenter_cover,
                url=self.server_config["media_url"],
                presentation_sources=presentation_sources,
                presenter_sources=presenter_sources,
                year=year,
                contributor=contributor,
                frames=oframes,
            )
            if os.path.isfile(dest_dir + "/lecture.json"):
                rename = (
                    "lecture-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + ".json"
                )
                os.rename(dest_dir + "/lecture.json", dest_dir + "/" + rename)
                msg = msg + "\n File backed-up: %s/%s" % (dest_dir, rename)
            with open(dest_dir + "/lecture.json", "w") as file_object:
                file_object.write(lecture)
        # Same for info.xml
        template = env.get_template("info.xml")
        # The event_type directory should be concat
        result = self.get_legacy_dir(event_type)
        legacy_dir = result["value"]
        master_path = self.server_config["master_path"] + "\\" + legacy_dir
        public_path = (
            self.server_config["public_path"] + "\\" + legacy_dir
        )  # + '\\test' # test to be remoced from path
        # Format = WEBLECTURE or VIDEO
        if (event_type == "simple_event") or (event_type == "web_lecture"):
            format = "WEBLECTURE"
            # Include the composite picture in picture video
            if "composite" in vars():
                video_sources.append(json.loads(composite))
            info = template.render(
                id=id,
                title=title,
                duration=realduration,
                seconds=realseconds,
                cover=cover,
                legacy=legacy,
                slides=slides_filename,
                presentation_cover=presentation_cover,
                presenter_cover=presenter_cover,
                presentation_sources=presentation_sources,
                presenter_sources=presenter_sources,
                video_sources=video_sources,
                format=format,
                master_path=master_path,
                public_path=public_path,
                year=year,
                frames=jframes,
            )
        else:
            format = "VIDEO"
            if len(presentation_sources) > 0:
                video_sources = presentation_sources
            else:
                video_sources = presenter_sources
            info = template.render(
                id=id,
                title=title,
                duration=realduration,
                seconds=realseconds,
                cover=cover,
                legacy=legacy,
                slides=slides_filename,
                presentation_cover=presentation_cover,
                presenter_cover=presenter_cover,
                video_sources=video_sources,
                format=format,
                master_path=master_path,
                public_path=public_path,
                year=year,
                frames=jframes,
            )
        template = env.get_template("camera.smil")
        camera = template.render(presenter_sources=presenter_sources)
        template = env.get_template("slides.smil")
        slides = template.render(presentation_sources=presentation_sources)
        # Check if file exists to make a copy
        if os.path.isfile(dest_dir + "/data.json"):
            rename = "data-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + ".json"
            os.rename(dest_dir + "/data.json", dest_dir + "/" + rename)
            msg = msg + "\n File backed-up: %s/%s" % (dest_dir, rename)
        with open(dest_dir + "/data.json", "w") as file_object:
            file_object.write(data)
        # Create a data.v2.json file
        self.create_datav2_json(dest_dir, data)
        if os.path.isfile(dest_dir + "/info.xml"):
            rename = "info-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + ".xml"
            os.rename(dest_dir + "/info.xml", dest_dir + "/" + rename)
            msg = msg + "\n File backed-up: %s/%s" % (dest_dir, rename)
        with open(dest_dir + "/info.xml", "w") as file_object:
            file_object.write(info)
        # The smil files
        # If it is a Weblecture, there should be two files: 668017c27_desktop_camera.smil & 668017c27_desktop_slides.smil
        # If it is a Conference, only one file: 668017c27.smil
        if len(presentation_sources) > 0 and len(presenter_sources) > 0:
            # print("Presenter & presentation")
            msg = msg + "\n Presenter & presentation"
            smilfile = (
                id + "_desktop_camera.smil"
            )  # Example: 668017c27_desktop_camera.smil
            if os.path.isfile(dest_dir + "/" + smilfile):
                rename = (
                    "camera-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + ".smil"
                )
                os.rename(dest_dir + "/" + smilfile, dest_dir + "/" + rename)
                msg = msg + "\n File backed-up: %s/%s" % (dest_dir, rename)
            with open(dest_dir + "/" + smilfile, "w") as file_object:
                file_object.write(camera)
            smilfile = (
                id + "_desktop_slides.smil"
            )  # Example: 668017c27_desktop_slides.smil
            if os.path.isfile(dest_dir + "/" + smilfile):
                rename = (
                    "slides-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + ".smil"
                )
                os.rename(dest_dir + "/" + smilfile, dest_dir + "/" + rename)
                msg = msg + "\n File backed-up: %s/%s" % (dest_dir, rename)
            with open(dest_dir + "/" + smilfile, "w") as file_object:
                file_object.write(slides)
        else:
            # print("Only resenter or presentation")
            smilfile = id + ".smil"  # Example: 668017c27.smil
            #
            if os.path.isfile(dest_dir + "/" + smilfile):
                rename = (
                    "smil-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + ".smil"
                )
                os.rename(dest_dir + "/" + smilfile, dest_dir + "/" + rename)
                msg = msg + "\n File backed-up: %s/%s" % (dest_dir, rename)
            if len(presenter_sources) > 0:
                # print("Only presenter")
                msg = msg + "\n Only presenter"
                with open(dest_dir + "/" + smilfile, "w") as file_object:
                    file_object.write(camera)
            else:
                # print("Only presentation")
                msg = msg + "\n Only presentation"
                with open(dest_dir + "/" + smilfile, "w") as file_object:
                    file_object.write(slides)
        # Example: 668017c27_desktop_slides.smil
        msg = msg + "\n File saved: %s/data.json" % (dest_dir)
        # return {'value' : msg, 'error' : False, 'error-msg' : 'data.json not saved'+id+':'+title}

        # except ValueError:
        return {"value": msg, "error": False, "error-msg": "Ok"}

    def create_datav2_json(self, dest_dir, data):
        with open(dest_dir + "/data.v2.json", "w") as file_object:
            # Copy the data to a new var
            data_v2 = data
            # Replace https://ocmedia-apo.cern.ch/ with /
            data_v2 = data_v2.replace("https://ocmedia-apo.cern.ch/", "/")
            # Replace https://ocmedia-bakony.cern.ch/ with /
            data_v2 = data_v2.replace("https://ocmedia-bakony.cern.ch/", "/")
            # Replace https://ocmedia-montblanc.cern.ch/ with /
            data_v2 = data_v2.replace("https://ocmedia-montblanc.cern.ch/", "/")
            file_object.write(data_v2)

    def get_event(self, indico_id, pretty="yes", detail=None, headers="yes"):
        if detail != None:  # sessions|contributions
            params = {"detail": detail, "pretty": pretty}
        else:
            params = ""
        endpoint = "export/event/%s.json" % (quote(indico_id))
        # print("%s%s"%(self.server_config['server_url'],endpoint))
        if headers:
            auth = "Bearer %s" % (self.server_config["api_token"])
            headers = {"Authorization": auth}
        return get_call_server(
            self.server_config,
            endpoint,
            headers=headers,
            params=params,
            json=True,
            digest=False,
            verbose=True,
        )

    def get_events(self, category_id, pretty="yes", headers="yes"):
        endpoint = "/export/categ/%s.json" % (quote(category_id))
        # print("%s%s"%(self.server_config['server_url'],endpoint))
        if headers:
            auth = "Bearer %s" % (self.server_config["api_token"])
            headers = {"Authorization": auth}
        return get_call_server(
            self.server_config,
            endpoint,
            headers=headers,
            params="",
            json=True,
            digest=False,
            verbose=True,
        )

    def get_config(self, type, dir, resource, users, groups, externals, emails):
        if type == "open":
            open = True
        else:
            open = False
        env = Environment(loader=FileSystemLoader(self.server_config["template_dir"]))
        template = env.get_template("apache.conf")
        config = template.render(
            open=open,
            dir=dir,
            resource=resource,
            users=users,
            groups=groups,
            externals=externals,
            emails=emails,
        )
        return config

    def create_link(self, contribution_db_id, title, link_href, headers="yes"):
        params = {"title": title, "url": link_href, "obj": contribution_db_id}
        endpoint = "api/audiovisual/create-link"
        if headers:
            auth = "Bearer %s" % (self.server_config["api_token"])
            headers = {"Authorization": auth}
        # Methos post
        return post_call_server(
            self.server_config,
            endpoint,
            headers=headers,
            params=params,
            json=True,
            digest=False,
            verbose=True,
        )

    def get_link(self, contribution_db_id, title, link_href, headers="yes"):
        params = {"title": title, "url": link_href, "obj": contribution_db_id}
        # Check if it already exists!!
        endpoint = "api/audiovisual/get-link"
        if headers:
            auth = "Bearer %s" % (self.server_config["api_token"])
            headers = {"Authorization": auth}
        # Methos post
        return post_call_server(
            self.server_config,
            endpoint,
            headers=headers,
            params=params,
            json=True,
            digest=False,
            verbose=True,
        )
