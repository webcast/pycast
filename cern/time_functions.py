from datetime import datetime
from dateutil.tz import tzlocal
import pytz

def string_sh_to_date_time_utc(string):
    dt = None

    try:
        dt = datetime.strptime(string, '%d/%m/%Y %H:%M:%S')
    except ValueError:
        print 'Can not parse datetime: "%s". Must be in dd/mm/yyyy hh:mm:ss format' % (string)
        return None

    dt_utc = datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=pytz.utc)
    return dt_utc

def string_mh_to_date_time_utc(string):
    dt = None

    try:
        dt = datetime.strptime(string, '%Y-%m-%dT%H:%M:%SZ')
    except ValueError:
        print 'Can not parse datetime: "%s". Must be in yyyy-mm-ddddThh:mm:ssZ format' % (string)
        return None

    dt_utc = datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=pytz.utc)
    return dt_utc

def string_gl_to_date_time_utc(string):
    dt = None

    try:
        dt = datetime.strptime(string, '%Y-%m-%dT%H:%M:%S')
    except ValueError:
        print 'Can not parse datetime: "%s". Must be in yyyy-mm-ddddThh:mm:ss format' % (string)
        return None

    dt = datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=tzlocal())
    dt_utc = dt.astimezone(pytz.utc)
    return dt_utc

