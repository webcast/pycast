""" Class to interact with the oid """
import logging
import requests
from configparser import RawConfigParser
import jwt

class Oid(object):
    """ Class to interact with the Oid """
    def __init__(self, config_file, client_id, client_secret):
        # Get the secret from parsed config file, if client_secret is null:
        config = RawConfigParser()
        config.read(config_file)
        self.server_config = {}


        
        if (client_secret==None):
            logging.info('client_secret not provided. Get from config file')
            # Replace webcast_ by secret_ in the client_id
            secret = secret = config.get('Oid', 'secret_%s' %(client_id))
            self.server_config['secret'] = secret
        else:
            self.server_config['secret'] = client_secret
        prefix = config.get('Oid', 'app_prefix')
        if (prefix):
            client_id = prefix+client_id
        else:
            client_id = 'webcast_'+client_id
        self._headers = self._get_auth_headers(client_id, self.server_config['secret'])
        if not self._headers:
            raise Exception("Failed getting a token")

        #https://pyjwt.readthedocs.io/en/latest/usage.html
        encoded = self._headers['Authorization'].replace('Bearer ','')
        decoded = jwt.decode(encoded,  options={"verify_signature": False},  audience="authorization-service-api", algorithms=["RS256"])
        #logging.debug(decoded)
        #Let's retrieve the client_uid for this application
        #info = self._api_call(requests.get, "Application/?filter=displayName:%s" % client_id)
        info = self._api_call(requests.get, "Application/?filter=applicationIdentifier:%s" % client_id)

        if not info:
            raise Exception("Failed getting the list of applications")
        for app in info['data']:
            logging.debug("Checking %s and %s ", app['applicationIdentifier'], app['id'])
            if app['applicationIdentifier'] == client_id:
                self._client_uid = app['id']
                return
        raise Exception("Failed retrieving the client_uid")

    def get_roles(self):
        """ Returns all the roles defined for the application"""
        return self._api_call(requests.get, "Application/%s/roles" % self._client_uid)

    def get_role_id(self, rolename, roledesc):
        """ Returns the role id. If it doesn't exist, it creates it """
        info = self._api_call(requests.get,
                              #"Application/%s/roles?filter=displayName:%s" %
                              "Application/%s/roles?filter=name:%s" %
                              (self._client_uid, rolename))
        if info['data']:
            return info['data'][0]['id']
        else:
            result=self.create_role(rolename, roledesc)
            if (result==None):
                logging.warn("Error creating the role. It failed!!")
        return result

    def get_group_id(self, group):
        """Returns the groupid. If the entry does not exist, it creates it"""
        info = self._api_call(requests.get, "Group?filter=groupIdentifier:%s" % group)
        if info['data']:
            return info['data'][0]['id']
        return None

    def delete_role(self, role_id):
        """" deletes a role """
        return self._api_call(requests.delete,
                              "Application/%s/roles/%s" % (self._client_uid, role_id))

    def create_role(self, rolename, roledesc):
        """ creates a role """
        #data = {"name":rolename, "displayName": roledesc,
        #        "description":"Role created automatically",
        #        "applicationId":self._client_uid,
        #        "id":self._client_uid}
        data = {"name":rolename, "displayName": roledesc,
                "description":"Role created automatically",
                "minimumLoaId":"f0000000-0000-0000-0000-0000000000b1"}
        logging.info("Creating a new role:%s with desc:%s"%(rolename,roledesc))
        info = self._api_call(requests.post, "Application/%s/roles" % self._client_uid,
                              data)

        if info['message'] is None:
            logging.info("The creation of the role worked")
            return info['data']['id']
        else:
            logging.error(info['message'])
        return None

    def get_role_groups(self, role_id):
        """ get all the groups that belong to a group"""
        return self._api_call(requests.get,
                              "Application/%s/roles/%s/groups" % (self._client_uid,
                                                                  role_id))

    def create_role_group(self, role, group):
        """ adds an egroup to a role """
        info = self._api_call(requests.post,
                              "Application/%s/roles/%s/groups/%s" % (self._client_uid,
                                                                     role, group))
        #print(info)
        if info['message'] is None:
            logging.info("The creation of the group in the role  worked")
            return info['data']
        else:
            logging.warn("The creation of the role group failed: %s"%(info['message'])) 
        return None

    def delete_role_group(self, role_id, group_id):
        """" deletes a group from a role """
        return self._api_call(requests.delete,
                              "Application/%s/roles/%s/groups/%s" % (self._client_uid,
                                                                     role_id, group_id))

# Private functions
    def _get_auth_headers(self, client_id, client_secret): #pylint: disable=no-self-use
        """ Retrieve a token to create new roles """
        token_endpoint = "https://auth.cern.ch/auth/realms/cern/api-access/token"
        try:
            response = requests.post(token_endpoint, auth=(client_id, client_secret),
                                     data={"audience": "authorization-service-api",
                                           "grant_type":"client_credentials"})
        except requests.exceptions.ConnectionError as my_error:
            logging.error("Error connecting to  %s:\n %s ", token_endpoint, str(my_error))
            return None
        response = response.json()
        if 'error' in response:
            logging.error("Error getting the token %s", response)
            return None
        #logging.debug("What did we get %s", response)
        return {"Authorization": "Bearer " + response['access_token'],
                'Content-type': 'application/json'}

    def _api_call(self, operation, url, data=None):
        """ doing a call to the authentication service"""
        auth_endpoint = "https://authorization-service-api.web.cern.ch/api/v1.0"

        logging.info("Calling %s. Endpoint %s", operation.__name__, url)
        try:
            response = operation("%s/%s" % (auth_endpoint, url),
                                 headers=self._headers, json=data)
        except Exception as my_error: #pylint: disable=broad-except
            logging.info("Error getting the roles: %s", str(my_error))
            return None

        return response.json()