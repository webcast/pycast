# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os
import inspect
import sys
import datetime
import io
import ntpath
import shutil
from json import dumps
from urllib.parse import quote
from datetime import date, datetime

from cern.libtlp import TLPClient, TLPStandardClient, TLPSpeechClient
from cern.opencast_class import Opencast
from .rest_functions import get_call_server 

import hashlib
import hmac
import time
import requests
try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class MLLP:
    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        self.headers=['Accept: application/v1.1.0+json']

        mllp_url = config.get('MLLP', 'mllp_url')
        mllp_user = config.get('MLLP', 'user')
        mllp_api_key = config.get('MLLP', 'api_key')
        mllp_asr = config.get('MLLP', 'mllp_asr') 
        mllp_player = config.get('MLLP', 'mllp_player')
        self.server_config = {}
        self.server_config['server_url'] = mllp_url
        self.server_config['api_key'] = mllp_api_key
        self.server_config['user'] = mllp_user 
        self.server_config['player'] = mllp_player #'https://ttp.mllp.upv.es/player'
        self.server_config['asr'] = mllp_asr

        self.opencast_object = Opencast(config_file)

        self.tlp = TLPSpeechClient(self.server_config['server_url'], self.server_config['player'], self.server_config['user'], self.server_config['api_key'])

    def get_langs(self, tlp_id):
        # MLLP object stores the content in the self object (_data property)
        self.tlp.api_langs(tlp_id)
        return(self.tlp.get_printable_response_data())

    def get_history(self, tlp_id):
        # MLLP object sotores the content in the self object (_data property)
        self.tlp.api_edit_history(tlp_id)
        return(self.tlp.get_printable_response_data())

    def get_subs(self, tlp_id, lang, format, dst_file, backup=False):
        self.tlp.api_get(tlp_id,lang,format)
        # Check wether response_data are bytes or text. If they are text some error happened
        if (isinstance(self.tlp.get_printable_response_data(), str)):
            with open(dst_file+'.error.txt','w') as fd:
                fd.write(self.tlp.get_printable_response_data())
        else:
            if (backup):
                if os.path.isfile(dst_file):
                    #Get file extension
                    filepath, filename = os.path.split(dst_file)
                    #First is the filename, last is the extension: 982048_es.vtt or maybe 982048_es.vtt (old format)
                    #print("Filename"+filename)
                    if (len(filename.split("_"))==1):
                        name, language, ext = filename.split(".")
                    else:
                        name, langext = filename.split("_")
                        language = langext.split(".")[0]
                        ext = langext.split(".")[1] #parts[len(parts)-1]
                    #del parts[len(parts)-1]
                    #del parts[0]
                    separator = ''
                    rename= name + "-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + "_" + language + "." + ext            
                    os.rename(dst_file, filepath+'/'+rename)
                    #print("File backed-up: %s/%s"%(dst_file,filepath+'/'+rename))
            with open(dst_file,'wb') as fd:
                fd.write(self.tlp.get_printable_response_data())
                return("File saved: %s"%(dst_file))
    def get_audio(self, tlp_id, lang, aid, dst_file, backup=False):
        self.tlp.api_audiotrack(tlp_id, lang, aid)
        # Check wether response_data are bytes or text. If they are text some error happened
        if (isinstance(self.tlp.get_printable_response_data(), str)):
            with open(dst_file+'.error.txt','w') as fd:
                fd.write(self.tlp.get_printable_response_data())
        else:
            if (backup):
                if os.path.isfile(dst_file):
                    #Get file extension
                    filepath, filename = os.path.split(dst_file)
                    #First is the filename, last is the extension: 982048.es.vtt
                    parts = filename.split(".")
                    name = parts[0]
                    ext = parts[len(parts)-1]
                    del parts[len(parts)-1]
                    del parts[0]
                    separator = ''
                    rename= name + "-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + "." + separator.join(parts) + "." + ext            
                    os.rename(dst_file, filepath+'/'+rename)
                    #print("File backed-up: %s/%s"%(dst_file,filepath+'/'+rename))            
            with open(dst_file,'wb') as fd:
                fd.write(self.tlp.get_printable_response_data())

    def get_list(self, user):
        self.tlp.api_list(user)
        return(self.tlp.get_printable_response_data())

    def get_systems(self):
        self.tlp.api_systems()
        return(self.tlp.get_printable_response_data())

    def get_status(self, tlp_id):
        self.tlp.api_status(tlp_id)
        return(self.tlp.get_printable_response_data())

    def update(self, id, lang, title):
        self.tlp.manifest_init(id)
        # Add media language and title:
        self.tlp.manifest_set_metadata(language=lang,title=title)
        #self.tlp.manifest_set_main_media_file("https://videos.cern.ch/api/files/ce322ac4-8866-4f76-af23-954722203bb6/360p.mp4?versionId=da626c98-2d34-4e38-bf32-bf9621496736&download")
        #self.tlp.manifest['media']['filename']="test"
        #self.tlp.manifest['media']['fileformat']="mp4"
        #self.tlp._manifest_files.append("/tmp/test.mp4")
        #self.tlp.manifest_set_options(generate=False, test_mode=False)
        self.tlp.manifest_add_speaker("speaker-id_1", "John Snow", email="jsnow21@got.com")        
        self.tlp.api_ingest_update()
        # Why to print the response? I just want the upload ID!
        #upload_id = self.tlp.ret_data['id']
        upload_id = self.tlp.ret_data

        return(upload_id)

    def delete(self, id, mode, user):
        #if (id.startswith(self.server_config['user'])):
        self.tlp.manifest_init(id)
        self.tlp.api_ingest_delete(id, mode=mode, su=user)
        data = self.tlp.ret_data
        #else:
        #    data= {'rcode': '1', 'id': ''}
        return(data)
        
    def ingest(self, id, lang, asr, title, org_file):

        if not (asr):
            asr=self.server_config['asr']
        self.tlp.manifest_init(id)
        # Add media language and title:
        self.tlp.manifest_set_metadata(language=lang, title=title)
        # Add main media file:
        self.tlp.manifest_set_main_media_file(org_file)
        # Enable Ingest Service's Test Mode, since we are doing a test:
        self.tlp.manifest_set_options(generate=True, test_mode=False)
        self.tlp.manifest_add_subtitles_request(lang,asr) 
        if (lang=='en'):
            self.tlp.manifest_add_subtitles_request("fr")
            #self.tlp.manifest_add_subtitles_request("es")
            ##self.tlp.manifest_add_audiotrack_request("es")
            #self.tlp.manifest_add_subtitles_request("de")
        elif (lang=='fr'):
            self.tlp.manifest_add_subtitles_request("en")
            #self.tlp.manifest_add_audiotrack_request("en")
            #self.tlp.manifest_add_subtitles_request("de")
        elif (lang=='de'):
            self.tlp.manifest_add_subtitles_request("en")
            #self.tlp.manifest_add_audiotrack_request("en")
            #self.tlp.manifest_add_subtitles_request("fr")            
        else:
            self.tlp.manifest_add_subtitles_request("en")
            #self.tlp.manifest_add_audiotrack_request("en")
        #request translation (should be a TLPTextClient(TLPStandardClient))
        #self.tlp.manifest_add
        # Generate Media Package File and upload it via the /ingest/new interface:
        self.tlp.api_ingest_new()
        # Why to print the response? I just want the upload ID!
        upload_id = self.tlp.ret_data['id']

        return(upload_id)