# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os
import inspect
import sys
import datetime
import io
import ntpath
import shutil
from json import dumps
from urllib.parse import quote
from datetime import date, datetime

from cern.opencast_class import Opencast
from .rest_functions import get_call_server
from .rest_functions import upload_file

import time
import requests
try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode

class TOOLS:
    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        self.headers=['Accept: application/v1.1.0+json']

        upload_url = config.get('Upload', 'Upload_server')
        upload_path = config.get('Upload', 'Upload_path')
        upload_user = config.get('Upload', 'Upload_user')
        upload_pwd = config.get('Upload', 'Upload_passwd')


        self.server_config = {}
        self.server_config['Upload_server'] = upload_url
        self.server_config['Upload_path'] = upload_path
        self.server_config['Upload_pwd'] = upload_pwd 

        #self.opencast_object = Opencast(config_file)

    def upload(self, dest, filepath, user, passwd):

        headers = self.headers[:]
        params = []
        if (dest):
            url=dest
        else:
            # By default if dest is not provided
            url = self.server_config['Upload_server'] + self.server_config['Upload_path']
        print(url)
        return upload_file(full_url=url, filepath=filepath, 
                            user=user, passwd=passwd, headers=headers,
                            digest=False, verbose=False)