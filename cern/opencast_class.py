# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os
import inspect
import sys
from json import dumps

from .rest_functions import get_call_server 
from .rest_functions import get_list_items
from .rest_functions import download_file
from .rest_functions import post_call_server
from .rest_functions import put_call_server
from urllib.parse import quote
from datetime import date, datetime

SUCCEEDED_WORKFLOW_STATE = 'succeeded'
RUNNING_WORKFLOW_STATE = 'running'

class Opencast:
    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        self.headers=['Accept: application/v1.1.0+json']

        server_url = config.get('Opencast', 'server_admin_url')
        user = config.get('Opencast', 'user')
        passwd = config.get('Opencast', 'passwd')
        path = config.get('Indico', 'path')
        dest_dir = config.get('Media', 'dest_dir')
        #port = config.get('Opencast', 'port')

        self.server_config = {}
        self.server_config['server_url'] = server_url
        self.server_config['user'] = user 
        self.server_config['passwd'] = passwd
        self.server_config['path'] = path
        self.server_config['dest_dir'] = dest_dir
        #self.server_config['port'] = port
        
    # methods for events

    def get_event(self, event_id, with_publications=False, with_metadata=False):
        endpoint = '/api/events/%s' % (quote(event_id))
        headers = self.headers[:]
        params = []
        if with_publications:
            params.append(('withpublications', 'true'))
        if with_metadata:
            params.append(('withmetadata', 'true'))

        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params=params, 
                            json=True, digest=True, verbose=False)

    def get_asset(self, event_id):
        endpoint = '/assets/episode/%s' % (quote(event_id))
        headers = self.headers[:]
        params = []

        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params=params, 
                            json=False, digest=True, verbose=False)

    def get_events(self, params=[]):
        parse_list_args = ['', '']

        return get_list_items(self.__get_events, params, 
                            self.__parse_list_items, parse_list_args)

    def update_event(self, id_event, new_event):
        endpoint = '/rest/recordings/' + quote(id_event)
        data = new_event
        return put_call_server(self.server_config, endpoint, data, 
                            headers=[], json=True, digest=True)
                            
    def udpate_event_acl(self, event_id, acl):
        endpoint = '/api/events/%s/acl' % (quote(event_id))
        headers = self.headers[:]
        data = {'acl': dumps(acl)}

        return put_call_server(self.server_config, endpoint, data, 
                            headers=headers, params=[],
                            json=True, digest=True, verbose=True)


    def __get_events(self, params):
        endpoint = '/api/events'
        headers = self.headers[:]

        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params=params,
                            json=False, digest=False, verbose=False)

    # methods for episodes
    def get_episode(self, event_id, params):
        self.contents = ""
        ext = ".xml"
        if params['json']:
            ext = ".json"
        #fullurl = self.server_engage_url + "/search/episode" + ext
        #if (params is not None):
        #    fullurl = fullurl + "?" + urlencode(params)

        endpoint = '/search/episode%s' % (quote(ext))
        headers = self.headers[:]
        #params = []
        #if event_id:
        #    params.append(('id', quote(event_id)))
        #    params.append(('admin','true'))
        #    params.append(('q',''))

        return get_call_server(self.server_config, endpoint, headers=headers,  
                            params=params, json=True, digest=False, verbose=False)

    # methods for series

    def get_serie(self, serie_id):
        endpoint = '/api/series/%s' % (quote(serie_id))
        headers = self.headers[:]
        params = [('type', metadata_type)]

        return get_call_server(self.server_config, endpoint, headers=headers,  
                            params=params, json=True, digest=True, verbose=False)


    def get_series(self):
        parse_list_args = ['', '']

        return get_list_items(self.__get_series, [], 
                            self.__parse_list_items, parse_list_args)

    def __get_series(self, params):
        endpoint = '/api/series'
        headers = self.headers[:]

        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params=params,
                            json=False, digest=False, verbose=False)

    def get_serie_acl(self, serie_id):
        endpoint = '/api/series/%s/acl' % (quote(serie_id))
        headers = self.headers[:]

        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params=[], 
                            json=True, digest=False, verbose=False)

    def create_serie(self, metadata, acl):
        endpoint = '/api/series'
        data = {'metadata': dumps(metadata), 'acl': dumps(acl)}
        headers = self.headers[:]

        return post_call_server(self.server_config, endpoint, data, 
                            headers=headers,
                            json=False, digest=False, verbose=False)

    def udpate_serie_acl(self, serie_id, acl):
        endpoint = '/api/series/%s/acl' % (quote(serie_id))
        headers = self.headers[:]
        data = {'acl': dumps(acl)}

        return put_call_server(self.server_config, endpoint, data, 
                            headers=headers, params=[],
                            json=False, digest=False, verbose=False)

    def update_serie_metadata(self, serie_id, metadata_type,  metadata):
        endpoint = '/api/series/%s/metadata' % (quote(serie_id))
        headers = self.headers[:]
        params = [('type', metadata_type)]
        data = {'metadata': dumps(metadata)}

        return put_call_server(self.server_config, endpoint, data, 
                            headers=headers, params=params,
                            json=False, digest=False, verbose=False)

    def remove_serie(self, series_id):
        endpoint = '/api/series/%s' % (quote(series_id))
        headers = self.headers[:]

        return delete_call_server(self.server_config, endpoint, headers=headers,  
                            digest=False, verbose=False)


    # methods for event metadata
    def get_event_metadata(self, event_id, metadata_type):
        endpoint = '/api/events/%s/metadata' % (quote(event_id))
        headers = self.headers[:]
        params = [('type', metadata_type)]

        return get_call_server(self.server_config, endpoint, headers=headers,  
                            params=params, json=True, digest=True, verbose=False)

    # methods for event metadata
    def get_event_publications(self, event_id, metadata_type):
        endpoint = '/api/events/%s/publications' % (quote(event_id))
        headers = self.headers[:]
        params = [('type', metadata_type)]

        return get_call_server(self.server_config, endpoint, headers=headers,  
                            params=params, json=True, digest=True, verbose=False)

    def update_event_metadata(self, event_id, metadata_type, metadata):
        endpoint = '/api/events/%s/metadata' % (quote(str(event_id)))
        headers = self.headers[:]
        params = [('type', metadata_type)]
        data = {'metadata': dumps(metadata)}

        return put_call_server(self.server_config, endpoint, data, 
                            headers=headers, params=params,
                            json=True, digest=False, verbose=True)

    def republish_metadata(self, event_id):
        REPUBLISH_WORKFLOW = 'republish-metadata'

        return self.create_workflow_instance(REPUBLISH_WORKFLOW, event_id)


     
    # methods for worflows
    def get_worflow(self, workflow_id):
        endpoint = '/api/workflows/%s' % (quote(workflow_id))
        headers = self.headers[:]

        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params=[], 
                            json=True, digest=False, verbose=False)

    def get_workflows_processing(self):
        return self.get_workflows_by_state(state=[RUNNING_WORKFLOW_STATE])

    def get_workflows_by_state(self, state=[], not_state=[]):
        #endpoint = '/api/workflows'
        #headers = self.headers[:]
        params = []

        filters = '' 
        for item in state:
            if len(filters) > 0:
                filters += ',' 
            filters += 'state:' + str(item)

        for item in not_state:
            if len(filters) > 0:
                filters += ',' 
            filters += 'state_not:' + str(item)

        if len(filters) > 0:
            params.append(('filter', filters))


        #return get_call_server(self.server_config, endpoint, headers=headers,  
        #                    params=params, json=True, digest=False, verbose=False)
        return self.get_workflows(params)

    def get_workflows(self, params):
        parse_list_args = ['', '']

        return get_list_items(self.__get_workflows, params, 
                            self.__parse_list_items, parse_list_args)


    def __get_workflows(self, params):
        endpoint = '/api/workflows'
        headers = self.headers[:]

        return get_call_server(self.server_config, endpoint, 
                            headers=headers, params=params,
                            json=False, digest=False, verbose=False)

    def create_workflow_instance(self, workflow_definition_identifier, event_id, configuration=None):
        endpoint = '/api/workflows'
        headers = self.headers[:]
        data = {} 
        data['event_identifier'] = event_id
        data['workflow_definition_identifier'] = workflow_definition_identifier
        if configuration:
            data['configuration'] = configuration

        return post_call_server(self.server_config, endpoint, data, 
                            headers=headers,
                            json=False, digest=False, verbose=False)


    # other methods
    def get_file(self, fullurl, filedir=None, filename=None, fullpath=False, backup=False):
        user = self.server_config['user']
        passwd = self.server_config['passwd']
        if (fullpath):
            filepath=filedir
        else:
            filepath=self.server_config['dest_dir']+'/'+filedir
       
        if not os.path.exists(filepath):
            os.makedirs(filepath)
        else:            
            #Check if file exists to make a copy
            if (backup):
                if os.path.isfile(filepath+'/'+filename):
                    #Get file extension
                    name, ext = filename.split(".")
                    rename= name + "-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + "." + ext            
                    os.rename(filepath+'/'+filename, filepath+'/'+rename)
                    print("File backed-up: %s/%s"%(filepath,rename))
        print(fullurl)
        msg= download_file(fullurl, headers=[], filepath=filepath+'/'+filename, user=user, passwd=passwd, digest=True, verbose=False)
        return(msg)

    # utilities
    def __parse_list_items(self, data, parent, childs):
        total = -1  # No total elements in data 
        items = data

        return total, items

def parse_event_metadata(metadata):
    result = {}

    for field in metadata:
        iden =field['id']
        value = field['value']
        result[iden] = value

    return result
