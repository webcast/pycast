# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os

from ldap3 import Server, Connection, MOCK_SYNC, ALL_ATTRIBUTES


class LDAPClient:
    server_url = None
    use_test_server = None
    server = None
    base_dn = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
    base_dn_external = "OU=Externals,DC=cern,DC=ch"
    conn = None

    service_retrieve_attributes = ['cn',
                                   'uidNumber',
                                   'mail',
                                   ]

    user_retrieve_attributes_full = ['cn',
                                     'uid',
                                     'mail',
                                     ]

    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        server_url = config.get('LDAP', 'ldap_url')  #"xldap.cern.ch"

        self.server_config = {}
        self.server_config['server_url'] = server_url
                
        self.server_url = server_url
        self.server = Server(self.server_url)

    def _set_connection(self, connection):
        self.conn = connection

    def _make_connection(self):
        if not self.conn:
            self.conn = Connection(self.server, auto_bind=True)

    def search_users(self, username):
        self._make_connection()
        search_filter = "(&(displayName=*{}*)(cernAccountType=Primary))".format(username)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=self.user_retrieve_attributes_full)

        results = self.conn.entries
        self.conn.unbind()
        serialized_results = []
        results = results[:30]

        for user in results:
            serialized_results.append(self.serialize_user(user))

        return serialized_results

    def get_user_by_person_id(self, person_id):
        self._make_connection()
        search_filter = "(&(employeeID={})(cernAccountType=Primary))".format(person_id)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=self.user_retrieve_attributes_full)
        result = self.conn.entries

        assert len(result) == 1
        self.conn.unbind()
        return self.serialize_user(result[0])

    def get_user_by_username(self, username):
        self._make_connection()
        search_filter = "(&(cn={})(cernAccountType=Primary))".format(username)
        self.conn.search(search_base=self.base_dn, search_filter=search_filter,
                         attributes=ALL_ATTRIBUTES)
        result = self.conn.entries
        assert len(result) == 1
        self.conn.unbind()
        return self.serialize_user(result[0])

    def get_user_by_email(self, email, external=False):
        dn_to_use = self.base_dn
        search_filter = f"(&(mail={email})(|(cernAccountType=Primary)(cernAccountType=Service)))"
        if external:
            dn_to_use = self.base_dn_external
            search_filter = "(mail={})".format(email)
        self._make_connection()
        self.conn.search(search_base=dn_to_use, search_filter=search_filter,
                         attributes=ALL_ATTRIBUTES)
        result = self.conn.entries
        #for entry in result:
        #    logger.debug("{} {} {}".format(entry['mail'], entry["cn"], entry["saMAccountName"]))

        if len(result) == 0:
            self.conn.unbind()
            return None

        assert len(result) == 1
        self.conn.unbind()
        return self.serialize_user(result[0])

    def serialize_user(self, user):

        return {
            'mail': str(user['mail']),
            'username': str(user['cn']),
            'saMAccountName': str(user['saMAccountName'])
        }
