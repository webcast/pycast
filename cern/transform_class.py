# -*- coding: utf8 -*-

from configparser import RawConfigParser
import os
import inspect
import sys
import json

from xml.dom import minidom
import xml.etree.ElementTree as ET
from jinja2 import Template, Environment, FileSystemLoader

from .config import getApp, getBalancer

import shutil
import subprocess
import shlex

class TR:
    def __init__(self, config_file):
        config = RawConfigParser()
        config.read(config_file)

        template_dir = config.get('Indico', 'template_dir')
 
        self.config = {}
        self.config['template_dir'] = template_dir

    def __get_length(self, filename):
        result = subprocess.run(["ffprobe", "-v", "error", "-show_entries",
                                "format=duration", "-of",
                                "default=noprint_wrappers=1:nokey=1", filename],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
        return float(result.stdout)

    def __get_video_metadata(self, pathToInputVideo):
        cmd = "ffprobe -v quiet -print_format json -show_streams"
        args = shlex.split(cmd)
        args.append(pathToInputVideo)
        # run the ffprobe process, decode stdout into utf-8 & convert to JSON
        ffprobeOutput = subprocess.check_output(args).decode('utf-8')
        ffprobeOutput = json.loads(ffprobeOutput)

        # prints all the metadata available:
        #import pprint
        #pp = pprint.PrettyPrinter(indent=2)
        #pp.pprint(ffprobeOutput)

        # for example, find height and width
        length = ffprobeOutput['streams'][0]['duration']
        try:
            width = ffprobeOutput['streams'][0]['width']
        except:
            try:
                width = ffprobeOutput['streams'][1]['width']
            except:
                width = None

        try:
            height = ffprobeOutput['streams'][0]['height']
        except:
            try:
                height = ffprobeOutput['streams'][1]['height']
            except:
                height = None                

        return round(float(length)), width, height     
        
    def xml_2_json(self, config_file, type="xml", dir=None, lecture=None, info=None, smil=None):
        if (type):
            if(dir)and(os.path.isdir(dir)):
                # Get location and id from params
                id = os.path.basename(os.path.normpath(dir))
                parent = os.path.dirname(os.path.normpath(dir))
                location = os.path.basename(parent)
                #ssources=[]
                presenter_sources = []
                presentation_sources = []
                jframes = []
                captions = []
                cover = 'cern.jpg'
                presentation_cover = None
                presenter_cover = None
                # media_url == url -> Replace
                url = getBalancer(config_file, location) + '/' + id
                if (info and smil):
                    if(lecture!=None):
                        print("Lecture (%s) and XML info (%s) and smil (%s) files provided."%(lecture,info, smil))
                        lecdoc = minidom.parse(dir+'/'+lecture)
                        items = lecdoc.getElementsByTagName('LECTURE')
                        title = items[0].attributes['TITLE'].value
                        author = items[0].getElementsByTagName('AUTHOR')[0].firstChild.nodeValue
                        try:
                            duration = items[0].getElementsByTagName('DURATION')[0].firstChild.nodeValue
                        except:
                            duration = "0:0"
                        seconds = int(duration.split(':')[0])*60+int(duration.split(':')[1])
                        data = lecdoc.getElementsByTagName('SEQ')

                        smildoc = minidom.parse(dir+'/'+smil)
                        switches = smildoc.getElementsByTagName('video')
                        for switch in switches:
                            src=switch.attributes['src'].value.split(':')[1]
                            #if(src=='master.mp4'):
                            #elif(src.split('-')[1]=='podcast.mp4')or(src.split('-')[1]=='mobile.mp4'):
                            #    w=640; h=360
                            #else:
                            #    w=800; h=600                            
                            if(switch.attributes['src'].value.split(':')[0]=='mp4'):
                                length, w, h  = self.__get_video_metadata(dir+'/'+src)                               
                                mimetype='video/mp4'
                                obj = '{"src":"%s","type":"%s","baudrate":"%s","w":"%s","h":"%s"}'%(src,mimetype,switch.attributes['system-bitrate'].value,w,h)
                                presenter_sources.append(json.loads(obj))
                        slides = lecdoc.getElementsByTagName('SLIDE')
                        for slide in slides:
                            slideid = slide.attributes['SRC'].value.split('/')[1].split('.')[0]
                            # slide.attributes['SRC'] has not the /thumbs folder in the path, but /real/slides and thumbs/slides
                            # The data.json Jinja2 template will add the /thumbs folder
                            slideurl=str('real/slides/'+slide.attributes['SRC'].value.split('/')[1])
                            thumburl=str('thumbs/slides/'+slide.attributes['SRC'].value.split('/')[1])
                            # The first slide is the cover: BEGIN="00:00:00"
                            if (slide.attributes['BEGIN'].value == "00:00:00"):
                                cover = slideurl 
                                presenter_cover = cover      
                            timing=slide.attributes['BEGIN'].value
                            begin = int(timing.split(':')[0])*3600+int(timing.split(':')[1])*60+int(timing.split(':')[2])
                            obj = '{"id":"%s","url":"%s","thumb":"%s","type":"%s","seconds":"%s"}'%(slideid,slideurl,thumburl,slide.attributes['TYPE'].value,begin)
                            jframes.append(json.loads(obj))
                    else:
                        # Get info from info.xml and smil files
                        print("Only info (%s) and smil (%s) files provided."%(info, smil))

                        lecdoc = minidom.parse(dir+'/'+info)
                        items = lecdoc.getElementsByTagName('master')
                        title = ''
                        author = ''
                        try:
                            duration = items[0].attributes['duration'].value       
                        except:
                            duration = "0:0:0"
                            # Try metadata
                            try:
                                metadata = lecdoc.getElementsByTagName('metadata')
                                title = metadata[0].attributes['title_eng'].value
                                author = metadata[0].attributes['author'].value
                            except:
                                author='no name'
                        if duration=='None':
                            seconds = 0
                        else:                              
                            seconds = int(duration.split(':')[0])*3600+int(duration.split(':')[1])*60+int(duration.split(':')[2])

                        smildoc = minidom.parse(dir+'/'+smil)
                        switches = smildoc.getElementsByTagName('video')
                        for switch in switches:
                            src=switch.attributes['src'].value.split(':')[1]
                            w=switch.attributes['width'].value
                            h=switch.attributes['height'].value
                            bitrate=switch.attributes['system-bitrate'].value                    
                            if(switch.attributes['src'].value.split(':')[0]=='mp4'):
                                mimetype='video/mp4'
                                length, width, height  = self.__get_video_metadata(dir+'/'+src)
                                #w = width
                                #h = height
                                if (seconds == 0):
                                    #seconds= int(self.__get_length(dir+'/'+src)))
                                    seconds = length                                  
                            else:
                                mimetype='undefined'
                            obj = '{"src":"%s","type":"%s","baudrate":"%s","w":"%s","h":"%s"}'%(src,mimetype,bitrate,w,h)
                            presenter_sources.append(json.loads(obj))

                        data = lecdoc.getElementsByTagName('slave')
                        for slave in data:
                            ext = slave.attributes['ext'].value
                            if (ext=='jpg'):
                                # Use only posterframes due to the poor quality of themselves (35Kb)
                                if (slave.attributes['profile_desc'].value.startswith('posterframe')):
                                    # slave_filename_relative has not the /thumbs folder in the path, but only the filename
                                    # The data.json Jinja2 template will add the /thumbs folder                                    
                                    slideurl=slave.attributes['slave_filename_relative'].value
                                    idext=slideurl.split('.')[0]
                                    percent=slideurl.split('-')[4] 
                                    thumburl = slideurl
                                    # If slideurl contains thumbnail, it is thumb, 
                                    # if it contails posterframe it is utl
                                    #thumburl=slideurl.replace('posterframe','thumbnail') # Be careful, the wxh also canges 
                                    # Ex: 856696c220-posterframe-640x360-at-90-percent.jpg -> 856696c220-thumbnail-179x101-at-90-percent.jpg
                                    begin=int(seconds*int(percent)/100)
                                    if (begin == 0 ):
                                        cover = slideurl
                                        presenter_cover = cover
                                    #print(thumburl + ':' + slideurl)
                                    obj = '{"id":"%s","url":"%s","thumb":"%s","type":"%s","seconds":"%s"}'%(idext,thumburl,slideurl,'imge/jpg',begin)
                                    jframes.append(json.loads(obj))
                            if (ext=='0-percent'):
                                cover = slave.attributes['slave_filename_relative'].value
                                presenter_cover = cover


                elif (info):
                    if(lecture!=None):
                        # Two smil files 699730c1_desktop_slides.smil & 699730c1_desktop_camera.smil available
                        print("Lecture (%s) and JSON info (%s) files provided."%(lecture,info))
                        title = ''
                        author = ''
                        seconds = 0
                        cover='cern.jpg'
                        smildoc = minidom.parse(dir+'/'+id+'_desktop_camera.smil')
                        switches = smildoc.getElementsByTagName('video')
                        for switch in switches:
                            src=switch.attributes['src'].value.split(':')[1]
                            if(switch.attributes['src'].value.split(':')[0]=='mp4'):
                                mimetype='video/mp4'
                                w = switch.attributes['width'].value
                                h = switch.attributes['height'].value
                            else:
                                mimetype='undefined'
                                w = 0
                                h = 0
                            obj = '{"src":"%s","type":"%s","baudrate":"%s","w":"%s","h":"%s"}'%(src,mimetype,switch.attributes['system-bitrate'].value,w,h)
                            presenter_sources.append(json.loads(obj))
                        smildoc = minidom.parse(dir+'/'+id+'_desktop_slides.smil')
                        switches = smildoc.getElementsByTagName('video')
                        for switch in switches:
                            src=switch.attributes['src'].value.split(':')[1]
                            if(switch.attributes['src'].value.split(':')[0]=='mp4'):
                                mimetype='video/mp4'
                                w = switch.attributes['width'].value
                                h = switch.attributes['height'].value
                            else:
                                mimetype='undefined'
                                w = 0
                                h = 0
                            obj = '{"src":"%s","type":"%s","baudrate":"%s","w":"%s","h":"%s"}'%(src,mimetype,switch.attributes['system-bitrate'].value,w,h)
                            presentation_sources.append(json.loads(obj))
                        # Get thumbnails from lecture.json
                        with open(dir+'/lecture.json') as f:
                            data = json.load(f)
                            title = data['lecture']['title']
                            speakers = data['lecture']['speakers']
                            seconds = data['lecture']['duration']
                            thumbs =  data['lecture']['thumbs']
                            for thumb in thumbs:
                                begin = thumb['begin']
                                # OJO! No hay carpeta real
                                # Tiene video de las slides!
                                # slideurl = 'thumbs/'+thumb['src']
                                # thumburl = 'thumbs/'+thumb['src']
                                # No subfolder needed as the data.json file will set up the destination path:
                                slideurl = thumb['src']
                                thumburl = thumb['src']                                
                                if (begin == 0):
                                    cover = 'thumbs/'+ slideurl
                                    presentation_cover = cover
                                    presenter_cover = cover
                                obj = '{"id":"%s","url":"%s","thumb":"%s","type":"%s","seconds":"%s"}'%(thumb['src'],slideurl,thumburl,'imge/jpg',begin)
                                jframes.append(json.loads(obj))                                    
                    else:                    
                        # Get info from info.xml and smil files
                        print("Only info.xml provided:%s"%(info))

                        lecdoc = minidom.parse(dir+'/'+info)
                        items = lecdoc.getElementsByTagName('master')
                        title = ''
                        author = ''
                        seconds = 0
                        cover='cern.jpg'

                        try:
                            duration = items[0].attributes['duration'].value
                        except:
                            duration = "0:0:0"
                            # Try metadata
                            try:
                                metadata = lecdoc.getElementsByTagName('metadata')
                                title = metadata[0].attributes['title_eng'].value
                                author = metadata[0].attributes['author'].value
                            except:
                                author='no name'
                        seconds = int(duration.split(':')[0])*3600+int(duration.split(':')[1])*60+int(duration.split(':')[2])
                        data = lecdoc.getElementsByTagName('slave')
                        for slave in data:
                            ext = slave.attributes['ext'].value
                            if (ext=='mp4'):# or ext=='wmv'):
                                # Ex:   "245432c8-0600-kbps-maxH-360-25-fps-audio-128-kbps-48-kHz-stereo.mp4"
                                #       "a072cs5t3-0120-kbps-384x288-12.5-fps-audio-20-kbps-22-kHz-mono.wmv"
                                src=slave.attributes['slave_filename_relative'].value
                                length, width, height  = self.__get_video_metadata(dir+'/'+src)
                                if (seconds == 0):
                                    #seconds= int(self.__get_length(dir+'/'+src)))
                                    seconds = length
                                bitrate=src.split('-')[1]
                                mimetype='video/mp4'
                                #w=640;h=src.split('-')[4]
                                w = width
                                h = height
                                obj = '{"src":"%s","type":"%s","baudrate":"%s","w":"%s","h":"%s"}'%(src,mimetype,bitrate,w,h)
                                presenter_sources.append(json.loads(obj))
                            if (ext=='jpg'):
                                # Ex: "245432c8-posterframe-512x288-at-10-percent.jpg" 
                                # Some strange named: CERN-VIDEO-C-587-posterframe-384x288-at-80-percent.jpg
                                # slave_filename_relative has not the /thumbs folder in the path, but only the filename
                                # The data.json Jinja2 template will add the /thumbs folder                                
                                slideurl=slave.attributes['slave_filename_relative'].value
                                if (slideurl.find('posterframe') > -1):
                                    n = len(slideurl.split('-'))
                                    percentN=slideurl.split('-')[n-2]
                                    idext=slideurl.split('.')[0]
                                    #print('Items:%s-%s-%s'%(n,percentN,idext))
                                    percent=slideurl.split('-')[4]
                                    begin=int(int(seconds)*int(percentN)/100)
                                    if (begin == 0):
                                        cover = slideurl
                                        presenter_cover = cover
                                    print(slideurl)
                                    obj = '{"id":"%s","url":"%s","thumb":"%s","type":"%s","seconds":"%s"}'%(idext,slideurl,slideurl,'imge/jpg',begin)
                                    jframes.append(json.loads(obj))
                else:
                    return {'value' : '', 'error' : True, 'error-msg' : 'Info and smil files not provided.'}

                if (cover=='cern.jpg'):
                    # Copy also the default cern.jpg img file from templates folder.
                    shutil.copyfile(self.config['template_dir']+'/'+cover,dir+'/'+cover)
                env = Environment(loader=FileSystemLoader(self.config['template_dir']))
                template = env.get_template('data.json')
                config = template.render(id=id, title=title, duration=seconds, cover=cover, captions=captions, \
                    presentation_cover=presentation_cover, presenter_cover=presenter_cover, \
                    url=url, presenter_sources=presenter_sources, presentation_sources=presentation_sources, \
                    frames=jframes)
                return {'value' : config, 'error' : False, 'error-msg' : id+':'+title} 

            else:
                return {'value' : '', 'error' : True, 'error-msg' : 'dir not provided or it is not a dir'} 
        else:
            return {'value' : '', 'error' : True, 'error-msg' : 'Filetype not provided.'} 