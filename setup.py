from setuptools import setup, find_packages
import codecs
import os
import re

here = os.path.abspath(os.path.dirname(__file__))


# with open("README.md", "r") as fh:
#    long_description = fh.read()


def read(file_paths, default=""):
    # intentionally *not* adding an encoding option to open
    try:
        with codecs.open(os.path.join(here, *file_paths), "r") as fh:
            return fh.read()
    except Exception:
        return default


setup(
    name="PyCast",
    url="https://gitlab.cern.ch/webcast/pycast",
    author="Miguel Angel Valero",
    author_email="mvaleron@cern.ch",
    install_requires=[
        "requests",
        "configobj",
        "jsonschema",
        "setuptools",
        "argh",
        "Jinja2",
        "python-logstash-async",
        "click",
        "pyjwt",
        "ldap3",
        "numpy",
        "spacy",
        "Cython",
        "lxml",
        "bs4",
        "spacy_langdetect",
    ],
    version="1.7.8",
    license="MIT",
    scripts=[
        "scripts/indico.py",
        "scripts/openid.py",
        "scripts/media.py",
        "scripts/octools.py",
        "scripts/mllp.py",
        "scripts/legacy.py",
        "scripts/copy_all.sh",
        "scripts/s3_backup.py",
        "scripts/octools.py",
        "scripts/whisper-getvtt.sh",
    ],
    description="Opencast Tools for Webcast",
    long_description="Opencast Tools for Webcast",
    long_description_content_type="text/markdown",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
