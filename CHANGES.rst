Changelog
=========


Version 0.2
-------------

*Unreleased*

Bugfixes
^^^^^^^^

- None so far :)

Version 0.1
-------------

*Released on October 29, 2020*

Security fixes
^^^^^^^^^^^^^^
- 
.. note::

  
Improvements
^^^^^^^^^^^^
-

Bugfixes
^^^^^^^^
- 

Internal Changes
^^^^^^^^^^^^^^^^
- 
.. note::


Major Features
^^^^^^^^^^^^^^
- 

Internationalization
^^^^^^^^^^^^^^^^^^^^
-