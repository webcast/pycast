Welcome to the PyCast documentation!
==================================

Installation
------------

This section contains installation guides for specific operating systems or Linux distributions.

- `CentOS <install/debian-based.rst>`_

Configuration
------------
