#!/usr/bin/python3.6
import os
import inspect
import sys
import logging
import logging.config
import getopt
from argparse import ArgumentParser
from configparser import RawConfigParser
from xml.etree.ElementTree import parse
import json
from os import path, system
from datetime import date, datetime
import io
import ntpath
import shutil
import yaml
import re
import distutils
import csv

from cern.config import *


# add '../' to system path
CMD_FOLDER = os.path.realpath(
    os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0])
)
CMD_SUBFOLDER = os.path.realpath(
    os.path.abspath(
        os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], "../")
    )
)

if CMD_SUBFOLDER not in sys.path:
    sys.path.insert(0, CMD_SUBFOLDER)

from cern.opencast_class import Opencast
from cern.indico_class import Indico
from cern.ces_class import CES
from cern.openid_class import Oid
from cern.mllp_class import MLLP

from distutils import dir_util

_CONF_FILE = configuration_file("../pycast.cfg")


def verbose_print(*args):
    # Print each argument separately so caller doesn't need to
    # stuff everything to be printed into a single string
    for arg in args:
        print(arg),


def ensure_dir(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def main():
    global logger
    parser = ArgumentParser(description="Media args")
    parser.add_argument(
        "-action",
        dest="action",
        required=True,
        help="Action to execute: export (-tlpid required) | subsfromfile (-org_file required)",
    )
    parser.add_argument("-org_dir", dest="org_dir", default=None, help="Org path")

    parser.add_argument("-dest_dir", dest="dest_dir", default=None, help="Dest path")
    parser.add_argument(
        "-tlp_id", dest="tlp_id", default=None, help="Translectures Event ID"
    )
    parser.add_argument(
        "-contribution_id",
        dest="contribution_id",
        default=None,
        help="Opencast contribution ID",
    )

    parser.add_argument("-lang", dest="lang", default="en", help="Language (en|fr|es)")
    parser.add_argument(
        "-asr",
        dest="asr",
        default="150",
        help="ASR System to use.default = 150: CERN-Adapted English ASR System (Adapt-1L-id)",
    )
    parser.add_argument(
        "-year", dest="year", default=None, help="Year of the event. Useful for Indico"
    )
    parser.add_argument(
        "-title",
        dest="title",
        default=None,
        help="Optional argument for giving a title in MLLP server",
    )
    parser.add_argument(
        "-user",
        dest="user",
        default="admin",
        help="Optional argument for getting videos of user or ingest for him",
    )

    parser.add_argument(
        "-org_file",
        dest="org_file",
        default=None,
        help="json origin when action=subsfromfile. video file when action=ingest",
    )

    parser.add_argument(
        "-dr",
        action="store_true",
        dest="dry_run",
        default=False,
        help="Only test mode. Not further processing.",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="print all the results",
    )
    parser.add_argument(
        "-c",
        dest="config_file",
        action="store",
        default=_CONF_FILE,
        help="configuration file",
    )
    args = parser.parse_args()
    # check verbose
    if args.verbose is False:
        global verbose_print
        verbose_print = lambda *a: None  # do-nothing function

    # check config file
    config_file = args.config_file
    if not path.isfile(config_file):
        verbose_print('[ERROR] Config file: "%s" does not exists.' % (config_file))
        # Load default values
        # update_configuration(config_file)
        exit(1)
    else:
        # read logging configuration file path
        config = RawConfigParser()
        config.read(config_file)

    logging_conf_file = config.get("Logging", "path")
    if path.isabs(logging_conf_file):
        logging_conf_file = path.abspath(logging_conf_file)
    else:
        conf_base_dir = path.dirname(config_file)
        logging_conf_file = path.join(conf_base_dir, logging_conf_file)

    # check logging config file
    if not path.isfile(logging_conf_file):
        verbose_print(
            '[Error] Logging config file "%s" not exists.' % (logging_conf_file)
        )
        exit(1)

    with open(logging_conf_file, "r") as f:
        config_logger = yaml.safe_load(f.read())
        logging.config.dictConfig(config_logger)

    logger = logging.getLogger(__name__)

    # Dry-Run, test mode.
    if args.dry_run is True:
        # Simple arg checking
        logger.info(args.action)
        if args.dest_dir:
            verbose_print('[INFO] (test-mode). Origin "%s"' % (args.dest_dir))
        else:
            verbose_print("[INFO] (test-mode)")
        return 0

    tlp = MLLP(config_file)
    # verbose_print('Action:'+args.action)
    if args.action == "langs":
        if args.year:
            tlp_id = "opencast-" + args.year + "-" + args.tlp_id
            langs = tlp.get_langs(tlp_id)
            print(langs)
            # print("Langs:%s"%(tlp.get_langs(args.tlp_id)))
        elif args.tlp_id:
            langs = tlp.get_langs(args.tlp_id)
            print(langs)
        else:
            logger.error("-tlp_id or -year not provided.")
    elif args.action == "check-trs":
        if args.tlp_id and args.year:
            tlp_id = "opencast-" + args.year + "-" + args.tlp_id
            langs = tlp.get_langs(tlp_id)
            if json.loads(langs)["rcode"] == 1:
                logging.error("Media not found in the Transcription Servers")
                print("false")
            else:
                print("true")
    elif args.action == "trs":
        if args.tlp_id and args.year:
            # MLLP object stores the content in the self object (_data property), thus get_sbust return the property through the get_printable_response_data function
            if args.dest_dir:
                dest_path = args.dest_dir
                # if len(dest_dir)=2 only Location/id parsed. Complete the path from config file
                if len(args.dest_dir.split("/")) == 2:
                    dest_path = config.get("Media", "dest_dir") + "/" + args.dest_dir
                    indico_id = args.dest_dir.split("/")[1]
                    location = args.dest_dir.split("/")[0]
                logging.info("-dest_dir provided. -dest_path: " + dest_path)
            else:
                dest_path = (
                    config.get("Media", "dest_dir")
                    + "/"
                    + args.year
                    + "/"
                    + args.tlp_id
                )
                logging.info(
                    "-tlp_id and -year provided for getting subtitles but missing dest_dir. Defaulting to:"
                    + dest_path
                )
            if args.tlp_id.startswith("opencast") or args.tlp_id.startswith("legacy"):
                tlp_id = args.tlp_id
                contribution_id = args.contribution_id
                logging.info(
                    "tlp_id from direct args.tlp_id input param: %s" % (tlp_id.strip())
                )
            else:
                tlp_id = "opencast-" + args.year + "-" + args.tlp_id
                contribution_id = args.tlp_id
                logging.info(
                    "tlp_id build from args.year + args.tlp_id: %s" % (tlp_id.strip())
                )
            langs = tlp.get_langs(tlp_id)
            if json.loads(langs)["rcode"] == 1:
                logging.error("Media not found in the Transcription Servers")
                sys.exit("Error")
            jlangs = []
            jlangs.append(json.loads(langs))
            # Prepare the JSON object
            # Get the filepath from [metadata][preview]
            # Check if file exists to make a copy
            if os.path.isfile(dest_path + "/data.json"):
                rename = (
                    "data-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + ".json"
                )
                shutil.copyfile(dest_path + "/data.json", dest_path + "/" + rename)
                logging.info("File backed-up: %s/%s" % (dest_path, rename))
                with open(dest_path + "/data.json") as json_file:
                    data = json.load(json_file)
                    # Be careful. The perview image could be in the thumbs folder!!!
                    # head, tail = ntpath.split(data['metadata']['preview'])
                    # Get the path from the first mp4 video source
                    head, tail = ntpath.split(
                        data["streams"][0]["sources"]["mp4"][0]["src"]
                    )
                    logging.info(head)
            else:
                logging.error("data.json not found: %s/%s" % (dest_path, "data.json"))
                return
            captions_sources = []
            for jlang in jlangs:
                for lng in jlang["langs"]:
                    dst_file = (
                        dest_path + "/" + contribution_id + "_" + lng["lang_code"]
                    )
                    # Finally only vtt
                    format = "vtt"
                    lang = lng["lang_code"]
                    text = lng["lang_name"]
                    url = (
                        head
                        + "/"
                        + contribution_id
                        + "_"
                        + lng["lang_code"]
                        + "."
                        + format
                    )
                    tlp.get_subs(
                        tlp_id, lang, format, dst_file + "." + format, True
                    )  # ttml type 1
                    logging.info("Saving %s" % (dst_file + "." + format))
                    obj = '{"format":"%s", "lang":"%s", "text":"%s", "url":"%s"}' % (
                        format,
                        lang,
                        text,
                        url,
                    )
                    captions_sources.append(json.loads(obj))
                    # Check audiotrack
                    for audio in lng["audiotracks"]:
                        logging.info(audio["aid"])
                        tlp.get_audio(
                            tlp_id, lang, audio["aid"], dst_file + ".wav", True
                        )  # ttml type 1
                        logging.info("Saving %s" % (dst_file + ".wav"))
            verbose_print(captions_sources)
            # Open data.json for replacing captions
            with open(dest_path + "/data.json") as json_file:
                data = json.load(json_file)
                # del data['captions']
                # append the new captions
                captions = json.dumps(captions_sources)
                captionstr = '{"captions":' + captions + "}"
                # temp.append(captions_sources)
                # Search for the captions
                found = False
                for captions in data:
                    if "captions" in captions:
                        found = True
                if found:
                    logging.info("Captions found. Backup and replace")
                    for p in data["captions"]:
                        verbose_print("lang: " + p["lang"])
                        verbose_print("text: " + p["text"])
                        verbose_print("format: " + p["format"])
                        verbose_print("url: " + p["url"])
                        verbose_print("")
                    data["captions"] = captions_sources
                else:
                    logging.info("Captions not found.append.")
                    data["captions"] = captions_sources
                    # data.update({'captions':json.dumps(captions, indent = 4)})
            with open(dest_path + "/data.json", "w") as json2_file:
                json.dump(data, json2_file, indent=4)
            # Update data.v2.json as well with the captions
            update_datav2_json_with_captions(dest_path, data)
            if not args.tlp_id.startswith("legacy"):
                ces_object = CES(config_file)
                response = ces_object.set_state(
                    contribution_id, "transcription-update-finished", headers="yes"
                )
                if response["error"] is True:
                    msg = "Can not notify CES %s: Error %s." % (
                        contribution_id,
                        response["error-msg"],
                    )
                    logging.error(msg)
                else:
                    msg = "CES has been notified %s: Msg %s." % (
                        contribution_id,
                        response["value"],
                    )
                    logging.info(msg)
        else:
            logger.error("-tlp_id or -year not provided.")
    elif args.action == "list":
        print(tlp.get_list(args.user))
    elif args.action == "systems":
        print(tlp.get_systems())
    elif args.action == "status":
        if args.year:
            tlp_id = "opencast-" + args.year + "-" + args.tlp_id
            status = tlp.get_status(tlp_id)
            print(status)
        elif args.tlp_id:
            status = tlp.get_status(args.tlp_id)
            print(status)
        else:
            logger.error("-tlp_id or -year not provided.")
    elif args.action == "ingest":
        if args.tlp_id and args.year:
            if args.org_file:
                if args.org_dir:
                    org_file = args.org_dir + "/" + args.org_file
                else:
                    org_dir = config.get("Media", "dest_dir")
                    org_file = (
                        org_dir
                        + "/"
                        + args.year
                        + "/"
                        + args.tlp_id
                        + "/"
                        + args.org_file
                    )
                tlp_id = "opencast-" + args.year + "-" + args.tlp_id
                if args.title:
                    title = args.title
                else:
                    title = tlp_id
                # -org_file should be absolute to let ingest videos from everywhere
                # org_file='/mnt/import_share/Conferences/2018/625910c20/625910c20-1000-kbps-853x480-25-fps-audio-96-kbps-44-kHz-stereo.mp4'
                print("%s:%s:%s:%s" % (tlp_id, args.asr, title, org_file))
                # Translate the language table from Indico/Opencast to MLLP
                if args.lang == "eng":
                    language = "en"
                elif args.lang == "fra":
                    language = "fr"
                elif args.lang == "deu":
                    language = "de"
                elif args.lang == "esp":
                    language = "es"
                else:
                    language = args.lang
                # Check if it already exists
                langs = tlp.get_langs(tlp_id)
                if json.loads(langs)["rcode"] == 1:
                    logging.info(
                        "Media not found in the Transcription Servers. It can be uploaded."
                    )
                    upload_id = tlp.ingest(tlp_id, language, args.asr, title, org_file)
                    if upload_id != None:
                        logger.info(upload_id)
                        # Notify CES OK
                        ces_object = CES(config_file)
                        response = ces_object.set_state(
                            args.tlp_id, "transcription_ingest_finished", headers="yes"
                        )
                        if response["error"] is True:
                            msg = "Can not notify CES %s: Error %s." % (
                                args.tlp_id,
                                response["error-msg"],
                            )
                            logging.error(msg)
                        else:
                            msg = "CES has been notified %s: Msg %s." % (
                                args.tlp_id,
                                response["value"],
                            )
                            logging.info(msg)
                    else:
                        logger.error("MLLP Upload failed: upload_id=None")
                else:
                    logging.error(
                        "Media already exists in the transcription service. It should be removed/renamed first.."
                    )
            else:
                logger.error("-org_file not provided.")
        else:
            logger.error("-tlp_id or -year not provided.")
    elif args.action == "subsfromfile":
        # Open json file
        if args.org_file:
            org_file = args.org_file
        else:
            org_file = "/tmp/list.json"
        with open(org_file) as f:
            if args.dest_dir:
                data = json.load(f)
                for i in data:
                    info = i["id"].split("-")
                    print(info[2])
                    ensure_dir(args.dest_dir + "/" + info[2])
                    langs = tlp.get_langs(i["id"])
                    jlangs = []
                    jlangs.append(json.loads(langs))
                    for jlang in jlangs:
                        for lng in jlang["langs"]:
                            dst_file = (
                                args.dest_dir
                                + "/"
                                + info[2]
                                + "/"
                                + info[2]
                                + "_"
                                + lng["lang_code"]
                            )
                            # Finally only vtt
                            tlp.get_subs(
                                i["id"],
                                lng["lang_code"],
                                "vtt",
                                dst_file + ".vtt",
                                True,
                            )  # ttml type 1
                            logging.info("Saving %s" % (dst_file + ".vtt"))

                    # tlp.get_subs(i['id'], args.lang, 'ttml', dst_file) # ttml type 1
                    # dst_file = args.dest_dir+"/"+info[2]+'/'+info[2]+"."+args.lang+".srt"
                    # tlp.get_subs(i['id'], args.lang, 'srt', dst_file)
                    # logging.info("Saving %s"%(dst_file))
                    # Also copying files:
                    # system("media.py -c /etc/pycast/pycast.cfg -action transfer -org_dir /mnt/import_share/Conferences/2017/" + info[2]+ " -dest_dir " + args.dest_dir+"/"+info[2]+ " -v -data -conf_dir /mnt/media_share/media_data/2017/config -app 0000 -mlp")
                    # Copy only metadata, gen data.json, copy mllp file.
                    if args.org_dir:
                        org_arg = "-org_dir %s/%s" % (args.org_dir, info[2])
                    else:
                        org_arg = ""
                    cmd = (
                        "media.py -c /etc/pycast/pycast.cfg -action transfer %s -dest_dir %s/%s -v -mlp"
                        % (org_arg, args.dest_dir, info[2])
                    )
                    system(cmd)
            else:
                logging.info(
                    "-org_file provided for getting subtitles but nowhere to save the file."
                )


def update_datav2_json_with_captions(dest_path, data):
    with open(dest_path + "/data.v2.json", "w") as json2_file:
        # Copy the data to a new var
        data_v2 = data
        # convert data to string
        data_v2 = json.dumps(data_v2)
        # Replace https://ocmedia-apo.cern.ch/ with /
        data_v2 = data_v2.replace("https://ocmedia-apo.cern.ch/", "/")
        # Replace https://ocmedia-bakony.cern.ch/ with /
        data_v2 = data_v2.replace("https://ocmedia-bakony.cern.ch/", "/")
        # Replace https://ocmedia-montblanc.cern.ch/ with /
        data_v2 = data_v2.replace("https://ocmedia-montblanc.cern.ch/", "/")
        # Convert string to json
        data_v2 = json.loads(data_v2)
        json.dump(data_v2, json2_file, indent=4)


def log(myArgs):
    global logger
    logger.debug("MLLP call: mllp.py " + myArgs)


if __name__ == "__main__":
    #    sys.exit(main())
    main()
    log(" ".join(sys.argv[1:]))
