#!/bin/bash

n=1001;
max=1002;
app=2020;
eg="None"
path="/mnt/opencast_downloads"

function usage()
{
    echo "Delete roles (optionaly directories and files) from an openid app (year)"
    echo ""
    echo "./delete_many.sh"
    echo "-h --help"
    echo "--app=$app"
    echo "--egroup=$eg"
    echo "--from=$n"
    echo "--to=$max"
    echo "--path=$path"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --app)
            app=$VALUE
            ;;
        --egroup)
            eg=$VALUE
            ;;
        --from)
            n=$VALUE
            ;;
        --to)
            max=$VALUE
            ;;
        --path)
            path=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

echo "APP is $app";
echo "EGROUP is $eg";
echo "FROM is $n";
echo "TO is $max";
echo "PATH is $path";


while [ "$n" -le "$max" ]; do
  #rmdir -Rf "s$n"
   openid.py  -c /etc/pycast/pycast.cfg -oid_app $app -oid_acl_del $n -oid_eg_del $eg
  n=`expr "$n" + 1`;
done
