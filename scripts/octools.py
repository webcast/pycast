#!/usr/bin/python3.6
import os
import inspect
import sys
import logging
import logging.config
import getopt
from argparse import ArgumentParser
from configparser import RawConfigParser
import requests
from requests.auth import HTTPDigestAuth
from requests.auth import HTTPBasicAuth
from xml.etree import ElementTree
from datetime import datetime, timedelta
from os import path, system
import io
import ntpath
import shutil
import json 
import yaml
from urllib.parse import quote_plus, quote, urlencode
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import re
import csv
import unicodedata

from cern.mllp_class import MLLP
from cern.indico_class import Indico
from cern.ttaas_class import TtaaS
from cern.ldap_class import LDAPClient

from lxml.html.clean import Cleaner
from bs4 import BeautifulSoup

import warnings
warnings.filterwarnings('ignore')

logging.getLogger('chardet').setLevel(logging.INFO)

_CONF_FILE = './pycast.cfg'
_LOG_FILE = './log/pycast.log'

server =''  # default server
session = requests.Session()
headers=''

def verbose_print(*args):
    # Print each argument separately so caller doesn't need to
    # stuff everything to be printed into a single string
    for arg in args:
        print(arg),
    print


################
#### CREATE ####
################

def create_media_package():
    verbose_print("createMediaPackage")
    r = session.get("{0}/createMediaPackage".format(server+'/ingest'), verify=False)
    verbose_print(r.status_code, r.content)
    verbose_print(r.request.url)
    verbose_print(r.request.body)
    verbose_print(r.request.headers)
    tree = ElementTree.fromstring(r.content)
    media_package_id = tree.attrib["id"]
    verbose_print(media_package_id)
    return media_package_id, r.content

def create_serie(serie_xml, acl_filepath):
    verbose_print("series ACL")

    form_data = dict(
        metadata=serie_xml,
        acl="security/xacml+episode"
    )
    with open(acl_filepath, "rb") as f:
        verbose_print("started streaming acl file")
        files=dict(
            acl=f
        )
        r = session.post("{0}/series".format(server+'/api/'), files=files, data=form_data, verify=False)
        if (r.status_code!=201):
            return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
        else:
            return {'value' : r.content, 'error' : False, 'error-msg' : ''} 

#############
#### ADD ####
#############

def add_acl(media_package_xml, acl_filepath):
    verbose_print("addAttachment ACL")
    form_data = dict(
        mediaPackage=media_package_xml,
        flavor="security/xacml+episode"
    )
    with open(acl_filepath, "rb") as f:
        verbose_print("started streaming acl file")
        files=dict(
            acl=f
        )
        r = session.post("{0}/addAttachment".format(server+'/ingest'), files=files, data=form_data, verify=False)
        if (r.status_code!=201):
            return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
        else:
            return {'value' : r.content, 'error' : False, 'error-msg' : ''} 

def add_user(username, password, name, roles):
    verbose_print("addUser")
    form_data = dict(
        username=username,
        password=password,
        email=username+"@cern.ch",
        name=name,
        roles=roles,   
        manageable= "true",     
    )
    r = session.post("{0}/".format(server+'/admin-ng/users'), data=form_data, verify=False)
    verbose_print(r.status_code, r.request.body)
    if (r.status_code!=201):
        if (r.status_code==409):
            return {'value' : r.status_code, 'error' : True, 'error-msg' : 'User exists'}
        else:
            return {'value' : r.status_code, 'error' : True, 'error-msg' : 'Other user error'} 
    else:
        return {'value' : r.status_code, 'error' : False, 'error-msg' : ''}  


def add_group(groupname, description, roles, users):
    verbose_print("addGroup")
    form_data = dict(
        name=groupname,
        description=description,
        roles=roles,
        users=users,    
    )
    r = session.post("{0}/".format(server+'/admin-ng/groups'), data=form_data, verify=False)
    if (r.status_code!=201):
        if (r.status_code==409):
            return {'value' : r.status_code, 'error' : True, 'error-msg' : 'Group exists'}
        else:
            return {'value' : r.status_code, 'error' : True, 'error-msg' : 'Other group error'} 
    else:
        return {'value' : r.status_code, 'error' : False, 'error-msg' : ''} 
    

def add_serie(id, title, description, role, theme):
    verbose_print("addSerie")
    # To do: Check if the Series exist
    metadata = """[ {{
            "label": "Dummy",\n
            "flavor": "dublincore/series",
            "fields": [
            {{
                "id": "identifier",
                "value": "{id}"
            }},	
            {{
                "id": "title",
                "value": "{title}"
            }},
            {{
                "id": "description",
                "value": "{description}"
            }},            
            {{
                "id": "subjects",
                "value": ["Internal","{title}"]
            }}
            ]  
        }}
        ]
        """
    acl ="""[  
        {{
            "allow": true,
            "action": "write",
            "role": "ROLE_ADMIN"
        }},
        {{
            "allow": true,
            "action": "read",
            "role": "ROLE_ADMIN"
        }},
        {{
            "allow": true,
            "action": "read",
            "role": "ROLE_USER"
        }}
        ]"""
    acl_role = """[  
        {{
            "allow": true,
            "action": "write",
            "role": "ROLE_ADMIN"
        }},
        {{
            "allow": true,
            "action": "read",
            "role": "ROLE_ADMIN"
        }},
        {{
            "allow": true,
            "action": "read",
            "role": "{role}"
        }},
        {{
            "allow": true,
            "action": "write",
            "role": "{role}"
        }},
        {{
            "allow": true,
            "action": "read",
            "role": "ROLE_USER"
        }}
        ]
        """
        
    #print(acl.format(role=role.upper())) 
    verbose_print(metadata.format(id=id, title=title, description=description, subjects=title))
    verbose_print(acl_role.format(role='kk'))
    verbose_print(acl.format())
    response=get_theme(theme)
    if (response['error']==True):
        msg = 'Error getting theme "%s"'%(theme)
        verbose_print(msg)
        logger.error(msg)
        return      
    else:
        verbose_print(response['value'])   
    form_data = dict(
        metadata=metadata.format(id=id, title=title, description=description, subjects=title),
        acl=acl_role.format(role=role.upper()) if role else acl.format(),
        theme=('', response['value'])[theme!=''],
    )
    r = session.post("{0}/".format(server+'/api/series'), data=form_data, verify=False)
    if (r.status_code!=201):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        return {'value' : r.content, 'error' : False, 'error-msg' : ''} 

def add_theme(name, bumper_filepath, trailer_filepath):
    verbose_print("addTheme")
    bumper = add_file(bumper_filepath)
    if (bumper['error']=='True'):
        return {'value' : bumper['value'], 'error' : True, 'error-msg' : bumper['error-msg']}
    trailer = add_file(trailer_filepath)
    if (trailer['error']=='True'):
        return {'value' : trailer['value'], 'error' : True, 'error-msg' : trailer['error-msg']}
    form_data = dict(
        id=name,
        name=name,
        bumperActive="true",
        trailerActive = "true",
        bumperFile = bumper['value'],
        trailerFile = trailer['value'],
    )
    
    r = session.post("{0}/".format(server+'/admin-ng/themes'), data=form_data, verify=False)
    verbose_print(r.status_code, r.content)
    if (r.status_code!=200):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        return {'value' : r.content, 'error' : False, 'error-msg' : ''} 



def add_metadata(media_package_xml, name, serie):
    verbose_print("addDCCatalog")
    now = datetime.utcnow()
    start = now.strftime("%Y-%m-%dT%H:%M")
    end = (now + timedelta(hours=1)).strftime("%Y-%m-%dT%H:%M")
    xml_metadata = """<?xml version="1.0" encoding="UTF-8" ?>
    <dublincore
        xmlns="http://www.opencastproject.org/xsd/1.0/dublincore/"
        xmlns:dcterms="http://purl.org/dc/terms/"
        xmlns:oc="http://www.opencastproject.org/matterhorn/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <dcterms:identifier>0</dcterms:identifier>
            <dcterms:creator>admin</dcterms:creator>
            <dcterms:contributor>No contributor</dcterms:contributor>
            <dcterms:created>{start}</dcterms:created>
            <dcterms:temporal xsi:type="dcterms:Period">start={start}; end={end}; scheme=W3C-DTF;</dcterms:temporal>
            <dcterms:description>{name}</dcterms:description>
            <dcterms:subject>{name}</dcterms:subject>
            <dcterms:language>eng</dcterms:language>
            <dcterms:spatial>Internal</dcterms:spatial>
            <dcterms:title>{name}</dcterms:title>
            <dcterms:isPartOf>{serie}</dcterms:isPartOf>
    </dublincore>
    """
    #print(media_package_xml)
    #print(xml_metadata.format(start=start, end=end, name=name, serie=serie))

    form_data = dict(
        mediaPackage=media_package_xml,
        flavor="dublincore/episode",
        dublinCore=xml_metadata.format(start=start, end=end, name=name, serie=serie),
    )
    r = session.post("{0}/addDCCatalog".format(server+'/ingest'), data=form_data, verify=False)
    return r.content 

def add_file(filepath):
    verbose_print("addFile")
    if not os.path.exists(filepath):
        verbose_print(f'{filepath} does not exist')
        return {'value' : 404, 'error' : True, 'error-msg' : 'File doesnt exist'}
    with open(filepath, 'rb') as f:
        files=dict(
            BODY= (os.path.basename(filepath), f, 'video/mp4')
        )
       
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.',
            #'Content-Type': 'multipart/form-data',
        }
        #internal={'Content-Type': 'multipart/form-data; boundary=ebf9f03029db4c2799ae16b5428b06bd', 'X-Requested-Auth': 'Digest'}
        r = session.post("{0}/".format(server+'/staticfiles/'), files=files, headers=headers, verify=False)
        verbose_print(r.status_code, r.content)
        verbose_print(r.request.url)
        verbose_print(r.request.headers)
        if (r.status_code!=201):
            if (r.status_code==409):
                return {'value' : r.status_code, 'error' : True, 'error-msg' : 'It exists'}
            else:
                return {'value' : r.status_code, 'error' : True, 'error-msg' : 'Other error'} 
        else:
            return {'value' : r.content, 'error' : False, 'error-msg' : ''} 

def add_track(media_package_xml, video_filepath):
    verbose_print("addTrack")
    form_data = dict(
        mediaPackage=media_package_xml,
        flavor="presenter/source"
    )
    with open(video_filepath, 'rb') as f:
        verbose_print("started streaming video file")
        files=dict(
            BODY=f
        )
        r = session.post("{0}/addTrack".format(server+'/ingest'), files=files, data=form_data, verify=False)
        verbose_print(r.status_code, r.content)
        verbose_print(r.request.url)
        # verbose_print(r.request.body)
        verbose_print(r.request.headers)
        return r.content

def ingest(media_package_xml):
    verbose_print("ingest and run dummy wf")
    form_data = dict(
        mediaPackage=media_package_xml,
    )
    # r = session.post("{0}/ingest/cern-cds-videos".format(API_ENDPOINT), data=form_data, verify=False)
    r = session.post("{0}/ingest/cern-dummy".format(server+'/ingest'), data=form_data, verify=False)
    verbose_print(r.status_code, r.content)
    if (r.status_code!=200):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        return {'value' : r.content, 'error' : False, 'error-msg' : ''} 

def run_workflow(workflow, event, params):
    verbose_print("runWorkflow")
    form_data = dict(
        event_identifier=event,
        workflow_definition_identifier=workflow,
        configuration=params,
    )
    r = session.post("{0}/api/workflows".format(server), data=form_data, verify=False)
    verbose_print(r.status_code, r.content)
    verbose_print(r.request.url)
    verbose_print(r.request.body)
    verbose_print(r.request.headers)
    verbose_print(r.status_code, r.content)
    id=''
    if (r.status_code!=201):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        data = json.loads(r.content)
        return {'value' : data, 'error' : False, 'error-msg' : ''} 

def roles(name):
    if (name=='admin'):
        return('ROLE_ADMIN')
    elif (name=='adminandui'):
        return('''
            ROLE_ADMIN,
            ROLE_ADMIN_UI
        ''')
    elif (name=='manager'):
        return('''
            ROLE_API_WORKFLOW_INSTANCE_CREATE,
            ROLE_API_EVENTS_CREATE,
            ROLE_API_EVENTS_ACL_VIEW,
            ROLE_UI_EVENTS_DETAILS_METADATA_VIEW,
            ROLE_UI_EVENTS_COUNTERS_VIEW,
            ROLE_UI_EVENTS_DETAILS_COMMENTS_VIEW,
            ROLE_API_SERIES_PROPERTIES_VIEW,
            ROLE_UI_EVENTS_DETAILS_SCHEDULING_VIEW,
            ROLE_STUDIO,
            ROLE_API_EVENTS_SCHEDULING_VIEW,
            ROLE_API_EVENTS_PUBLICATIONS_VIEW,
            ROLE_API_WORKFLOW_DEFINITION_VIEW,
            ROLE_UI_EVENTS_CREATE,
            ROLE_UI_SERIES_DETAILS_STATISTICS_VIEW,
            ROLE_UI_EVENTS_EDITOR_VIEW,
            ROLE_UI_EVENTS_VIEW,
            ROLE_UI_JOBS_VIEW,
            ROLE_UI_EVENTS_DETAILS_MEDIA_VIEW,
            ROLE_API_EVENTS_METADATA_VIEW,
            ROLE_API_EVENTS_SCHEDULING_EDIT,
            ROLE_UI_EVENTS_DELETE,
            ROLE_UI_EVENTS_DETAILS_VIEW,
            ROLE_UI_SERIES_DETAILS_METADATA_VIEW,
            ROLE_API_EVENTS_VIEW,
            ROLE_API_STATISTICS_VIEW,
            ROLE_API_EVENTS_METADATA_EDIT,
            ROLE_API_SERIES_ACL_VIEW,
            ROLE_UI_SERIES_DETAILS_THEMES_VIEW,
            ROLE_UI_SERIES_DETAILS_VIEW,
            ROLE_API_WORKFLOW_INSTANCE_VIEW,
            ROLE_API_EVENTS_MEDIA_VIEW,
            ROLE_USERS,
            ROLE_UI_EVENTS_DETAILS_ATTACHMENTS_VIEW,
            ROLE_ADMIN_UI,
            ROLE_API_EVENTS_EDIT,
            ROLE_UI_EVENTS_DETAILS_WORKFLOWS_VIEW,
            ROLE_UI_EVENTS_EMBEDDING_CODE_VIEW,
            ROLE_UI_EVENTS_DETAILS_ACL_VIEW,
            ROLE_API_SERIES_METADATA_VIEW,
            ROLE_API_WORKFLOW_INSTANCE_EDIT,
            ROLE_UI_SERIES_DETAILS_ACL_VIEW,
            ROLE_UI_EVENTS_DETAILS_STATISTICS_VIEW,
            ROLE_API_SERIES_VIEW,
            ROLE_UI_EVENTS_DETAILS_ASSETS_VIEW,
            ROLE_UI_EVENTS_DETAILS_PUBLICATIONS_VIEW
        ''')
    elif (name=='external'):
        return ('''
            ROLE_UI_EVENTS_DETAILS_STATISTICS_VIEW,
            ROLE_API_EVENTS_METADATA_VIEW,
            ROLE_UI_EVENTS_DETAILS_METADATA_VIEW,
            ROLE_API_EVENTS_MEDIA_VIEW,
            ROLE_API_SERIES_VIEW,
            ROLE_API_SERIES_METADATA_VIEW,
            ROLE_API_WORKFLOW_INSTANCE_EDIT,
            ROLE_UI_SERIES_DETAILS_VIEW,
            ROLE_API_SERIES_ACL_VIEW,
            ROLE_API_WORKFLOW_INSTANCE_VIEW,
            ROLE_API_EVENTS_CREATE,
            ROLE_UI_EVENTS_COUNTERS_VIEW,
            ROLE_ADMIN_UI,
            ROLE_UI_EVENTS_DETAILS_COMMENTS_VIEW,
            ROLE_UI_EVENTS_DETAILS_VIEW,
            ROLE_API_WORKFLOW_INSTANCE_CREATE,
            ROLE_UI_EVENTS_EDITOR_VIEW,
            ROLE_API_EVENTS_METADATA_EDIT,
            ROLE_UI_EVENTS_DETAILS_ASSETS_VIEW,
            ROLE_API_STATISTICS_VIEW,
            ROLE_UI_EVENTS_VIEW,
            ROLE_API_EVENTS_VIEW,
            ROLE_UI_SERIES_DETAILS_THEMES_VIEW,
            ROLE_API_EVENTS_SCHEDULING_EDIT,
            ROLE_UI_EVENTS_DELETE,
            ROLE_USERS,
            ROLE_UI_EVENTS_DETAILS_MEDIA_VIEW,
            ROLE_UI_EVENTS_DETAILS_ATTACHMENTS_VIEW,
            ROLE_UI_EVENTS_EMBEDDING_CODE_VIEW,
            ROLE_UI_SERIES_DETAILS_ACL_VIEW,
            ROLE_API_SERIES_PROPERTIES_VIEW,
            ROLE_UI_SERIES_DETAILS_METADATA_VIEW,
            ROLE_API_EVENTS_PUBLICATIONS_VIEW,
            ROLE_UI_SERIES_DETAILS_STATISTICS_VIEW,
            ROLE_UI_EVENTS_CREATE,
            ROLE_UI_EVENTS_DETAILS_WORKFLOWS_VIEW,
            ROLE_API_EVENTS_SCHEDULING_VIEW,
            ROLE_UI_EVENTS_DETAILS_PUBLICATIONS_VIEW,
            ROLE_API_WORKFLOW_DEFINITION_VIEW,
            ROLE_STUDIO,
            ROLE_UI_EVENTS_DETAILS_ACL_VIEW,
            ROLE_UI_EVENTS_DETAILS_SCHEDULING_VIEW,
            ROLE_API_EVENTS_EDIT,
            ROLE_UI_JOBS_VIEW,
            ROLE_API_EVENTS_ACL_VIEW
        ''')

#############
#### GET ####
#############

def get_theme(name):
    verbose_print("getTheme")
    form_data = dict(
        limit="1000",
    )
    r = session.get("{0}/themes.json".format(server+'/admin-ng/themes'), data=form_data, verify=False)
    verbose_print(r.status_code, r.content)
    verbose_print(r.request.url)
    # verbose_print(r.request.body)
    verbose_print(r.request.headers)
    id=''
    result = json.loads(r.content)
    data=result['results']
    for i in data:
        if i['name'] == name:
            id=i['id']
            break
    if (r.status_code!=200):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        return {'value' : id, 'error' : False, 'error-msg' : ''} 

def get_serie(id):
    verbose_print("getSerie")
    form_data = dict(
        limit="1000",
    )
    #r = session.get("{0}/{1}".format(server+'/api/series',id), data=form_data, verify=False)
    r = session.get("{0}/".format(server+'/api/series'), data=form_data, verify=False)
    verbose_print(r.status_code, r.content)
    verbose_print(r.request.url)
    # verbose_print(r.request.body)
    verbose_print(r.request.headers)
    title=''
    data = json.loads(r.content)
    for i in data:
        if i['identifier'] == id:
            verbose_print('found')
            title=i['title']
            break
    if (r.status_code!=200):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        return {'value' : title, 'error' : False, 'error-msg' : ''} 

def get_event(params, data):
    verbose_print("getEvent")

    if ("start" in params):
        # Do not include metadata as it fails
        withmetadata="false"
    else:
        withmetadata="true"
    verbose_print("{0}/?{1}".format(server+'/api/events',"withmetadata="+withmetadata+"&"+params))
    r = session.get("{0}/?{1}".format(server+'/api/events',"withmetadata="+withmetadata+"&"+params), verify=False)
    #r = session.get("{0}/?{1}".format(server+'/api/events', 'filter=title:'+name), verify=False)

    verbose_print(r.status_code, r.content)
    verbose_print(r.request.url)
    verbose_print(r.request.body)
    verbose_print(r.request.headers)


    if (r.status_code==200):
        content = json.loads(r.content)
        result=[]
        for i in content:
            # Get the first one
            if (data=='id'):
                result=i['identifier']
                break
            else: #all
                data = dict(
                    event_id=i['identifier'],
                    title=i['title'],
                    date=i['start'],
                    created=i['created']
                )
                if (withmetadata=="true"):
                    fields=i['metadata'][0]['fields']
                    for j in fields:
                        if j['id']=='contribution_id':
                            data["contribution_id"]=j['value']
                result.append(data)

        return {'value' : result, 'error' : False, 'error-msg' : ''}                 
    else:
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 

def get_wf(params, withconfiguration, limit=0):
    verbose_print("{0}/?{1}".format(server+'/api/workflows',params+'&withconfiguration='+withconfiguration+'&limit='+str(limit)))
    r = session.get("{0}/?{1}".format(server+'/api/workflows',params+'&withconfiguration='+withconfiguration+'&limit='+str(limit)), verify=False)
    verbose_print(r.status_code, r.content)
    verbose_print(r.request.url)
    # verbose_print(r.request.body)
    verbose_print(r.request.headers)    
    events=[]
    data = json.loads(r.content)
    for i in data:
        data = dict(
            event_id=i['event_identifier'],
            identifier=i['identifier']
        )
        if withconfiguration=="true" and 'ingest_start_date' in i['configuration']:
            data["date"]=i['configuration']['ingest_start_date']
            if ('flagForTranscription' in i['configuration'].keys()):
                data["flagForTranscription"]=i['configuration']['flagForTranscription']
            else:
                data["flagForTranscription"]='false'
        else:
            #Some data will work as we tell the client that it has not been transcripted
            data['date']='20220105T194712Z'
            data["flagForTranscription"]='false'
        events.append(data)
    if (r.status_code!=200):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        return {'value' : events, 'error' : False, 'error-msg' : ''} 

##############
### DELETE ###
##############

def delete_event(id):
    verbose_print("deleteEvent")
    verbose_print("{0}/{1}".format(server+'/api/events',id))
    r = session.delete("{0}/{1}".format(server+'/api/events',id), verify=False)
    verbose_print(r.status_code, r.content)
    verbose_print(r.request.url)
    verbose_print(r.request.body)
    verbose_print(r.request.headers)
    if (r.status_code!=202):
        return {'value' : r.status_code, 'error' : True, 'error-msg' : r.content} 
    else:
        return {'value' : 'deleted', 'error' : False, 'error-msg' : ''} 



def main():
    global logger
    global server

    parser = ArgumentParser(description='Indico args')
    parser.add_argument('-action', dest='action', required = True, help='Action to execute.')
    parser.add_argument('-host', dest='host', required = False, default="" , help='Opencast host.')    
    parser.add_argument('-ev_id', dest='event_id', required = False, help='Opencast Event ID') 

    parser.add_argument('-id', dest='id', required = False, help='Identifier')
    parser.add_argument('-name', dest='name', required = False, help='Name')
    parser.add_argument('-pass', dest='password', required = False, help='Password')
    parser.add_argument('-desc', dest='description', required = False, default='', help='Description')                       
    parser.add_argument('-role', dest='role', required = False, help='Role: admin|adminandui|manager|external|user')  
    parser.add_argument('-serie', dest='serie', required = False, default='', help='Serie: 0')
    parser.add_argument('-theme', dest='theme', required = False, default='', help='Theme: CERNKnowledge|eLearning')
    parser.add_argument('-bumper', dest='bumper', required = False, default='', help='bumper filepath')
    parser.add_argument('-trailer', dest='trailer', required = False, default='', help='trailer filepath')
    parser.add_argument('-params', dest='params', required = False, default='', help='params to parse to the workflow')
    parser.add_argument('-year', dest='year', required = False, default='', help='year of the event')
    parser.add_argument('-wf', dest='wf', required = False, default='', help='workflow to run')
    parser.add_argument('-path', dest='path', required = False, default='', help='destination file when getting a resource')
    parser.add_argument('-mode', dest='mode', required = False, default=None, help='simple|full output')
    parser.add_argument('-user', dest='user', required = False, default='admin',help='Default user: admin|cern|legacy')  


    #parser.add_argument('-event', dest='event', required = False, default='', help='Event: Dummy')

    parser.add_argument('-c', dest='config_file', action='store', default=_CONF_FILE,
                        help='configuration file')    
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='print all the results')
    parser.add_argument('-dr', action='store_true', dest='dry_run', default=False,
                        help='Only test mode. Not further processing.')

    args = parser.parse_args()
    # check verbose
    if args.verbose is False:
        global verbose_print
        verbose_print = lambda *a: None      # do-nothing function
     

    # check config file
    config_file = args.config_file
    if not path.isfile(config_file):
            verbose_print('[WARN] Config file: "%s" does not exists. Use default configs.' % (config_file))
            exit(1)

    # read logging configuration file path
    config = RawConfigParser()
    config.read(config_file)

    logging_conf_file = config.get('Logging', 'path')
    if path.isabs(logging_conf_file):
        logging_conf_file = path.abspath(logging_conf_file) 
    else:
        conf_base_dir = path.dirname(config_file)
        logging_conf_file = path.join(conf_base_dir, logging_conf_file)
    verbose_print("Logger:" + logging_conf_file)
    # check logging config file
    if not path.isfile(logging_conf_file):
        verbose_print('[Error] Logging config file "%s" does not exists.' % (logging_conf_file))
        exit(1)

    with open(logging_conf_file, 'r') as f:
        config_logger = yaml.safe_load(f.read())
        logging.config.dictConfig(config_logger)
    logger = logging.getLogger("rundeck")

    #No needed as logger is defined from config file
    #logging.basicConfig(filename=_LOG_FILE, level=logging.DEBUG, format="%(asctime)s | %(levelname)s | (%(funcName)s:%(process)d) | %(message)s")
    #logger = logging.getLogger(__name__)

    # Dry-Run, test mode.
    if args.dry_run is True:
        # Simple arg checking
        logger.info(args.action)
        verbose_print('dry_run')    
        if (args.event_id):
            verbose_print('[INFO] (test-mode). Action: %s. Event "%s"' % (args.action, args.event_id))
        else:
            verbose_print('[INFO] (test-mode). Action: %s' % args.action)
        return 0

    verbose_print('[INFO] ' + args.action)
    logger.info(args.action)
    if (args.host):
        server=args.host
    else:
        server = config.get('Opencast', 'server_admin_url')
    verbose_print(server)
    if (config.get('Tools', 'auth_method')=='digest'):
        session.auth = HTTPDigestAuth(config.get('Tools', 'user'), config.get('Tools', 'passwd'))
        #headers = {'content-type': 'application/x-www-form-urlencoded', 'X-Requested-Auth': 'Digest'}
        headers = {'X-Requested-Auth': 'Digest'}
        session.headers = headers
        verbose_print('[INFO] Digest auth')
    else:
        session.auth = (config.get('Tools', 'user'), config.get('Tools', 'passwd'))
        verbose_print('[INFO] Basic auth')

    response= {'value' : 'start', 'error' : False, 'error-msg' : ''} 
    if (args.action == 'test'):
        verbose_print('[INFO] (test-action)')
    elif (args.action == 'add_serie'):
        if not(args.id or args.name or args.role):
            msg = 'Missing param/s: -id, -name, -role are required'
            verbose_print(msg)
            logger.error(msg)
            return        
        response=get_serie(args.id)
        verbose_print("Get serie:"+response['value'])
        if(response['error']==False):
            if (response['value']!=''):
                msg = 'Serie "%s" already exists'%(args.name)
                verbose_print(msg)
                logger.warning(msg)            
            else:
                if (args.theme):
                    response=get_theme(args.theme)
                    verbose_print(response['value'])
                    if(response['error']==False):
                        if (response['value']==''):
                            msg = 'Theme "%s" does not exists'%(args.theme)
                            verbose_print(msg)
                            logger.error(msg)         
                        else:
                            msg = 'Theme found "%s"'%(args.theme)
                            verbose_print(msg)
                            logger.info(msg)    
                            response=add_serie(args.id, args.name, args.description, args.role, args.theme)            
                else:
                    msg = 'No theme provided'
                    verbose_print(msg)
                    logger.info(msg)  
                    response=add_serie(args.id, args.name, args.description, args.role, '')
    elif (args.action == 'get_theme'):
        if not(args.name):
            msg = 'Missing param/s: -name required'
            verbose_print(msg)
            logger.error(msg)
            return
        response=get_theme(args.name)
        verbose_print('Theme id: %s'%(response['value']))
    elif (args.action == 'add_file'):
        if (not args.bumper and (not args.trailer)):
            msg = 'Missing param/s: -bumper or -trailer required'
            verbose_print(msg)
            logger.error(msg)
            return
        if args.bumper:
            response=add_file(args.bumper)
        else:
            response=add_file(args.trailer)
        verbose_print(response['value'])        
    elif (args.action == 'get_event'):
        if not(args.params):
            if not(args.name):
                msg = 'Missing param/s: -name required'
                verbose_print(msg)
                logger.error(msg)
                return                    
            response=get_event("filter=title:"+args.name,'id')
            verbose_print('Event id: '+response['value'])
            # Return the clean response value to console for Rundeck processing
            print(response['value'])
        else:
            response=get_event(args.params,'all')    
            verbose_print(response['value'])
            # Return the clean response value to console for Rundeck processing
            if(response['error']==False):
                print(response['value'])
            else:
                print('Error')
    elif (args.action == 'delete_event'):
        if not(args.id):
            msg = 'Missing param/s: -id required'
            verbose_print(msg)
            logger.error(msg)
            return
        # Get the CES Service URL from config file:

        response=delete_event(args.id)
        verbose_print('Event deleted:%s '%(response['value']))
        if(response['error']==False):
            print(response['value'])
        else:
            print('Error')
    elif (args.action == 'get_serie'):
        if not(args.id):
            msg = 'Missing param/s: -id required'
            verbose_print(msg)
            logger.error(msg)
            return        
        response=get_serie(args.id)
        verbose_print('Serie title:%s '%(response['value']))    
    elif (args.action == 'add_theme'):
        if not(args.name):
            msg = 'Missing param/s: -name required'
            verbose_print(msg)
            logger.error(msg)
            return
        response=get_theme(args.name)
        verbose_print(response['value'])
        if(response['error']==False):
            if (response['value']):
                msg = 'Theme "%s" already exists'%(args.name)
                verbose_print(msg)
                logger.error(msg)            
            else:
                if (args.bumper and args.trailer):    
                    bumper_filepath = args.bumper 
                    trailer_filepath = args.trailer 
                else:
                    bumper_filepath = os.path.join(path.dirname(config_file) , 'templates', args.name, config.get('Tools', 'Bumper'))#os.getcwd() + os.sep + os.pardir
                    trailer_filepath = os.path.join(path.dirname(config_file) , 'templates', args.name, config.get('Tools', 'Trailer'))#os.getcwd() + os.sep + os.pardir
                verbose_print(bumper_filepath)
                verbose_print(trailer_filepath)
                response=add_theme(args.name, bumper_filepath, trailer_filepath)        
    elif (args.action == 'add_group'):
        if not(args.name or args.description or args.role):
            msg = 'Missing param/s: -name, -desc, -role are required'
            verbose_print(msg)
            logger.error(msg)
            return        
        response=add_group(args.name,args.description,roles(args.role),'')        
    elif (args.action == 'add_user'):
        if not(args.name or args.password or args.description):
            msg = 'Missing param/s: -name, -pass, -desc, -role are required'
            verbose_print(msg)
            logger.error(msg)
            return
        if (args.role=='admin'):
            role='[{"name": "ROLE_ADMIN","type": "INTERNAL"}]'
        else:
            role='[{"name": "ROLE_GROUP_'+args.name.upper()+'","type": "INTERNAL"},{"name": "ROLE_GROUP_EXTERNAL","type": "INTERNAL"}]'
            external=add_group('External','External API + UI',roles('external'),'')
            if(external['error']==False):
                logging.info("Result group external: %s"%(external['value'])) 
            else:
                logging.warning("Result group external: %s"%(external['error-msg']))      
        response=add_user(args.name ,args.password, args.description,role)
        if(response['error']==False):
            logging.info("User OK")
            group=add_group(args.name,args.description,'ROLE_GROUP_EXTERNAL',args.name)   
            if(group['error']==False):
                logging.info("Result group %s: %s"%(args.name,group['value'])) 
            else:
                logging.warning("Result group: %s"%(group['error-msg']))  
        else:
            logging.error("Result user:%s"%(response['error-msg'])) 
    elif (args.action == 'add_event'): 
        logger.info("-action %s parsed."%(args.action))
        if not(args.name):
            msg = 'Missing param/s: -name required'
            verbose_print(msg)
            logger.error(msg)
            return      
        video_filepath = "./data/camera.mp4"
        acl_filepath = "./data/acl.xml"
        if not(args.serie):
            serie='0'
            jsonvar=add_serie(serie, "Dummy", "Dummy Serie", 'ROLE_ADMIN', '')
        else:
            serie=args.serie

        # Check if event exists
        response=get_event("filter=title:"+args.name,'id')
        if(response['error']==False):
            if (response['value']):
                msg = 'Event "%s" already exists'%(args.name)
                verbose_print(msg)
                logger.error(msg)            
            else:
                verbose_print('Event id: %s'%(response['value']))

                mp_id, mp_xml = create_media_package()
                mp_xml=add_metadata(mp_xml, args.name, serie)
                # Not required for Dummy Event
                #mp_xml=add_track(mp_xml, video_filepath)
                # Not required for Dummy Event
                #mp_xml=add_acl(mp_xml, acl_filepath)
                response=ingest(mp_xml)



    elif (args.action == 'get_wf'): 
        logger.info("-action %s parsed."%(args.action))
        if not(args.params):
            msg = 'Missing param/s: -params for the filter required'
            verbose_print(msg)
            logger.error(msg)
            return      

        # Check if event exists
        response=get_wf(args.params, "true", limit=15000)
        if(response['error']==False):
            if (response['value']):
                msg = 'Events "%s" with filter "%s" found.'%(args.params, response['value'])
                verbose_print(msg)
                logger.info(msg)
                # Return the clean response value to console for Rundeck processing
                print(response['value'])
            else:
                msg = 'Events "%s" with filter "%s" doesnt exist.'%(args.params, response['value'])
                verbose_print(msg)
                logger.error(msg)   

    elif (args.action == 'check-trs'):
        if (args.id and args.year):
            tlp = MLLP(config_file)
            id = 'opencast-'+args.year+'-'+args.id        
            langs = tlp.get_langs(id)
            jlangs=json.loads(langs)
            if(jlangs['rcode']==1):
                print ("false")
            else:
                print("true")
        else:
            print("Missing args: id, year")    

    elif (args.action == 'get-langs'):
        if (args.id and args.year):
            tlp = MLLP(config_file)
            id = 'opencast-'+args.year+'-'+args.id        
            langs = tlp.get_langs(id)
            jlangs=json.loads(langs)
            if(jlangs['rcode']==1):
                print ("false")
            else:
                print(jlangs)
        else:
            print("Missing args: id, year") 
    elif (args.action == 'get-lang'):
        if (args.id and args.year and args.params):
            tlp = MLLP(config_file)
            id = 'opencast-'+args.year+'-'+args.id    
            lang = args.params
            if (args.path):
                file = args.path
            else:
                file = '/tmp/{}_{}.vtt'.format(args.id,lang)
            lang = tlp.get_subs(id, lang, 'vtt', file)
            print(lang)
        else:
            print("Missing args: id, year, params:lang=en") 
    elif (args.action == 'get-versions'):
        if (args.id and args.year):
            tlp = MLLP(config_file)
            id = 'opencast-'+args.year+'-'+args.id    
            history = json.loads(tlp.get_history(id))
            if (len(history["edit_history"]) > 0):
                for i in history["edit_history"]:
                    for j in i["edit_stats"]:
                        print(print("{}/{}/{}".format(i["session_id"],j,i["ended_at"])))
            else:
                print("No version")                        
        else:
            print("Missing args: id, year")
    elif (args.action == 'get-version'):
        if (args.id and args.year and args.params):
            tlp = MLLP(config_file)
            id = 'opencast-'+args.year+'-'+args.id    
            history = json.loads(tlp.get_history(id))
            if (len(history["edit_history"]) > 0):
                for i in history["edit_history"]:
                    for j in i["edit_stats"]:
                        if (j==args.params):
                            # As they are ordered from last to first, return the first item in the list
                            #print(print("{}/{}/{}".format(i["session_id"],j,i["ended_at"])))
                            print(i["ended_at"])
                            return
                # If the language is not found in the edit_history return anyway the current date
                print(False)
                #print(datetime.today())
            else:
                #No history. Return today
                print(False)
                #print(datetime.today())
        else:
            print("Missing args: id, year")
    elif (args.action == 'tlp-events'):
        tlp = MLLP(config_file)
        list = tlp.get_list(args.user)
        print(list)
    elif (args.action == 'tlp-delete'):
        if (args.id):
            # Check first, that it exists:
            tlp = MLLP(config_file)            
            #langs = tlp.get_langs(args.id)
            #jlangs=json.loads(langs)
            #if(jlangs['rcode']!=1):
            if (args.mode=='soft'):
                mode=args.mode
            else:
                mode='hard'
            data = tlp.delete(args.id, mode, args.user)
            if(data['rcode']=='1'):
                print("Error. The event does not belong to the user whose token you are using.")
            elif(data['rcode']=='0'):
                print("Unexpected error.")
            else:
                print(data)
            #else:
            #    print("The event doesnt exists ni TLP.")
        else:
            print("Missing args: id")
    elif (args.action == 'get-login'):
        if (args.id):
            ldap = LDAPClient(config_file)
            login = ldap.get_user_by_email(args.id)
            if login!=None:
                print({"login":login['username']})
            else:
                print({"login":"not@found"})
        else:
            print("Missing args: id = email") 

    elif (args.action == 'run_wf'): 
        logger.info("-action %s parsed."%(args.action))
        if not(args.id or args.name):
            msg = 'Missing param/s: -id and -name event_name required'
            verbose_print(msg)
            logger.error(msg)
            return      

        # Check if event exists
        if (args.name):
            response=get_event("filter=title:"+args.name,'id')
        else:
            response=get_event("filter=identifier:"+args.id,'id')
        if(response['error']==False):
            if (response['value']):
                msg = 'Event "%s" with id "%s" exists. We can run the WF'%(args.id, response['value'])
                verbose_print(msg)
                logger.info(msg)
                if not(args.params):
                    verbose_print('No params provided.')
                    logger.info('No params provided.')
                else:
                    logger.info("Params: %s"%(args.params))
                wf=run_workflow(args.wf, response['value'], args.params)   
                if(wf['error']==False):
                    logger.info("Result WF %s: %s"%(args.wf,wf['value'])) 
                    # Return the clean response value to console for Rundeck processing
                    print(wf['value'])
                else:
                    logger.warning("Result WF: %s"%(wf['error-msg']))  
   

            else:
                msg = 'Event "%s" doesnt exist. Unable to continue: %s'%(args.name, response['value'])
                verbose_print(msg)
                logger.error(msg)   
    elif (args.action == 'legacy_get_event'):
        if not(args.event_id):
            msg = 'Missing param/s: -ev_id required'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            indico_object = Indico(config_file)
            response = indico_object.get_event(args.event_id)
            ok=False
            if(response['error']==False):
                if (response['value']):
                    try:
                        if 'value' in response:
                            if 'results' in response['value']:
                                if(len(response['value']['results'])>0):
                                    ok=True
                                    msg = 'Event "%s" found: %s'%(args.event_id, response['value']['results'][0]['title'])
                                else:
                                    msg='No results'
                            else:
                                msg='Error results'
                        else:
                            msg='Error value'
                    except ValueError:
                        msg = 'Event "%s" error:\n%s'%(args.event_id, ValueError)
                    verbose_print(msg)
                    logger.info(msg)
                    if (args.mode=='simple'):
                        if ok==True:
                            event=response['value']['results'][0]
                            year=event['startDate']['date'].split('-')[0]
                            event_id=event['id']
                            title=event['title']
                            creator=event['creator']['email']
                            description=event['description']  
                            if ('categoryId' in event):
                                category=event['categoryId']
                            else:
                                category='0'
                        else:
                            year=''
                            event_id=''
                            title=msg
                            creator=''
                            category=''
                            description=''
                        data = dict(
                                year=year,
                                event_id=event_id,
                                title=title,
                                creator=creator,
                                category=category,
                                description=description                              
                            )
                        print(data)
                    else:
                        print(response['value'])
            else:
                msg = 'Event "%s" doesnt exist. Unable to continue: %s'%(args.event_id, response['value'])
                verbose_print(msg)
                logger.error(msg)
    elif (args.action == 'legacy_get_contribution'):
        if not(args.event_id):
            msg = 'Missing param/s: -ev_id required'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            # Get the main id (truncate the strings). Just convert to number and then to string
            indico_id=args.event_id
            if (len(indico_id.split("_"))>0):
                indico_id=indico_id.split("_")[0]            
            rex = re.compile("^[0-9]+c[0-9]+sc[0-9]+$")
            rey = re.compile("^a[0-9]+$")
            session_id=None
            event_id=None
            contribution_id=None
            subcontribution_id=None
            db_id=None
            if indico_id.isdigit():
                msg='Direct event indico_id format: %s'%(indico_id)
                event_id=indico_id
            elif re.match(r'[0-9]+c[0-9]+$', indico_id):
                # Append ?detail=contributions to API query
                msg='Contribution indico_id format: %s'%(indico_id)
                event_id = re.search('(.+?)(c)', indico_id).group(1)
                contribution_id=re.search('(.+?)(c)([0-9]+)', indico_id).group(3)
            elif re.match(r'[0-9]+s[0-9]+$', indico_id):
                # Append ?detail=contributions to API query
                msg='Session indico_id format: %s'%(indico_id)
                event_id = re.search('(.+?)(s)', indico_id).group(1)
                session_id=re.search('(.+?)(s)([0-9]+)', indico_id).group(3)
            elif rex.match(indico_id):
                # Append ?detail=sessions to API query
                msg='Subcontribution indico_id format: %s'%(indico_id)
                event_id = re.search('(.+?)(c)', indico_id).group(1)
                contribution_id=re.search('([0-9]+)(c)([0-9]+)(sc)([0-9]+)', indico_id).group(3)
                subcontribution_id=re.search('([0-9]+)(c)([0-9]+)(sc)([0-9]+)', indico_id).group(5)
            elif rey.match(indico_id) or re.compile("^(a[0-9]+)([c|s])?([a-zA-Z0-9]*)+$").match(indico_id):
                csv_file = csv.reader(open('/mnt/media_share/media_data/IndicoIds.csv', "r"), delimiter=",")
                #Lets match the complex aEVENT ids: a00116cs1t5,a02333_pt1,a032441c1,a033725_part1,a02206cs1t10,a021060cs1t4sc0,a021198s4,a031647cs0t1
                indico_id=re.search("^(a[0-9]+)([c|s])?([a-zA-Z0-9]*)+$",indico_id).group(1)
                #loop through the csv list        
                event_id = None
                msg="Indico event %s NOT found in IndicoIds.csv file, for resource %s"%(event_id,indico_id)
                for row in csv_file:
                    #if current rows 2nd value is equal to input, print that row
                    if indico_id == row[0]:
                        event_id=row[1]
                        msg="Indico event %s (%s) found in IndicoIds.csv file, for resource %s"%(event_id,row,indico_id)
                        break
            else:
                msg='Any other NOT-ALLOWED indico_id format: %s. Must be xx or xxcyy or xxcyysczz'%(indico_id)
                verbose_print(msg)
                exit()


            #print("Event id:%s. Session id: %s. Contribution id:%s. Subcontribution id:%s"%(event_id,session_id, contribution_id, subcontribution_id))
            indico_object = Indico(config_file)
            if contribution_id!=None or subcontribution_id!=None or session_id!=None or args.mode!=None:
                if (args.mode=='simple'):
                    mode=None
                elif args.mode=='auto':
                    if (subcontribution_id!=None or session_id!=None):
                        mode='sessions'
                    elif(contribution_id!=None):
                        mode='contributions'
                    else:
                        mode='sessions'
                else:
                    mode = args.mode
                response = indico_object.get_event(event_id, detail=mode)
            else:
                response = indico_object.get_event(event_id, detail=None)
            if(response['error']==False):
                if (response['value'] and 'results' in response['value'] and len(response['value']['results'])>0):
                    msg = 'Event "%s" found: %s'%(event_id, response['value']['results'][0]['title'])
                    verbose_print(msg)
                    logger.info(msg)
                    if (args.mode=='simple'):
                        event=response['value']['results'][0]
                        data = dict(
                                year=event['startDate']['date'].split('-')[0],
                                event_id=event['id'],
                                db_id='',
                                title=event['title'],
                                creator=event['creator']['email'],
                                category=event['categoryId'],
                                description=event['description']
                        )
                        print(data)
                    elif (args.mode=='contributions' or args.mode=='sessions' or args.mode=='auto'):
                        contribution_json={'title':None,'description':None,'friendly_id':None,'db_id':None,'db_id_full':None}
                        subcontribution_json={'title':None,'description':None,'friendly_id':None,'db_id':None,'db_id_full':None}
                        event=response['value']['results'][0]
                        #append all the contributions:
                        s_db_id=None
                        if (contribution_id!=None or session_id!=None):
                            # First level
                            #print(response['value']['results'])
                            found=False
                            for i in response['value']['results']:
                                #print(i)
                                if 'sessions' in i:
                                    for session_json in (i['sessions']):
                                        #print(session_json['title'])
                                        # This is a session. Do not search for contributions
                                        if (session_id!=None): 
                                            if ('session' in session_json and (session_id=="%s"%(session_json['session']['friendly_id']))):
                                                s_db_id="%s"%(session_json['session']['db_id'])
                                                found=True
                                                break                                            
                                        # This is a contribution or subcontribution. Just search inside
                                        elif (contribution_id!=None):                                            
                                            #if ('session' in session_json and (contribution_id=="%s"%(session_json['session']['friendly_id']))):
                                            #    contribution_json=session_json['session']
                                            #    break
                                            for contribution_json in (session_json['contributions']):
                                                c=contribution_json["friendly_id"]
                                                #print(c)
                                                if (contribution_id=="%s"%(c) or session_id=="%s"%(c)):
                                                    if subcontribution_id!=None and 'subContributions' in contribution_json:
                                                        for subcontribution_json in contribution_json['subContributions']:
                                                            #print(subcontribution_json["friendly_id"])
                                                            if (subcontribution_id=="%s"%(subcontribution_json["friendly_id"])):
                                                                if not 'description' in subcontribution_json:
                                                                    subcontribution_json['description']=contribution_json["description"]
                                                                    found=True
                                                                break                                 
                                                    break                                     
                                                else:
                                                    contribution_json={'title':None,'description':None,'friendly_id':None,'db_id':None,'db_id_full':None}
                                if 'contributions' in i and not found:
                                    for contribution_json in (i['contributions']):
                                        c=contribution_json["friendly_id"]
                                       #print(c)
                                        if (contribution_id=="%s"%(c) or session_id=="%s"%(c)):
                                            if subcontribution_id!=None and 'subContributions' in contribution_json:
                                                    for subcontribution_json in contribution_json['subContributions']:
                                                        #print(subcontribution_json["friendly_id"])
                                                        if (subcontribution_id=="%s"%(subcontribution_json["friendly_id"])):
                                                            if not 'description' in subcontribution_json:
                                                                subcontribution_json['description']=contribution_json["description"]
                                                                found=True
                                                            break   
                                            break
                                        else:
                                            contribution_json={'title':None,'description':None,'friendly_id':None,'db_id':None,'db_id_full':None}
                        # Direct event. One sessions will match the indico_id:
                        else:
                            for i in response['value']['results']:
                                if 'sessions' in i:
                                    for session_json in (i['sessions']):
                                        if (event_id=="%s"%(session_json['conference']['id'])):
                                            c=session_json['id']
                                            db_id=c
                                            #print("%s,%s"%(c,session_json['conference']))

                                #if 'contributions' in i:
                                #    for contribution_json in (i['contributions']):
                                #        c=contribution_json["friendly_id"]
                                #        print("%s,%s"%(c,contribution_json["title"]))

                        full_description = event['title']
                        if contribution_json['title'] != None: 
                            text_string = BeautifulSoup(contribution_json['title'], "lxml").text
                            clean_text = unicodedata.normalize("NFKD",text_string) #.encode("ascii", "ignore").decode()
                            contribution_json['title']=clean_text.replace("\r","").replace("\t","").replace("\n"," ").replace("’",'').replace("'",'')
                            full_description += ". " + contribution_json['title']
                        if subcontribution_json['title'] != None:
                            text_string = BeautifulSoup(subcontribution_json['title'], "lxml").text
                            clean_text = unicodedata.normalize("NFKD",text_string) #.encode("ascii", "ignore").decode()
                            subcontribution_json['title']=clean_text.replace("\r","").replace("\t","").replace("\n"," ").replace("’",'').replace("'",'')
                            full_description += ". " + subcontribution_json['title']
                        if event['description'] != None:
                            text_string = BeautifulSoup(event['description'], "lxml").text
                            clean_text = unicodedata.normalize("NFKD",text_string) #.encode("ascii", "ignore").decode()
                            event['description']=clean_text.replace("\r","").replace("\t","").replace("\n"," ").replace("’",'').replace("'",'').replace("•","")
                            full_description += ". " + event['description']
                        if contribution_json['description'] != None:
                            text_string = BeautifulSoup(contribution_json['description'], "lxml").text
                            clean_text = unicodedata.normalize("NFKD",text_string) #.encode("ascii", "ignore").decode()
                            contribution_json['description']=clean_text.replace("\r","").replace("\t","").replace("\n"," ").replace("’",'').replace("'",'').replace("•","")
                            full_description += ". " + contribution_json['description']
                        full_description=full_description.replace("\n","").replace("\t","").replace("\r","").replace("'",'')
                        full_description=BeautifulSoup(full_description, "lxml").text                        
                        contribution=dict(
                            year=event['startDate']['date'].split('-')[0],
                            event_id=event['id'],
                            db_id=db_id,
                            title=event['title'],
                            creator=event['creator']['email'],
                            category=event['categoryId'],
                            s_db_id=s_db_id,
                            _contribution_id=contribution_json['friendly_id'],
                            _db_id=contribution_json["db_id"],
                            _title=contribution_json['title'],
                            __subcontribution_id=subcontribution_json['friendly_id'],
                            __db_id=subcontribution_json["db_id"],
                            __title=subcontribution_json['title'],
                            description=event['description'],
                            _description=contribution_json["description"],
                            full_description = full_description
                        )
                        #print(json.dumps(contribution).replace("null","\"null\"").encode("ascii", "ignore").decode())
                        print(contribution)
                        #print(json.dumps(contribution).replace("null","\"null\""))
                    else:
                        print(response['value'])
                else:
                    msg = 'Unexpected response for event "%s". Unable to continue: %s'%(event_id, response['value'])
                    print(response['value'])
                    verbose_print(msg)
                    logger.error(msg)                          
            else:
                msg = 'Event "%s" doesnt exist. Unable to continue: %s'%(event_id, response['value'])
                print(response['value'])
                verbose_print(msg)
                logger.error(msg)                
    elif (args.action == 'legacy_get_events'):
        if not(args.serie):
            msg = 'Missing param/s: -serie required'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            result=[]
            indico_object = Indico(config_file)
            response = indico_object.get_events(args.serie)
            if(response['error']==False):
                if (response['value']):            
                    msg = 'Some events for serie "%s" were found: %s'%(args.serie, response['value']['results'][0]['id'])
                    for event in response['value']['results']:
                        str="%s/%s:%s"%(event['startDate']['date'].split('-')[0],event['id'],event['title'])
                        full_description=BeautifulSoup(event['description'].replace("\n","").replace("\t","").replace("\r","").replace("'",''), "lxml").text
                        data = dict(
                                year=event['startDate']['date'].split('-')[0],
                                event_id=event['id'],
                                title=event['title'],
                                creator=event['creator']['email'],
                                category=event['categoryId'],
                                description=event['description'],
                                full_description=full_description
                            )
                        result.append(data)          
                        verbose_print(str)
                    verbose_print(msg)
                    logger.info(msg)
                    print(result)
            else:
                msg = 'Serie "%s" doesnt exist. Unable to continue: %s'%(args.serie, response['value'])
                verbose_print(msg)
                logger.error(msg)
    elif (args.action == 'ttaas_ingest'):
        if not(args.path):
            msg = 'Missing media file -path that is required'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            if not(args.params):
                msg = 'Missing additional params: -params required'
                verbose_print(msg)
                logger.error(msg)
                return
            ttaas_object = TtaaS(config_file)
            if (args.description):
                title=args.description
            else:
                title='Untitled'
            response = ttaas_object.ingest_event(args.path, args.params)
            if response['value']:
                msg = 'Event "%s" ingested:\n%s'%(args.path, response['value'])
                verbose_print(msg)
                logger.info(msg)
                if (response['error']==True):
                    data = response
                else:        
                    data = json.loads(response['value'])
                print(data)
            else:
                msg = 'Event "%s" not ingested. Unable to continue: %s'%(args.path, response)
                verbose_print(msg)
                logger.error(msg)
                print(response)
    elif (args.action == 'ttaas_update'):
        if not args.id or not args.params:
            msg = 'Missing id or additional params:-id, -params required'
            verbose_print(msg)
            logger.error(msg)
            return
        ttaas_object = TtaaS(config_file)
        response = ttaas_object.update_event(args.id, args.params)
        if response['value']:
            msg = 'Event "%s" updated:\n%s'%(args.id, response['value'])
            verbose_print(msg)
            logger.info(msg)
            if (response['error']==True):
                data = response
            else:        
                data = json.loads(response['value'])
            print(data)
        else:
            msg = 'Event "%s" not updated. Unable to continue: %s'%(args.id, response)
            verbose_print(msg)
            logger.error(msg)
            print(response)
    elif (args.action == 'ttaas_translate'):
        if not args.id:
            msg = 'Missing id: -id required'
            verbose_print(msg)
            logger.error(msg)
            return
        ttaas_object = TtaaS(config_file)
        response = ttaas_object.request_translation(args.id)
        if response['value']:
            msg = 'Event "%s" sent for translation:\n%s'%(args.id, response['value'])
            verbose_print(msg)
            logger.info(msg)
            if (response['error']==True):
                data = response
            else:        
                data = json.loads(response['value'])
            print(data)
        else:
            msg = 'Event "%s" not sent for translation. Unable to continue: %s'%(args.id, response)
            verbose_print(msg)
            logger.error(msg)
            print(response)                       
    elif (args.action == 'ttaas_get'):
        if not(args.id):
            msg = 'Missing media id.'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            ttaas_object = TtaaS(config_file)
            response = ttaas_object.get_event(args.id)
            if response['value']:
                msg = 'Event "%s" retrieved:\n%s'%(args.path, response['value'])
                verbose_print(msg)
                logger.info(msg)                
                data = json.loads(response['value'])
                print(data)
            else:
                msg = 'Event "%s" not retrieved. Unable to continue: %s'%(args.path, response['value'])
                verbose_print(msg)
                logger.error(msg)                                  
        # Get the CES Service URL from config file:
        #response=str(delete_event(args.id))
        #verbose_print('Event deleted: '+response['value'])
        #if(response['error']==False):
        #    print(response['value'])
        #else:
        #    print('Error')
    elif (args.action == 'ttaas_check'):
        if not(args.id):
            msg = 'Missing media id.'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            ttaas_object = TtaaS(config_file)
            response = ttaas_object.check_event(args.id)
            if response['value']:
                msg = 'Event "%s" retrieved:\n%s'%(args.path, response['value'])
                verbose_print(msg)
                logger.info(msg)                
                data = json.loads(response['value'])
                print(data)
            else:
                msg = 'Event "%s" not retrieved. Unable to continue: %s'%(args.path, response['value'])
                verbose_print(msg)
                logger.error(msg)  
    elif (args.action == 'ttaas_langdetect'):
        if not(args.description):
            msg = 'Missing -desc description for the language detection .'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            try:
                import spacy
                from spacy.language import Language
                import en_core_web_sm
                nlp = en_core_web_sm.load()
                #nlp = spacy.load("en_core_web_sm")
                Language.factory("language_detector", func=get_lang_detector)
                nlp.add_pipe('language_detector', last=True)
                doc = nlp(args.description)
                # document level language detection. Think of it like average language of document!
                print(doc._.language)
            except ImportError as error:
                # Output expected ImportErrors: ModuleNotFoundError
                #print(error.__class__.__name__)
                print("{'language': None, 'score': 0}")
            except Exception as exception:
                # Output unexpected Exceptions.
                #print(exception, False)
                #print(exception.__class__.__name__ + ": " + exception.message)
                print("{'language': None, 'score': 0}")
    elif (args.action == 'indico_link'):
        if not(args.id or args.name or args.path):
            msg = 'Missing param/s: -id, -name, -path required (-id is the Indico database Identifier)'
            verbose_print(msg)
            logger.error(msg)
            return
        else:
            indico_object = Indico(config_file)
            response = indico_object.create_link(args.id, args.name, args.path)
            ok=False
            if(response['error']==True):
                msg='Something happened:%s'%(response['error-msg'])
                verbose_print(msg)
                logger.error(msg)
            else:
                msg='Link created/updated:%s'%(response['value'])
                verbose_print(msg)
                logger.info(msg)  
def get_lang_detector(nlp, name):
    try:
        from spacy_langdetect import LanguageDetector
    except ImportError as error:
        # Output expected ImportErrors.
        print(error.__class__.__name__)
        return
    except Exception as exception:
        # Output unexpected Exceptions.
        print(exception, False)
        print(exception.__class__.__name__ + ": " + exception.message)
        return 
    #return LanguageDetector(seed=42)  # We use the seed 42
    return LanguageDetector()  # We use the seed 42

def log(myArgs):
   global logger
   logger.debug('Opencast call to: octools.py ' + myArgs)

if __name__ == "__main__":
   main()
   log(' '.join(sys.argv[1:]))
