#!/bin/bash

type="WebLectures"
dir="/mnt/import_share"
echo `echo $1`;

function usage()
{
    echo "Count dirs from org/dir params; if --type=both, count WebLectures and Conferences in two columns.\nAdd 2>/dev/null to avoid error messages."
    echo ""
    echo "./count_dirs.sh"
    echo "--org=$org"
    echo "--type=$type"
    echo ""
}


while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --org)
            dir=$VALUE
            ;;
        --type)
            type=$VALUE
            ;;                   
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

echo "ORG is $org";
echo "TYPE is $type";

echo "$dir"

if [ "$type" == "both" ]; then
    for entry in `ls $dir/Conferences`; do
        cmd=`find $dir/Conferences/$entry/* -maxdepth 0 -type d | wc -l`
        cmd2=`find $dir/WebLectures/$entry/* -maxdepth 0 -type d | wc -l 2>/dev/null`
        echo "$entry,$cmd,$cmd2"
    done
else
    for entry in `ls $dir/$type`; do
        cmd=`find $dir/$type/$entry/* -maxdepth 0 -type d | wc -l`
        echo "$entry,$cmd"
    done
fi