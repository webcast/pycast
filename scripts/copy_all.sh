#!/bin/bash

# Copy many files:
#   copy_all.sh --org=/mnt/import_share/WebLectures --dst=/mnt/media_share/media_data --from=2019 --to=2020 --max=2 --app=0000 --type=WebLectures
# --id=735431c34
#   copy_all.sh --org=/mnt/import_share/WebLectures --dst=/mnt/media_share/media_data --from=1999 --to=2014 --max=2 --app=0000 --type=WebLectures
# Do not copy: Only generate .conf files
#   copy_all.sh --org=/mnt/import_share/Conferences --dst=/mnt/media_share/media_data --from=1999 --to=2014 --max=2000 --app=0000 --type=Conferences
# Only copy: But don't generate .conf files
#   copy_all.sh --dst=/mnt/media_share/media_data --from=1999 --to=2014 --copy=1 --max=10 --type=Conferences
# Only copy one id: But don't generate .conf files
#   copy_all.sh --org=/mnt/import_share/WebLectures/2019 --dst=/mnt/media_share/media_data/2019 --copy=1 --id=865784c1
#   copy_all.sh --org=/mnt/import_share/Conferences --dst=/mnt/media_share/media_data --copy=1 --from=2001 --to=2001 --max=2000 --type=Conferences
#   copy_all.sh --org=/mnt/import_share/Conferences/2001 --dst=/mnt/media_share/media_data/2001 --copy=1 --id=a032636c1


id="0"
max=10
org=""
dst=""
copy="0"
conf="0"
app="0"
from=1999
to=1999
type='WebLectures'
mlp="0"

function usage()
{
    echo "Copy media resources; generate apache .conf file and data.json"
    echo ""
    echo "./copy_many.sh"
    echo "--id=$id"
    echo "--max=$max"
    echo "--from=$from"
    echo "--to=$to"
    echo "--type=$type"
    echo "--org=$org"
    echo "--dst=$dst"
    echo "--copy=$copy"
    echo "--conf=$conf"
    echo "--app=$app"    
    echo "--mlp=$mlp"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --id)
            id=$VALUE
            ;;
        --max)
            max=$VALUE
            ;;            
        --org)
            org=$VALUE
            ;;
        --dst)
            dst=$VALUE
            ;;
        --from)
            from=$VALUE
            ;;
        --to)
            to=$VALUE
            ;;
        --type)
            type=$VALUE
            ;;              
        --copy)
            copy=$VALUE
            ;;
        --conf)
            conf=$VALUE
            ;;
        --app)
            app=$VALUE
            ;;  
        --mlp)
            mlp=$VALUE
            ;;           
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

echo "ID is $id";
echo "MAX is $max";
echo "ORG is $org";
echo "DEST is $dst";
echo "FROM is $from";
echo "TO is $to";
echo "TYPE is $type";
echo "COPY is $copy";
echo "CONF is $conf";
echo "APP is $app";
echo "MLP is $mlp";

#while [ "$n" -le "$num" ]; do

app_arg=""
conf_arg=""

if [ "$app" != "0" ]; then
  app_arg="-app $app"
fi
if [ "$mlp" == "0" ]; then
  mlp_arg=""
else
  mlp_arg="-mlp"
fi




if [ "$id" == "0" ]; then
  for ((i=$to; i >= $from ; i--)); do
    n=0
    for entry in `ls $org/$i`; do
      if [ "$n" -gt "$max" ]; then
        break
      fi

      if [ "$conf" != "0" ]; then
        conf_arg="-conf_dir $dst/$i/config $app_arg"
      fi
      echo $conf_arg
      if [ "$copy" == "0" ]; then
        media.py -c /etc/pycast/pycast.cfg  -action transfer -dest_dir $dst/$i/$entry -v -data $mlp_arg $app_arg $conf_arg
        #-conf_dir $dst/$i/config $app_arg  # $conf_arg
      else
        media.py -c /etc/pycast/pycast.cfg  -action transfer -org_dir $org/$i/$entry -dest_dir $dst/$i/$entry -v -data $mlp_arg $app_arg $conf_arg
        #-conf_dir $dst/$i/config $app_arg # $conf_arg
      fi
      n=`expr "$n" + 1`;
      echo "$n:$entry"
    done
    cat $dst/$i/config/info.log >> $dst/info.log 2>/dev/null
    mv $dst/$i/config/info.log $dst/$i/config/info-$i-$type.log 2>/dev/null
    cat $dst/$i/config/error.log >> $dst/error.log 2>/dev/null
    mv $dst/$i/config/error.log $dst/$i/config/error-$i-$type.log 2>/dev/null

  done
else
  if [ "$conf" != "0" ]; then
    conf_arg="-conf_dir $dst/$i/config $app_arg"
  fi
  echo $conf_arg
  if [ "$copy" == "0" ]; then
    media.py -c /etc/pycast/pycast.cfg  -action transfer -dest_dir $dst/$id -v -data $mlp_arg $app_arg $conf_arg
    #-conf_dir $dst/config $app_arg
  else
    media.py -c /etc/pycast/pycast.cfg  -action transfer -org_dir $org/$id -dest_dir $dst/$id -v -data $mlp_arg $app_arg $conf_arg
    #-conf_dir $dst/config $app_arg
  fi
  cat $dst$i/config/info.log >> $dst/info.log 2>/dev/null
  mv $dst$i/config/info.log $dst/$i/config/info-$i-$type.log 2>/dev/null
  cat $dst$i/config/error.log >> $dst/error.log 2>/dev/null
  mv $dst$i/config/error.log $dst/$i/config/error-$i-$type.log 2>/dev/null
fi
#done
