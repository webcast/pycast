#!/usr/bin/python3.6
import os
from os import system
import inspect
import sys
import logging
import logging.config
import getopt
from argparse import ArgumentParser
from configparser import RawConfigParser
from xml.etree.ElementTree import parse
import json
from os import path
from datetime import date, datetime
import io
import ntpath
import shutil
import yaml
import re
import distutils
import csv
import subprocess
import difflib

from cern.config import *
from cern.openid_class import Oid

# add '../' to system path
CMD_FOLDER = os.path.realpath(
    os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0])
)
CMD_SUBFOLDER = os.path.realpath(
    os.path.abspath(
        os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], "../")
    )
)

if CMD_SUBFOLDER not in sys.path:
    sys.path.insert(0, CMD_SUBFOLDER)

from cern.opencast_class import Opencast
from cern.indico_class import Indico
from cern.ces_class import CES
from cern.mllp_class import MLLP
from cern.transform_class import TR
from cern.tools_class import TOOLS

from distutils import dir_util

_CONF_FILE = configuration_file("../pycast.cfg")


def verbose_print(*args):
    # Print each argument separately so caller doesn't need to
    # stuff everything to be printed into a single string
    for arg in args:
        print(arg),
    print


def ensure_dir(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def mount(path_to_local_dir):
    # retcode = subprocess.call(["/usr/bin/mount", path_to_local_dir])
    retcode = system("sudo /usr/bin/mount " + path_to_local_dir)
    return retcode


def unmount(path_to_local_dir):
    retcode = system("sudo /usr/bin/umount " + path_to_local_dir)
    return retcode


def main():
    global logger
    parser = ArgumentParser(description="Media args")
    parser.add_argument(
        "-action", dest="action", required=True, help="Action to execute"
    )
    parser.add_argument("-org_dir", dest="org_dir", default=None, help="Origin path")
    parser.add_argument(
        "-dest_dir", dest="dest_dir", default=None, help="Destination path"
    )
    parser.add_argument(
        "-conf_dir", dest="conf_dir", default=None, help="Destination for config file."
    )
    parser.add_argument(
        "-conf_type",
        dest="conf_type",
        default="closed",
        help="ACL rights ( open | closed (only opencast-admins) | ces ). ces is default.",
    )

    parser.add_argument(
        "-service", dest="service", default=None, help="Service section."
    )
    parser.add_argument(
        "-app",
        dest="app",
        default=None,
        help="OpenID application for dynamically creating the role (egroup).",
    )
    parser.add_argument(
        "-data",
        action="store_true",
        dest="data",
        default=False,
        help="Copy all files from origin. Metadata files will always be copied (xml + smil). Gen data file (data.json file for player).",
    )
    parser.add_argument(
        "-manifest",
        action="store_true",
        dest="manifest",
        default=False,
        help="Gen data file (data.json file for player). It doesnt matter if -data is True or False",
    )
    parser.add_argument(
        "-mlp",
        action="store_true",
        dest="mlp",
        default=False,
        help="Get subtitles files from MLLP servers(.srt or .ttml (dfxp, vtt, text, json) files.",
    )
    parser.add_argument(
        "-mlp_prefix",
        dest="mlp_prefix",
        default="lhcp2020-test-",
        help="MLLP prefix for the uploaded transcription contributions.",
    )

    parser.add_argument(
        "-s", dest="new_state", help="workflow state for notification purposes."
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="print all the results",
    )
    parser.add_argument(
        "-dr",
        action="store_true",
        dest="dry_run",
        default=False,
        help="Only test mode. Not further processing.",
    )

    parser.add_argument(
        "-c",
        dest="config_file",
        action="store",
        default=_CONF_FILE,
        help="configuration file",
    )
    args = parser.parse_args()
    # check verbose
    if args.verbose is False:
        global verbose_print
        verbose_print = lambda *a: None  # do-nothing function

    # check config file
    config_file = args.config_file
    if not path.isfile(config_file):
        verbose_print('[ERROR] Config file: "%s" does not exists.' % (config_file))
        # Load default values
        # update_configuration(config_file)
        exit(1)
    else:
        # read logging configuration file path
        config = RawConfigParser()
        config.read(config_file)

    logging_conf_file = config.get("Logging", "path")
    if path.isabs(logging_conf_file):
        logging_conf_file = path.abspath(logging_conf_file)
    else:
        conf_base_dir = path.dirname(config_file)
        logging_conf_file = path.join(conf_base_dir, logging_conf_file)

    # check logging config file
    if not path.isfile(logging_conf_file):
        verbose_print(
            '[Error] Logging config file "%s" not exists.' % (logging_conf_file)
        )
        exit(1)

    with open(logging_conf_file, "r") as f:
        config_logger = yaml.safe_load(f.read())
        logging.config.dictConfig(config_logger)

    logger = logging.getLogger(__name__)

    # Dry-Run, test mode.
    if args.dry_run is True:
        # Simple arg checking
        logging.info(args.action)
        if args.org_dir:
            verbose_print('[INFO] (test-mode). Origin "%s"' % (args.org_dir))
        else:
            verbose_print("[INFO] (test-mode)")
        return 0

    #################################################################################
    #####                                                                       #####
    #####                                                                       #####
    #####                                                                       #####
    #################################################################################
    # verbose_print('Action:'+args.action)
    if args.action == "transfer":
        if args.dest_dir:
            # Mount the media share !
            retcode = mount(config.get("Media", "org_dir"))
            if retcode:
                if retcode == 32:
                    msg = "Already mounted."
                elif retcode == 1:
                    msg = "Does {} exists?".format(config.get("Media", "org_dir"))
                else:
                    msg = "Unknown error."
                verbose_print("Error mounting resource: {}".format(msg))
                logging.error("Error unmounting resource: {}".format(msg))
            else:
                logging.info(
                    "Resource successfully mounted: {}".format(
                        config.get("Media", "org_dir")
                    )
                )
            indico_id = os.path.basename(os.path.normpath(args.dest_dir))
            parent = os.path.dirname(os.path.normpath(args.dest_dir))
            location = os.path.basename(parent)
            dest_path = args.dest_dir
            logging.info("-dest_dir provided")
            if len(args.dest_dir.split("/")) == 2:
                dest_path = config.get("Media", "dest_dir") + "/" + args.dest_dir
                ensure_dir(dest_path)
                indico_id = args.dest_dir.split("/")[1]
                location = args.dest_dir.split("/")[0]
            if args.org_dir:
                org_path = args.org_dir
                logging.info("-org_dir provided")
                # Two dirs (year/id) or three dirs (Conferences|WebLectures/year/id) provided
                # Get the base path from config dir
                if (len(args.org_dir.split("/")) == 2) or (
                    len(args.org_dir.split("/")) == 3
                ):
                    org_path = config.get("Media", "org_dir") + "/" + args.org_dir
                if os.path.isdir(org_path):
                    # Implicit copy
                    logging.info("-org_path:" + org_path + ". Implicit copy.")
                    logging.info(
                        "-data : %s. %s"
                        % (
                            str(args.data),
                            (
                                "Copying all files"
                                if args.data == True
                                else "Copying only metadata files"
                            ),
                        )
                    )
                    # Create -dest_path if not exists
                    ensure_dir(dest_path)
                    ensure_dir(os.path.join(dest_path, "thumbs"))
                    # List -org_dir files and copy
                    dirs = os.listdir(org_path)
                    # This would copy all the files and directories
                    for file in dirs:
                        # if dir, create new dir and copy contents nested
                        if os.path.isdir(org_path + "/" + file):
                            ensure_dir(dest_path + "/" + file)
                            if args.data:
                                # Copy full content, not file level checking. Sub-fodlers will be also copied
                                dir_util.copy_tree(
                                    org_path + "/" + file,
                                    dest_path + "/" + file,
                                    preserve_mode=0,
                                    preserve_symlinks=0,
                                    update=1,
                                    verbose=1,
                                )
                                logging.info(
                                    "Copying dir:"
                                    + org_path
                                    + "/"
                                    + file
                                    + " to "
                                    + dest_path
                                    + "/"
                                    + file
                                )
                        else:
                            # Only copy .xml .json, .smil files or everything if explicitily parsed -data True
                            if args.data or (
                                file.endswith(".xml")
                                or file.endswith(".json")
                                or file.endswith(".smil")
                            ):
                                shutil.copyfile(
                                    org_path + "/" + file, dest_path + "/" + file
                                )
                                logging.info(
                                    "Copying file:"
                                    + org_path
                                    + "/"
                                    + file
                                    + " to "
                                    + dest_path
                                    + "/"
                                    + file
                                )
                                # Also copy the jpg files to the thumbs folder to unify weblectures and conferences model
                                # Weblectures uses TeoPlayer with the thumbs folder and Conferences take them from the main folder
                                if file.endswith(".jpg") or file.endswith(".png"):
                                    verbose_print(
                                        "Copying file:"
                                        + org_path
                                        + "/"
                                        + file
                                        + " to "
                                        + dest_path
                                        + "/thumbs/"
                                        + file
                                    )
                                    shutil.copyfile(
                                        org_path + "/" + file,
                                        "/" + dest_path + "/thumbs/" + file,
                                    )
                else:
                    logging.info("-org_dir:" + org_path + " is not a dir.")
            else:
                logging.info("-org_dir arg not provided. Don't copy.")
            # Find info.xml
            if os.path.isdir(dest_path):
                logging.info(
                    "-dest_dir arg provided. Gen data.json from xml, json and smil files."
                )
                if (args.data is True) or (args.manifest is True):
                    logging.info(
                        "-data arg provided. Gen data.json to dest dir:%s" % (dest_path)
                    )
                    id = os.path.basename(os.path.normpath(dest_path))
                    tr = TR(config_file)
                    if os.path.isfile(dest_path + "/info.xml"):
                        # It is a two media resource
                        if os.path.isfile(dest_path + "/lecture.xml"):
                            if os.path.isfile(dest_path + "/" + id + ".smil"):
                                msg = (
                                    "Three media resources found: lecture.xml, info.xml and %s.smil files found in: %s"
                                    % (id, dest_path)
                                )
                                result = tr.xml_2_json(
                                    config_file,
                                    "xml",
                                    dest_path,
                                    "lecture.xml",
                                    "info.xml",
                                    id + ".smil",
                                )
                            else:
                                msg = (
                                    "Missing %s.smil. info.xml and lecture.xml files found in: %s"
                                    % (id, dest_path)
                                )
                                result = tr.xml_2_json(
                                    config_file,
                                    "xml",
                                    dest_path,
                                    "lecture.xml",
                                    "info.xml",
                                    None,
                                )
                        else:
                            if os.path.isfile(dest_path + "/" + id + ".smil"):
                                msg = "Missing lecture.xml. Info file found: %s" % (
                                    dest_path + "/info.xml"
                                )
                                result = tr.xml_2_json(
                                    config_file,
                                    "xml",
                                    dest_path,
                                    None,
                                    "info.xml",
                                    id + ".smil",
                                )
                            elif os.path.isfile(dest_path + "/lecture.json"):
                                msg = "Missing lecture.xml. lecture.json found: %s" % (
                                    dest_path + "/lecture.json"
                                )
                                result = tr.xml_2_json(
                                    config_file,
                                    "xml",
                                    dest_path,
                                    "lecture.xml",
                                    "info.xml",
                                    None,
                                )
                            else:
                                # Get all the information from the XML file
                                msg = (
                                    "Missing lecture.xml and %s.smil. Only info file found: %s. Lets try with this."
                                    % (id, dest_path + "/info.xml")
                                )
                                result = tr.xml_2_json(
                                    config_file,
                                    "xml",
                                    dest_path,
                                    None,
                                    "info.xml",
                                    None,
                                )
                                # result = {'value' : '', 'error' : True, 'error-msg' : 'Missing lecture.xml and .smil file.'}
                        logging.info(msg)
                        if result["error"] == True:
                            logging.error("Error:%s" % (result["error-msg"]))
                        else:
                            logging.info("Result:%s" % (result["error-msg"]))
                        # Check if file exists to make a copy
                        if os.path.isfile(dest_path + "/data.json"):
                            rename = (
                                "data-"
                                + datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
                                + ".json"
                            )
                            os.rename(
                                dest_path + "/data.json", dest_path + "/" + rename
                            )
                            logging.info("File backed-up: %s/%s" % (dest_path, rename))

                        with open(dest_path + "/data.json", "w") as file_object:
                            file_object.write(result["value"])

                        # Create the data.v2.json file
                        create_datav2_json(dest_path, result["value"])

                        logging.info("Saving file: %s/data.json" % (dest_path))
                        # Gen data.json from file
                    elif os.path.isfile(dest_path + "/" + id + ".smil"):
                        # It is a two media resource
                        msg = (
                            "One -podcast.mp4 & -mobile.mp4 file type: Info file found: %s"
                            % (dest_path + "/" + id + ".smil")
                        )
                        logging.info(msg)
                        # Gen data.json from file
                    else:
                        # Only smil file (dir_desktop_camera.smil)
                        if os.path.isfile(
                            dest_path + "/" + id + "_desktop_camera.smil"
                        ):
                            msg = (
                                "One media resource: Not info file but smil file found: %s"
                                % (dest_path + "/" + id + "_desktop_camera.smil")
                            )
                        else:
                            msg = (
                                "No media resource found: Nor info.xml, neither %s"
                                % (dest_path + "/" + id + "_desktop_camera.smil")
                            )
                        logging.info(msg)
                    # logging.info("Test file: %s/info.txt"%(dest_path))
                    # with open(dest_path+'/info.txt', "w") as file_object:
                    #    file_object.write(msg)
                if args.mlp is True:
                    logging.info(
                        "-mlp arg provided. Getting subtitles from MLLP server, and saving to dest dir:%s"
                        % (dest_path)
                    )
                    tlp = MLLP(config_file)
                    id = os.path.basename(os.path.normpath(dest_path))
                    # dst_file = dest_path+"/"+id+".en.dfxp"
                    langs = tlp.get_langs(id)
                    jlangs = []
                    jlangs.append(json.loads(langs))
                    for jlang in jlangs:
                        for lng in jlang["langs"]:
                            verbose_print(lng["lang_code"])
                            dst_file = dest_path + "/" + id + "_" + lng["lang_code"]
                            # tlp.get_subs(id, lng["lang_code"], 'srt', dst_file+".srt") # dfxp extended type 0
                            # tlp.get_subs(id, lng["lang_code"], 'ttml', dst_file+".dfxp") # ttml type 1
                            # Finally only vtt
                            tlp.get_subs(
                                id, lng["lang_code"], "vtt", dst_file + ".vtt", True
                            )  # ttml type 1
                            logging.info("Saving %s" % (dst_file + ".vtt"))
                    # For reviewing and loading only translated languages through get_langs call
                    ##tlp.get_subs(args.mlp_prefix+id, "en", 'dfxp', dst_file) # dfxp extended type 0
                    # tlp.get_subs(args.mlp_prefix+id, "en", 'ttml', dst_file) # ttml type 1
                    # logging.info("Saving english dfxp subtitles:%s"%(dst_file))
                    # dst_file = args.dest_dir+"/"+id+".en.srt"
                    # tlp.get_subs(args.mlp_prefix+id, "en", 'srt', dst_file)
                    # logging.info("Saving english srt subtitles:%s"%(dst_file))

                # Notify CES if required:
                # Now done from Opencast Workflow operation
                # if (args.new_state=='notify'):
                #    logging.info("Notification of transfer to CES")
                #    ces_object = CES(config_file)
                #    response = ces_object.set_state(indico_id, 'processing-finished', headers='yes')
                #    if response['error'] is True:
                #        msg = 'Can not notify CES %s: Error %s.' % (indico_id, response['error-msg'])
                #        logging.error(msg)
                #    else:
                #        msg = 'CES has been notified %s: Msg %s.' % (indico_id, response['value'])
                #        logging.info(msg)

            else:
                logging.info("dest_path:" + dest_path + " is not a dir.")
            # Unmount the media share !
            retcode = unmount(config.get("Media", "org_dir"))
            if retcode:
                if retcode == 32:
                    msg = "{} not mounted.".format(config.get("Media", "org_dir"))
                else:
                    msg = "Unknown error."
                verbose_print("Error unmounting resource: {}".format(msg))
                logging.error("Error unmounting resource: {}".format(msg))
            else:
                logging.info(
                    "Resource successfully unmounted: {}".format(
                        config.get("Media", "org_dir")
                    )
                )
        else:
            logging.info("-dest_dir arg NOT provided. Nowhere to copy files")

    #################################################################################
    #####                                                                       #####
    #####                                                                       #####
    #####                                                                       #####
    #################################################################################
    # verbose_print('Action:'+args.action)
    if args.action == "legacy":
        # Try the arg passed first
        if args.dest_dir:
            # Mount the media share !
            retcode = mount(config.get("Media", "legacy_path"))
            if retcode:
                if retcode == 32:
                    msg = "Already mounted."
                elif retcode == 1:
                    msg = "Does {} exists?".format(config.get("Media", "legacy_path"))
                else:
                    msg = "Unknown error."
                verbose_print("Error mounting resource: {}".format(msg))
                logging.error("Error unmounting resource: {}".format(msg))
            else:
                logging.info(
                    "Resource successfully mounted: {}".format(
                        config.get("Media", "legacy_path")
                    )
                )
            indico_id = os.path.basename(os.path.normpath(args.dest_dir))
            logging.info("indico_id: %s" % (indico_id))
            parent = os.path.dirname(os.path.normpath(args.dest_dir))
            logging.info("parent: %s" % (parent))
            location = os.path.basename(parent)
            logging.info("location: %s" % (location))
            if len(args.dest_dir.split("/")) == 2:
                indico_id = args.dest_dir.split("/")[1]
                location = args.dest_dir.split("/")[0]
                verbose_print("event_type not provided through -dest_dir")
                dest_path = (
                    config.get("Media", "legacy_path")
                    + "/"
                    + location
                    + "/"
                    + indico_id
                )
            elif len(args.dest_dir.split("/")) == 3:
                indico_id = args.dest_dir.split("/")[2]
                location = args.dest_dir.split("/")[1]
                event_type = args.dest_dir.split("/")[0]
                verbose_print("Future use: %s" % (event_type))
                # Put evet_type before locations
                dest_path = (
                    config.get("Media", "legacy_path")
                    + "/"
                    + location
                    + "/"
                    + indico_id
                )
            else:
                dest_path = args.dest_dir
                # The os.path.dirname(of_the parent) is also the event_type
                event_type = args.dest_dir.split("/")[
                    (len(args.dest_dir.split("/")) - 3)
                ]
                logging.info("-dest_dir provided: %s" % (args.dest_dir))
                verbose_print("Future use: %s" % (event_type))
            if args.org_dir:
                org_path = args.org_dir
                logging.info("-org_dir provided")
                # Two dirs (year/id) or three dirs (Conferences|WebLectures/year/id) provided
                # Get the base path from config dir
                if (len(args.org_dir.split("/")) == 2) or (
                    len(args.org_dir.split("/")) == 3
                ):
                    # The org_dir for exporting the media files to DFS is the CEPHfs desr_dir
                    org_path = config.get("Media", "dest_dir") + "/" + args.org_dir
            else:
                # Default CEPHs dir
                org_path = (
                    config.get("Media", "dest_dir") + "/" + location + "/" + indico_id
                )
                logging.info(
                    "-org_dir arg not provided. Defaulting to %s." % (org_path)
                )
            if os.path.isdir(org_path):
                # Implicit copy
                logging.info("-org_path:" + org_path + ". Implicit copy.")
                logging.info("-dest_path:" + dest_path)
                # Create -dest_path if not exists
                ensure_dir(dest_path)
                # List -org_dir files and copy
                dirs = os.listdir(org_path)
                # This would copy all the files and directories
                for file in dirs:
                    # if dir, create new dir and nest action
                    if os.path.isdir(org_path + "/" + file):
                        ensure_dir(dest_path + "/" + file)
                        # Copy full content, not file level checking
                        dir_util.copy_tree(
                            org_path + "/" + file,
                            dest_path + "/" + file,
                            preserve_mode=0,
                            preserve_symlinks=0,
                            update=1,
                            verbose=1,
                        )
                        logging.info(
                            "Copying dir:"
                            + org_path
                            + "/"
                            + file
                            + " to "
                            + dest_path
                            + "/"
                            + file
                        )
                    else:
                        # Copy all files
                        shutil.copyfile(org_path + "/" + file, dest_path + "/" + file)
                        logging.info(
                            "Copying file:"
                            + org_path
                            + "/"
                            + file
                            + " to "
                            + dest_path
                            + "/"
                            + file
                        )
                # Copy also the apache .conf file (in config directory) to legacy root/config in order to apply DFS ACLs
                apache_conf = (
                    config.get("Media", "dest_dir")
                    + "/"
                    + location
                    + "/config/"
                    + indico_id
                    + ".conf"
                )
                # The folder is the parent of the year/event folder=event_type
                # Added year after event_type (Weblectures|Conferences) for avoiding new root folder creation: apache_dest = /Conferences/2021/config/id.conf
                apache_dest = (
                    config.get("Media", "legacy_path")
                    + "/"
                    + event_type
                    + "/"
                    + location
                    + "/config/"
                    + indico_id
                    + ".conf"
                )
                # Create the dest folder if it doesnt exist:
                ensure_dir(
                    config.get("Media", "legacy_path")
                    + "/"
                    + event_type
                    + "/"
                    + location
                    + "/config"
                )
                shutil.copyfile(apache_conf, apache_dest)
                logging.info(
                    "Copying Apache conf file:"
                    + apache_conf
                    + " to legacy "
                    + apache_dest
                )
            else:
                logging.info("-org_path:" + org_path + " is not a dir.")
            # Unmount the media share !
            retcode = unmount(config.get("Media", "legacy_path"))
            if retcode:
                if retcode == 32:
                    msg = "{} not mounted.".format(config.get("Media", "legacy_path"))
                else:
                    msg = "Unknown error."
                verbose_print("Error unmounting resource: {}".format(msg))
                logging.error("Error unmounting resource: {}".format(msg))
            else:
                logging.info(
                    "Resource successfully unmounted: {}".format(
                        config.get("Media", "legacy_path")
                    )
                )
        else:
            logging.error("-dest_dir not provided.")

    #################################################################################
    #####                                                                       #####
    #####                                                                       #####
    #####                                                                       #####
    #################################################################################
    elif args.action == "acl":
        logging.info("-action provided: acl.")
        if args.dest_dir:
            indico_id = os.path.basename(os.path.normpath(args.dest_dir))
            parent = os.path.dirname(os.path.normpath(args.dest_dir))
            location = os.path.basename(parent)
            dest_path = args.dest_dir
            logging.info("-dest_dir provided")
            # if len(dest_dir)=2 only Location/id parsed. Complete the path from config file
            if len(args.dest_dir.split("/")) == 2:
                dest_path = config.get("Media", "dest_dir") + "/" + args.dest_dir
                indico_id = args.dest_dir.split("/")[1]
                location = args.dest_dir.split("/")[0]
        else:
            dest_path = config.get("Media", "dest_dir")
            logging.info("-dest_dir not provided. Abort!!")
            indico_id = "unknown"
            location = "unknown"
            return
            # Get path from config_file
        logging.info(
            "Location/year=(%s) and indico_id/dir=(%s) from path: %s."
            % (location, indico_id, dest_path)
        )
        # If -conf_dir not provided, get from config_file + location/year
        if args.conf_dir:
            conf_dir = args.conf_dir
            # Get acl.json from Indico or CES API-Rest Service
            logging.info(
                "-conf_dir arg provided. Gen %s.conf to dest dir:%s"
                % (indico_id, args.conf_dir)
            )
        else:
            logging.info(
                "-conf_dir arg NOT provided. Get from config_file[Media][dest_dir]/Location."
            )
            conf_dir = config.get("Media", "dest_dir") + "/" + location + "/config"
        # Create conf dir if it doesn't exist
        ensure_dir(conf_dir)
        if os.path.isdir(conf_dir):
            # Check type based on indico_id format
            # When only numbers it is a event
            # When number+c+number it is a contribution
            # When number+c+number+s+number it is a sub-contribution, also called session
            # First: Clean the string (remove after: _, - , . )
            indico_org = None
            indico_object = Indico(config_file)
            try:
                indico_org = indico_id
                indico_id = re.search("(.+?)([\-\.])", indico_id).group(1)
            except AttributeError:
                found = ""  # apply your error handling
            rex = re.compile("^[0-9]+c[0-9]+sc[0-9]+$")
            rey = re.compile("^a[0-9]+$")
            # Lets split id_underscore events:
            # 27380c27_part2,23575_Part1_Main,23575_Part6_Michael_Yoo,31954c29a,46758-2009_part4,50273_Sartorius-en,50273_Sartorius-fr,
            # 50273_part1b,58217_part9,69338_LHCb,43170_LHCFest_DG_and_Lanting,
            if len(indico_id.split("_")) > 0:
                indico_id = indico_id.split("_")[0]
                # Gen the appropriate acl update command
            if indico_id.isdigit():
                logging.info("Direct event indico_id format: %s" % (indico_id))
                event_id = indico_id
            elif re.match(r"[0-9]+c[0-9]+$", indico_id):
                # Append ?detail=contributions to API query
                logging.info("Contribution indico_id format: %s" % (indico_id))
                event_id = re.search("(.+?)(c)", indico_id).group(1)
            elif rex.match(indico_id):
                # Append ?detail=sessions to API query
                logging.info("Subcontribution indico_id format: %s" % (indico_id))
                event_id = re.search("(.+?)(c)", indico_id).group(1)
            elif rey.match(indico_id) or re.compile(
                "^(a[0-9]+)([c|s])?([a-zA-Z0-9]*)+$"
            ).match(indico_id):
                csv_file = csv.reader(
                    open("/mnt/media_share/media_data/IndicoIds.csv", "r"),
                    delimiter=",",
                )
                # Lets match the complex aEVENT ids: a00116cs1t5,a02333_pt1,a032441c1,a033725_part1,a02206cs1t10,a021060cs1t4sc0,a021198s4,a031647cs0t1
                indico_id = re.search(
                    "^(a[0-9]+)([c|s])?([a-zA-Z0-9]*)+$", indico_id
                ).group(1)
                # loop through the csv list
                event_id = None
                msg = (
                    "Indico event %s NOT found in IndicoIds.csv file, for resource %s"
                    % (event_id, indico_id)
                )
                for row in csv_file:
                    # if current rows 2nd value is equal to input, print that row
                    if indico_id == row[0]:
                        event_id = row[1]
                        msg = (
                            "Indico event %s (%s) found in IndicoIds.csv file, for resource %s"
                            % (event_id, row, indico_id)
                        )
                        logging.info(
                            "Indico event %s (%s) found in IndicoIds.csv file, for resource %s"
                            % (event_id, row, indico_id)
                        )
                        indico_id = event_id
            else:
                event_id = None
                msg = (
                    "Any other NOT-ALLOWED indico_id format: %s. Appending to error_log file"
                    % (indico_id)
                )
                logging.error(msg)
                # if not os.path.exists(conf_dir+'/error.log'):
                #    os.mknod(conf_dir+'/error.log')
                # with open(conf_dir+'/error.log', "a") as file_object:
                #    file_object.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+','+indico_id+', ,'+indico_org+'\n')
                # Gen the open gen file anyway. Use original name, not truncated:
                config = indico_object.get_config(
                    "open", location, indico_org, None, None, None, None
                )
                with open(conf_dir + "/" + indico_org + ".conf", "w") as file_object:
                    file_object.write(config)
                return msg
            # if not os.path.exists(conf_dir+'/info.log'):
            #    os.mknod(conf_dir+'/info.log')
            # with open(conf_dir+'/info.log', "a") as file_object:
            #    file_object.write(datetime.now().strftime("%Y-%m-%d %H:%M:mlp%S")+','+indico_id)
            # Get dir from path (args.dest_dir or conf_file[Indico][dest_dir]+year+id)
            # if (os.path.isdir(dest_path)):
            #    with open(dest_path+'/info.txt', "a") as file_object:
            #        file_object.write(indico_id)
            logging.info("Get ACL for: %s/%s.conf" % (location, indico_org))
            users = []
            groups = []
            nousers = []
            externals = []
            emails = []
            nogroups = []
            if args.conf_type == "open":
                apache_config = indico_object.get_config(
                    "open", location, indico_org, None, None, None, None
                )
            elif args.conf_type == "closed":
                apache_config = indico_object.get_config(
                    "closed", location, indico_org, users, groups, externals, emails
                )
            elif args.conf_type == "ces":
                ces_object = CES(config_file)
                response = ces_object.get_acl(indico_id, headers="yes")
                # We can also get the ACL directly from Indico
                # response = indico_object.get_acl(event_id)

                if response["error"] is True:
                    logging.error(
                        "Can not get indico acl %s: Error %s."
                        % (indico_id, response["error-msg"])
                    )
                    # if not os.path.exists(conf_dir+'/error.log'):
                    #    os.mknod(conf_dir+'/error.log')
                    # with open(conf_dir+'/error.log', "a") as file_object:
                    #    file_object.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+',Not acl,'+indico_id+','+event_id+','+indico_org+'\n')
                else:
                    # print(response['value']['result'])
                    logger.info(
                        "CES event acl %s retrieved: %s."
                        % (indico_id, response["error-msg"])
                    )
                    users = response["value"]["result"]["allowed"]["users"]
                    tmpgroups = response["value"]["result"]["allowed"]["groups"]
                    others = response["value"]["result"]["allowed"]["externals"]
                    # Check group. Replace LDAP "CERN Users" by grappa Group "cern-accounts" to align with Indico emulation
                    groups = []
                    for item in tmpgroups:
                        group = str(item)
                        if group == "CERN Users":
                            verbose_print("CERN Users. Replace by cern-accounts")
                            groups.append("cern-accounts")
                        else:
                            verbose_print("Include group: %s" % (group))
                            groups.append(group)
                    # Not found users are also valid users, but only those that are not @cern.ch
                    # Check them
                    tmpusers = response["value"]["result"]["not_found"]["users"]
                    emails = []
                    ignore = []
                    # Remove @cern.ch from not_found
                    for item in tmpusers:
                        email = str(item)
                        if email.endswith("@cern.ch"):
                            verbose_print("Cern email, ignore: %s" % (email))
                            ignore.append(email)
                        else:
                            verbose_print("External email, include: %s" % (email))
                            emails.append(email)
                    logging.info("Allowed: Users:%s - Groups:%s" % (users, groups))
                    logging.info("Emails = Not_found: Users:%s " % (emails))
                    logging.info("Ignore = Not_found: Users:%s " % (emails))
                    # Create sepparate groups for uids and emails
                    for item in others:
                        externals.append(item["uid"])
                        emails.append(item["email"])
                        verbose_print(
                            "Uid: %s, Email: %s" % (item["uid"], item["email"])
                        )
                    logging.info(
                        "All allowed: Externals:%s - Emails:%s" % (externals, emails)
                    )
                    # Short arrays to get always the same order in the conf file
                    # For future use
                    # users.sort()
                    # groups.sort()
                    # emails.sort()
                    # externals.sort()
                    nousers = response["value"]["result"]["not_found"]["users"]
                    nogroups = response["value"]["result"]["not_found"]["groups"]
                    logging.info(
                        "Other users not found in CES: Users:%s - Groups:%s"
                        % (nousers, nogroups)
                    )
                if (not users) and (not groups) and (not externals) and (not emails):
                    logging.info("Empty list!!! Use open template")
                    apache_config = indico_object.get_config(
                        "open", location, indico_org, None, None, None, None
                    )
                else:
                    apache_config = indico_object.get_config(
                        "closed", location, indico_org, users, groups, externals, emails
                    )
                    # Also create the OpenID role by calling the openid.create_role method class
                    # If app not parsed, get it from config_file[Media][apps]
                    if args.app:
                        logger.info("-app arg parsed. Checking OpenId roles")
                        oid_app = args.app
                    else:
                        apps = config.get(
                            "Media", "apps"
                        ).split()  # apo:0000:1976-2019 bakony:2020:2020-2025
                        # default values
                        oid_app = "2021"
                        balancer = "montblanc"  # Oprional param. Not needed
                        for app in apps:
                            min = app.split(":")[2].split("-")[0]
                            max = app.split(":")[2].split("-")[1]
                            if location >= min and location <= max:
                                balancer = app.split(":")[0]
                                oid_app = app.split(":")[1]
                        logging.info(
                            "Balancer:%s.cern.ch,App:webcast_%s, based on year:%s"
                            % (balancer, oid_app, location)
                        )
                        # logger.info("-app missing. Get app from config_file, based on year:%s"%(location))
                        # app = '2021'
                    my_oid = Oid(config_file, oid_app, None)
                    for group in groups:
                        # Create the role (same as group): retrieves the role_id if it exists
                        role_id = my_oid.get_role_id(group, group)
                        group_id = my_oid.get_group_id(group)
                        # Add the egroup to the role_id
                        my_oid.create_role_group(role_id, group_id)
            # The file to process is the original indico_id => indico_org, not the transformed!
            logging.info("Gen file: %s/%s.conf from template" % (conf_dir, indico_org))
            # Check if file exists to compare, and make a copy if it has changed
            conf_file = conf_dir + "/" + indico_org + ".conf"
            if os.path.isfile(conf_file):
                f = open(conf_dir + "/" + indico_org + ".conf", "r")
                a = f.read().strip().splitlines()
                b = apache_config.strip().splitlines()
                if a != b:
                    verbose_print("=== Path:\t" + conf_file)
                    logger.debug("=== Path:\t" + conf_file)
                    file_date = datetime.utcfromtimestamp(os.path.getctime(conf_file))
                    result = ""
                    for line in difflib.unified_diff(
                        a,
                        b,
                        fromfile="Original:",
                        tofile="New file:",
                        fromfiledate=file_date.strftime("%Y-%m-%d %H:%M:%S"),
                        tofiledate=datetime.today().strftime("%Y-%m-%d %H:%M:%S"),
                        lineterm="",
                    ):
                        result += "%s\n" % (line)
                    verbose_print(result)
                    logger.debug(result)
                    rename = (
                        indico_org
                        + "-"
                        + datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
                        + ".bak"
                    )
                    os.rename(conf_file, conf_dir + "/" + rename)
                    logging.info("File backed-up: %s/%s" % (conf_dir, rename))

                    with open(
                        conf_dir + "/" + indico_org + ".conf", "w"
                    ) as file_object:
                        file_object.write(apache_config)
                    logging.info("Save file 1: %s.conf" % (indico_org))

                    # Create an .htaccess file with the content of the .conf file
                    create_htaccess_file(dest_path, indico_org, apache_config)

                else:
                    logging.info(
                        "No changes. No need to save file: %s.conf" % (indico_org)
                    )
                f.close()
            else:
                with open(conf_dir + "/" + indico_org + ".conf", "w") as file_object:
                    file_object.write(apache_config)
                logging.info("Save file 2: %s.conf" % (indico_org))

                # Create an .htaccess file with the content of the .conf file
                create_htaccess_file(dest_path, indico_org, apache_config)

                # logging.info("Saving file: %s.conf"%(indico_id))
                # with open(conf_dir+'/info.log', "a") as file_object:
                #    chain=str(users)+','+str(groups)+','+str(nousers)+','+str(nogroups)
                #    file_object.write(','+chain+'\n')

        else:
            logging.info("-conf_dir: " + conf_dir + " is not a dir.")
    #################################################################################
    #####                                                                       #####
    #####                                                                       #####
    #####                                                                       #####
    #################################################################################
    # verbose_print('Action:'+args.action)
    if args.action == "upload":
        # Try the arg passed first
        if args.org_dir:
            if args.service:
                if not path.isfile(args.org_dir):
                    verbose_print(
                        '[ERROR] Org file: "%s" does not exists.' % (config_file)
                    )
                    # Load default values
                    # update_configuration(config_file)
                    exit(1)
                # Gest last part of the org_dir
                head, tail = ntpath.split(args.org_dir)
                file = os.path.splitext(tail)[0]
                extension = os.path.splitext(tail)[1]
                verbose_print(file)
                try:
                    url = (
                        config.get("Upload", args.service + "_server")
                        + "/"
                        + config.get("Upload", args.service + "_path")
                        + "/"
                        + tail
                    )
                except:
                    logging.error("-service section not found")
                    verbose_print("-service section not found")
                    # Lets provoke error to let Opencast know
                    exit(1)
                verbose_print("Upload source: %s to %s" % (args.org_dir, url))
                tools = TOOLS(config_file)
                res = tools.upload(
                    url,
                    args.org_dir,
                    config.get("Upload", args.service + "_user"),
                    config.get("Upload", args.service + "_passwd"),
                )
                if res["error"] == False:
                    logging.info("Result upload %s: %s" % (args.org_dir, res["value"]))
                else:
                    logging.warning("Result upload: %s" % (res["error-msg"]))
            else:
                logging.warning("-service: Not provided")
                verbose_print("-service: Not provided")
        else:
            logging.warning("-org_dir: Not defined. Arg required")
            verbose_print("-org_dir: Not defined. Arg required")


def create_htaccess_file(dest_dir_path, indico_id, data):
    logging.info("Creating .htaccess file for " + indico_id)
    htaccess_file = dest_dir_path + "/" + ".htaccess"

    # Remove the opening <Location tag from the data string using a regex
    data = re.sub(r"<Location.*?>", "", data)

    # Remove the closing </Location> tag from the data string using a regex
    data = re.sub(r"</Location>", "", data)

    # Add <FilesMatch "^(?!data\.v2\.json).*$"> at the beginning of the file
    data = '<FilesMatch "^(?!data\.v2\.json).*$">\n' + data + "</FilesMatch>"

    # Remove AuthMerging Or
    data = data.replace("AuthMerging Or", "")

    data = (
        'AuthMerging Or\nAuthType openid-connect\n\n<Files "data.v2.json">\n    Require all granted\n</Files>\n\n'
        + data
    )

    with open(htaccess_file, "w") as file_object:
        file_object.write(data)


def create_datav2_json(dest_dir, data):
    with open(dest_dir + "/data.v2.json", "w") as file_object:
        # Copy the data to a new var
        data_v2 = data
        # Replace https://ocmedia-apo.cern.ch/ with /
        data_v2 = data_v2.replace("https://ocmedia-apo.cern.ch/", "/")
        # Replace https://ocmedia-bakony.cern.ch/ with /
        data_v2 = data_v2.replace("https://ocmedia-bakony.cern.ch/", "/")
        # Replace https://ocmedia-montblanc.cern.ch/ with /
        data_v2 = data_v2.replace("https://ocmedia-montblanc.cern.ch/", "/")
        file_object.write(data_v2)


def log(myArgs):
    global logger
    logger.debug("Media call: media.py " + myArgs)


if __name__ == "__main__":
    main()
    log(" ".join(sys.argv[1:]))
