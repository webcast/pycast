#!/usr/bin/python3
import os
import re
from os import system
from os import listdir
from os import path
import subprocess
import json
import ast
from datetime import date, datetime, timedelta
import time
from operator import itemgetter, truediv
import argparse
import ntpath
import subprocess
import shlex
import sys
import shutil
import atexit
import csv

from lxml.html.clean import Cleaner
from bs4 import BeautifulSoup


def get_ids(indico_id):
    rex = re.compile("^[0-9]+c[0-9]+sc[0-9]+$")
    rey = re.compile("^a[0-9]+$")
    event_type=None
    event_id=None
    session_id=None                
    contribution_id=None
    subcontribution_id=None
    # Lets split id_underscore events: 
    # 27380c27_part2,23575_Part1_Main,23575_Part6_Michael_Yoo,31954c29a,46758-2009_part4,50273_Sartorius-en,50273_Sartorius-fr,
    # 50273_part1b,58217_part9,69338_LHCb,43170_LHCFest_DG_and_Lanting,    
    if (len(indico_id.split("_"))>0):
        indico_id=indico_id.split("_")[0]
        # Gen the appropriate acl update command

    if indico_id.isdigit():
        msg='Direct event indico_id format: %s'%(indico_id)
        event_id=indico_id
        event_type="event"
    elif re.match(r'[0-9]+c[0-9]+$', indico_id):
        # Append ?detail=contributions to API query
        msg='Contribution indico_id format: %s'%(indico_id)
        event_id = re.search('(.+?)(c)', indico_id).group(1)
        contribution_id=re.search('(.+?)(c)([0-9]+)', indico_id).group(3)
        event_type="contribution"
    elif re.match(r'[0-9]+s[0-9]+$', indico_id):
        # Append ?detail=contributions to API query
        msg='Session indico_id format: %s'%(indico_id)
        event_id = re.search('(.+?)(s)', indico_id).group(1)
        session_id=re.search('(.+?)(s)([0-9]+)', indico_id).group(3)
        event_type="session"
    elif rex.match(indico_id):
        # Append ?detail=sessions to API query
        msg='Subcontribution indico_id format: %s'%(indico_id)
        event_id = re.search('(.+?)(c)', indico_id).group(1)
        contribution_id=re.search('([0-9]+)(c)([0-9]+)(sc)([0-9]+)', indico_id).group(3)
        subcontribution_id=re.search('([0-9]+)(c)([0-9]+)(sc)([0-9]+)', indico_id).group(5)
        event_type="subcontribution"
    elif rey.match(indico_id) or re.compile("^(a[0-9]+)([c|s])?([a-zA-Z0-9]*)+$").match(indico_id):
        csv_file = csv.reader(open('/mnt/media_share/media_data/IndicoIds.csv', "r"), delimiter=",")
        #Lets match the complex aEVENT ids: a00116cs1t5,a02333_pt1,a032441c1,a033725_part1,a02206cs1t10,a021060cs1t4sc0,a021198s4,a031647cs0t1
        indico_id=re.search("^(a[0-9]+)([c|s])?([a-zA-Z0-9]*)+$",indico_id).group(1)
        #loop through the csv list        
        event_id = None
        msg="Indico event %s NOT found in IndicoIds.csv file, for resource %s"%(event_id,indico_id)
        for row in csv_file:
            #if current rows 2nd value is equal to input, print that row
            if indico_id == row[0]:
                event_id=row[1]
                event_type="old"
                msg="Indico event %s (%s) found in IndicoIds.csv file, for resource %s"%(event_id,row,indico_id)
                break
    else:
        event_id= None
        event_type="other"
        msg='Any other NOT-ALLOWED indico_id format: %s.'%(indico_id)
    return event_type, event_id, session_id, contribution_id, subcontribution_id, msg

def get_captions(i_absolute_path):
    captions=[]
    failed=False
    if os.path.isfile(i_absolute_path+'/data.json'):
        #match=match+1
        with open(i_absolute_path+'/data.json') as json_file:
            #print(json_file)
            try:
                data = json.load(json_file)
                if 'captions' in data:
                    appended = False
                    for p in data['captions']:
                        #Check that the file exists.
                        if (not appended):
                            appended = True
                        head, tail = ntpath.split(p['url'])                                        
                        langs=dict(
                            lang=p['lang'],
                            found='%s'%(os.path.isfile(os.path.join(i_absolute_path,tail)))
                        )
                        captions.append(langs)
            except ValueError:  # includes simplejson.decoder.JSONDecodeError
                msg="Error loading data.json file for path: %s"%(i_absolute_path+'/data.json')
                failed=True
    return captions

def validateJSON(jsonData):
    try:
        json.loads(jsonData)
    except ValueError as err:
        return False
    return True

def validateOutput(jsonData):
    if not isinstance(jsonData, dict):
        return False    
    #try:
    #    data=ast.literal_eval(jsonData)
    #except ValueError as err:
    #    return False
    return True

def message(content):
  print("Message: %s"%(content))

def sanitize(dirty_html):
    cleaner = Cleaner(page_structure=True,
                  meta=True,
                  embedded=True,
                  links=True,
                  style=True,
                  processing_instructions=True,
                  inline_style=True,
                  scripts=True,
                  javascript=True,
                  comments=True,
                  frames=True,
                  forms=True,
                  annoying_tags=True,
                  remove_unknown_tags=True,
                  safe_attrs_only=True,
                  #safe_attrs=frozenset(['src','color', 'href', 'title', 'class', 'name', 'id']),
                  allow_tags=None
                  #remove_tags=('span', 'font', 'div')
                  )

    return cleaner.clean_html(dirty_html)

def build_events(events, succeded, errors, found, i_event_type, i_serie, year, i_year, i_event_id, i_contribution_id, i_title, i_absolute_path, i_description, i_db_id, i_creator, filesize):
    #if (debug=='True'):
    #    print("%s:%s:%s-%s/%s:%s"%(i_serie,year, i_year,i_event_id,i_contribution_id, i_title))
    streams=[]
    url=''
    file=''
    title='Empty'
    data=dict(metadata=dict(title='Empty'))
    #data['metadata']['title']='Empty'
    msg=''
    failed=False
    if os.path.isfile(i_absolute_path+'/data.json'):
        #match=match+1
        with open(i_absolute_path+'/data.json') as json_file:
            #print(json_file)
            try:
                data = json.load(json_file)
                try:
                    if 'streams' in data and len(data['streams'])>0:
                        dstreams = data['streams'][0]
                        size=0
                        try:
                            if 'mp4' in dstreams['sources']:
                                succeded.append('%s'%(os.path.join(i_absolute_path,'data.json')))
                                sources=dstreams['sources']['mp4']

                                for source in sources:
                                    url = source['src']
                                    file = os.path.join(i_absolute_path,ntpath.split(url)[1])

                                    if file !='' and os.path.exists(file):
                                        size = os.path.getsize(file)
                                        cmd = "ffprobe -v quiet -print_format json -show_streams"
                                        output = shlex.split(cmd)
                                        output.append(file)
                                        # run the ffprobe process, decode stdout into utf-8 & convert to JSON
                                        ffprobeOutput = subprocess.check_output(output).decode('utf-8')
                                        ffprobeOutput = json.loads(ffprobeOutput)
                                        #streams -> codec_type="audio", bit_rate
                                        for stream in ffprobeOutput['streams']:
                                            if stream['codec_type']=='audio':
                                                resource=dict(
                                                    file = file,
                                                    url = url,
                                                    size=size,
                                                    real_duration=round(float(stream['duration'])),
                                                    bit_rate=round(float(stream['bit_rate']))
                                                )
                                                streams.append(resource)
                                        # prints all the metadata available:
                                        #import pprint
                                        #pp = pprint.PrettyPrinter(indent=2)
                                        #pp.pprint(ffprobeOutput)

                                        # for example, find height and width
                                        #length = ffprobeOutput['streams'][0]['duration']
                                if (data['metadata']['title']!=''):
                                    title = data['metadata']['title']
                                else:
                                    title = 'Undefined'
                                duration = data['metadata']['duration']
                                # Search for the captions
                                captions=[]
                                if 'captions' in data:
                                    appended = False
                                    for p in data['captions']:
                                        #Check that the file exists.
                                        if (not appended):
                                            found.append(i_contribution_id)
                                            appended = True
                                        head, tail = ntpath.split(p['url'])                                        
                                        langs=dict(
                                            lang=p['lang'],
                                            found='%s'%(os.path.isfile(os.path.join(i_absolute_path,tail)))
                                        )
                                        captions.append(langs)
                                # Be careful. Replace unspected JSON control chars !
                                # mathlib inserts them like: $B_s\rightarrow D_s^*$ hadronic
                                title = title.replace("\n","\\n")
                                title = title.replace("\r","\\r")
                                #title = title.replace("^","")
                                # Double quote is not expected while it comes from data.json , but maybe it is a valid char from Indico                       
                                title = title.replace("'",'')
                                lang = "" # Indico doesn't know about languages
                                # Get lang from the event title + description (full_description=i_description)
                                #octools.py -c /etc/pycast/pycast.cfg -action ttaas_langdetect -desc "This is english"
                                i_description=i_description.replace("'","")
                                cmd = "octools.py -c /etc/pycast/pycast.cfg -action ttaas_langdetect -desc '%s'"%(i_description)
                                output = str(os.popen(cmd).read())
                                try:
                                    lang =ast.literal_eval(output)
                                except ValueError:
                                    lang = "{'language': None, 'score': 0}"
                                # Sort the streams
                                sorted_streams = sorted(streams, key=lambda k: k['bit_rate'], reverse=True)
                                for element in sorted_streams:
                                    if element['size'] < filesize:
                                        file=element['file']
                                        url=element['url']
                                        size=element['size']
                                        break
                                if (i_creator!=""):
                                    cmd = "octools.py -c /etc/pycast/pycast.cfg -action get-login -id %s"%(i_creator)
                                    output = str(os.popen(cmd).read())
                                    try:
                                        data = output.replace("'",'"')
                                        data= json.loads(data)                            
                                        if data['login']!='not@found' and data['login']!='' and data['login']!=None:
                                            login = data['login'] # Get from ldap'
                                        else:
                                            login = i_creator
                                    except ValueError:                     
                                        login = i_creator
                                else:
                                        login = 'is@empty'
                                event_data = dict(
                                    event_type=i_event_type,
                                    serie=i_serie,
                                    year=year,
                                    i_year=i_year,
                                    event_id=i_event_id,
                                    contribution_id = i_contribution_id,
                                    db_id=i_db_id,
                                    creator = i_creator,
                                    login = login,
                                    # The title cames from de data.json file (contribution), ot the contribution query, not from the general event i_title
                                    # It will also get good/clean characters
                                    title = title,
                                    i_title = i_title,
                                    i_description = i_description,
                                    lang = lang,
                                    duration = duration,
                                    file = file,
                                    size = size,
                                    url = url,
                                    streams = sorted_streams,
                                    captions = captions,
                                    media_id = "",
                                    media_state = "",
                                    processing_state = ""
                                )
                                events.append(event_data)
                                msg="Ok with video source for event (%s): %s"%(i_event_id,i_absolute_path+'/data.json')
                            else:
                                msg="Error. No mp4 in sources (%s): %s"%(i_event_id,i_absolute_path+'/data.json')
                                failed=True                                               
                                sources=[]                                
                        except ValueError:
                            msg="Error getting video source for event (%s): %s"%(i_event_id,i_absolute_path+'/data.json')
                            failed=True                                               
                            sources=[]
                    else:
                        msg="Error getting video stream for event (%s): %s"%(i_event_id,json_file)
                        failed=True
                except ValueError:
                    msg="Error getting video stream for event (%s): %s"%(i_event_id,i_absolute_path+'/data.json')
                    failed=True
                    sources=[]                                                            
            except ValueError:  # includes simplejson.decoder.JSONDecodeError
                msg="Error loading data.json file for event (%s): %s"%(i_event_id,i_absolute_path+'/data.json')
                failed=True
            # Check the filesize
            size = 0
    else:
        file='Not found'
        url='Not found'
        if (i_title!='' and i_title!='null'):
            title=i_title
        else:
            title='Undefined'
        duration = 0
        size = 0
        msg="Error data.json file not found for event (%s): %s"%(i_event_id,i_absolute_path+'/data.json')
        failed=True
    if (failed==True):
        error_data = dict(
            serie=i_serie,
            year=year,
            i_year=i_year,
            event_id=i_event_id,
            contribution_id = i_contribution_id,
            json=os.path.join(i_absolute_path,'data.json'),
            error=msg
        )
        errors.append(error_data)             

    return events, succeded, errors, found, msg

def replace(media_id, relative_path, debug='False'):
    result=[]
    if os.path.isfile(relative_path):
        f = open(relative_path, "r")
        upload = f.read()
        f.close()
        result = json.loads(upload)

        if (media_id!=result['mediaId']):
            msg="Reg. Id changed from %s to %s."%(result['mediaId'],media_id)
            result['mediaId']=media_id
            result['status']="UNKNOWN"
            f = open(relative_path, "w")
            f.write(json.dumps(result))
            f.close()
        else:
            msg="Uoooooou, regs Ids match! Do not replace."
    else:
        msg="TLP file not found."
    if (debug=='True'):
        print("%s - %s"%(relative_path,msg))
            
    return result, 'existing'

def update(media_id, relative_path, debug='False'):
    cmd = "octools.py -c /etc/pycast/pycast.cfg -action ttaas_check -id=%s"%(media_id)     
    output = str(os.popen(cmd).read())    
    #result= ast.literal_eval(json.dumps(output))
    data=output
    data = output.replace("'",'"')
    data = data.replace(": None",': "None"')
    data = data.replace(": False",': "False"')
    data = data.replace(": True",': "True"')                    
    #p = re.compile('(?<!\\\\)\'')
    #result = p.sub('\"', output)
    result = json.loads(data)
    if (media_id==result['mediaId']):
        msg="Reg. Id match. Update."
    else:
        msg="Uoooooou, reg Id does not match! Must be an update"

    f = open(relative_path, "w")
    f.write(data)
    f.close()

    if (debug=='True'):
        print("%s - %s"%(relative_path,msg))
    # Check the result
    if result['state'].startswith('COMPLETED') or result['state']=='ERROR_NOTIFYING_USER':
        msg="Media COMPLETED in the Transcription Service, retrieve captions."
        processing_state='found'
        # Update the state file:
    elif result['state']=='ALREADY_EXISTS':
        msg="Existing media. Should be replaced."
        processing_state='existing'
        # Update the state file:        
    elif(result['state'].startswith('invalid')):
        msg='Retry! Error processing:%s'(result['state'])
        processing_state='error'
    elif result['state'].startswith('ERROR') or result['state'].startswith('FAILED'): #"rcode_description": "Media ID [ legacy_f7b3a75b-fe49-4ca4-940e-1e34522d571 ] does not exist.",
        msg="Error processing"
        processing_state='processing-error'
    elif result['state'].startswith('RUNNING'): #"rcode_description": "Media ID [ legacy_f7b3a75b-fe49-4ca4-940e-1e34522d571 ] does not exist.",
        msg="Processing"
        processing_state='running'        
    else:
        msg="Media not found in the Transcription Service. Maybe it is being processed. Check the upload id? Remove the .tlp file and ingest again?"
        result['state']=='NOT_FOUND'
        processing_state='unknown'
    if (debug=='True'):
        print(msg)        
    return result, processing_state

def error(err_file, method='get',max_retries=3):                  
    if os.path.exists(err_file):
        f = open(err_file, "r")
        file = json.load(f)
        retries = int(file['retries'])
        f.close
    else:
        retries = 0
    if method=='add' and retries<max_retries:
        retries=retries+1        
        f = open(err_file, "w")                            
        f.write('{"retries":"%s"}'%(retries))
        f.close()
    return retries     

def ttaas_update(j, debug='False', dry_run='True'):
    callback = "http://ocmedia-browser.cern.ch:4000/endpoint/?year=%s&id=%s"%(j['year'],j['contribution_id'])
    url = ('https://weblecture-player.web.cern.ch/?year=%s&id=%s'%(j['year'],j['contribution_id'])) #https://weblecture-player.web.cern.ch/?id=932902c44&year=2022
    # Get the ACLs from CES and append them to -parms
    if j['login']=='' or j['login']==None:
        j['login']='unknown'
    if j['title']=='' or j['title']==None or j['title']=="Undefined":
        if (j['i_title']!='' and j['i_title']!=None and j['i_title']!="Undefined" and j['i_title']!='null'):
            title=j['i_title']
        else:
            title='Undefined'
    else:
        title=j['title']
    title=title.replace("'","")
    title=title.replace("\"","")
    cmd = "octools.py -c /etc/pycast/pycast.cfg -action ttaas_update -id=%s -params=\"{'title':'%s','username':'%s','searchTranscriptionIsPublic':'true','notificationUrl':'%s','referenceUrl':'%s'}\""%(j['media_id'],title,j['login'],callback, url)
    #notificationUrl is not available for the PATCH method
    #cmd = "octools.py -c /etc/pycast/pycast.cfg -action ttaas_update -id=%s -params=\"{'title':'%s','username':'%s','searchTranscriptionIsPublic':'true','referenceUrl':'%s'}\""%(j['media_id'],title,j['login'], url)
    if (debug=='True'):  
        print(cmd)
    # Dry-Run, test mode.
    if dry_run=='True':
        print('Do nothing')
    else:            
        output = str(os.popen(cmd).read())
        print(output)

def ttaas_translate(j, debug='False', dry_run='True'):
    cmd = "octools.py -c /etc/pycast/pycast.cfg -action ttaas_translate -id=%s"%(j['media_id'])
    if (debug=='True'):  
        print(cmd)
    # Dry-Run, test mode.
    if dry_run=='True':
        print('Do nothing')
    else:            
        output = str(os.popen(cmd).read())
        if (debug=='True'):
            print(output)

def ttaas_ingest(tlp_path, checkfile, j, filesize, max_retries=3, backup=False, debug='False', dry_run='True'):
    callback = "http://ocmedia-browser.cern.ch:4000/endpoint/?year=%s&id=%s"%(j['year'],j['contribution_id'])
    url = ('https://weblecture-player.web.cern.ch/?year=%s&id=%s'%(j['year'],j['contribution_id'])) #https://weblecture-player.web.cern.ch/?id=932902c44&year=2022
    # Check previous errors:
    # tlp_path is the full path to the record:
    err_file=os.path.join(tlp_path,'errors',"%s-%s.err"%(j['year'],j['contribution_id']))
    backup_file=os.path.join(tlp_path,'backup',"%s-%s.tlp"%(j['year'],j['contribution_id']))
    err_response=os.path.join(tlp_path,'errors',"%s-%s.tlp"%(j['year'],j['contribution_id']))
    if not os.path.exists(tlp_path): os.makedirs(tlp_path)
    if not os.path.exists(os.path.join(tlp_path,'errors')): os.makedirs(os.path.join(tlp_path,'errors'))
    retries = int(error(err_file,'get',3))
    #print("%s-%s/%s"%(tlp_path,retries,max_retries))
    if (int(j['size'])<filesize):                                                 
        if (retries<max_retries):
            # Get the ACLs from CES and append them to -parms
            if j['login']=='' or j['login']==None:
                j['login']='unknown'
            if j['title']=='' or j['title']==None or j['title']=="Undefined":
                if (j['i_title']!='' and j['i_title']!=None and j['i_title']!="Undefined" and j['i_title']!='null'):
                    title=j['i_title']
                else:
                    title='Undefined'
            else:
                title=j['title']
            title=title.replace("'","")
            title=title.replace("\"","")
            cmd = "octools.py -c /etc/pycast/pycast.cfg -action ttaas_ingest -path=%s -params=\"{'title':'%s','language':'%s','username':'%s','notificationMethod':'callback','comments':'---','transcriptionIsPublic':'false','notificationUrl':'%s','referenceUrl':'%s'}\""%(j['file'],title,j['lang']['language'],j['login'],callback, url)
            if (debug=='True'):  
                print(cmd)
            # Dry-Run, test mode.
            if dry_run=='True':
                media_id=""
                processing_state="test"
            else:            
                output = str(os.popen(cmd).read())
                if (output.find('usage') or output.find('error')) and (debug=='True'):
                    print(cmd)
                    print(output)
                # Make a valid JSON
                data = output.replace("'",'"')
                data = data.replace(": None",': "None"')
                data = data.replace(": False",': "False"')
                data = data.replace(": True",': "True"')
                # Check the result
                if (debug=='True'):                    
                    print('File doesnt exist. Send to TTaaS')
                    print(data)
                try:
                    result= json.loads(data)                             
                    if ('error' in result):
                        media_id='unknown'
                        processing_state='failed'     
                        #f = open(err_file, "w")                            
                        #f.write('{"retries":"%s"}'%(retries))
                        #f.close()
                        retries=error(err_file,method='add')
                        f = open(err_response, "w")                            
                        f.write(output)
                        f.close()                                         
                    else:
                        if ('mediaId' in result):
                            media_id=result['mediaId']
                            processing_state = "uploaded"
                        elif 'error_type' in result:
                            media_id=result['media_id']
                            processing_state = result['error_type']
                        else:
                            media_id=""
                            processing_state = "unknown"
                        # The tlp file will be overwritten or backed-up                        
                        if backup and os.path.isfile(checkfile):
                            if not os.path.exists(os.path.join(tlp_path,'backup')):
                                os.makedirs(os.path.join(tlp_path,'backup'))
                            shutil.move(checkfile, backup_file)
                        f = open(checkfile, "w")
                        f.write(data)
                        f.close()
                except ValueError:  # includes simplejson.decoder.JSONDecodeError
                    media_id=""
                    processing_state="invalidJSON"
        else:
            media_id=""
            processing_state="max_retries"
    else:
        media_id=""
        processing_state="max_filesize"                            
    j['media_id']=media_id
    j['processing_state']=processing_state
    if (debug=='True'):                   
        print(j)                 
    return j['media_id'], j['processing_state']

def to_minutes(seconds):
    minutes = seconds // 60
    seconds %= 60
    return "%02dm"%(minutes)

def to_hours(seconds):
    #seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    #return "%d:%02d:%02d"%(hour, minutes, seconds)
    return "%dh %02dm"%(hour, minutes)

def to_days(seconds):
    return timedelta(seconds=seconds)
    #time_obj = time.gmtime(uploaded_duration)
    #resultant_time = time.strftime("%H:%M:%S",time_obj)


def report(jsonData, score):
    summary={'processed':{'count':0,'duration':0,'size':0},'running':{'count':0,'duration':0,'size':0},\
    'error':{'count':0,'duration':0,'size':0},'processing-error':{'count':0,'duration':0,'size':0}, \
    'uploaded':{'count':0,'duration':0,'size':0},'undispatched':{'count':0,'duration':0,'size':0},'unknown':{'count':0,'duration':0,'size':0},\
    'skipped':{'count':0,'duration':0,'size':0},'other':{'count':0,'duration':0,'size':0},'long':{'count':0,'duration':0,'size':0}}
    not_found=[]
    untitled=[]
    types={'event':0,'session':0,'contribution':0,'subcontribution':0,'old':0,'other':0}
    langs={'en':0,'fr':0,'other':0,'poor':0}
    #with open(args.file,"r") as file:
    #    jsonData = json.load(file) 
    for event in jsonData:
        duration=0
        size=0
        if 'streams' in event:
            if len(event['streams'])>0:
                #try:
                if 'real_duration' in event['streams'][0]:
                    duration=event['streams'][0]['real_duration']
                #except ValueError:
                else:
                    duration=0

        if duration=="" or duration==0:
            if event['duration']!="":
                if (duration>21600):
                    summary['long']['size']+=size 
                    summary['long']['count']+=1
                    summary['long']['duration']+=duration
                duration=int(event['duration'])
            else:
                duration=0
                not_found.append(event['contribution_id'])
        if event['size']!="":
            size=int(event['size'])
        else:
            size=0                
        if event['processing_state']=='uploaded':
            index='uploaded'
        elif event['processing_state']=='found':
            index='processed'           
        elif event['processing_state']=='unknown':
            index='unknown'
        elif event['processing_state']=='running':
            index='running'                          
        elif event['processing_state']=='skipped':
            index='skipped' 
        elif event['processing_state']=='undispatched':
            index='undispatched'
        elif event['processing_state']=='processing-error':
            index='processing-error'  
        elif event['processing_state']=='error':
            index='error'                                          
        else:
            index='other'           
        summary[index]['size']+=size 
        summary[index]['count']+=1
        summary[index]['duration']+=duration
        #for t in types:
        #    if t==event['event_type']:
        #        #print('match type:%s'%(event['event_type']))
        #        types[t]+=1
        # Direct increment
        types[event['event_type']]+=1
        if (event['title']=="Undefined" or event['title']==""):
            untitled.append(event['contribution_id'])
        if(event['lang']['language']=='en'):
            langs['en']+=1
        elif event['lang']['language']=='fr' and event['lang']['score']>score:
            langs['fr']+=1
        elif event['lang']['score']>score:
            langs['other']+=1
        else:
            langs['poor']+=1


    total=0
    total_duration=0
    total_size=0
    print("==>=============================================================")
    for i in summary:
        if i!='long':
            total+=summary[i]['count']
            total_duration+=summary[i]['duration']
            total_size+=float(summary[i]['size'])
        summary[i]['minutes']=to_minutes(summary[i]['duration'])
        summary[i]['hours']=to_hours(summary[i]['duration'])
        summary[i]['days']=to_days(summary[i]['duration'])
        print("%s:%s - Duration:%ss = %s = %s = %s \nSize: %s GB %s MB %s KB"%(i.capitalize(),summary[i]['count'], summary[i]['duration'],summary[i]['minutes'], summary[i]['hours'], summary[i]['days'],round(float(summary[i]['size'])/1024/1024/1024),round(float(summary[i]['size']/1024/1024)),round(float(summary[i]['size'])/1024)))
        print("----------------------------------------------------------------")
    print("Total: %s - Duration: %ss = %sm = %s = %s\nSize: %s GB %s MB %s KB"%(total,total_duration,to_minutes(total_duration),to_hours(total_duration), to_days(total_duration),round(total_size/1024/1024/1024),round(total_size/1024/1024),round(total_size/1024)))
    print("----------------------------------------------------------------")
    str=''
    for j in types: str=str+"%ss: %s - "%(j.capitalize(),types[j])
    print(str)
    print("----------------------------------------------------------------")    
    str=''    
    for j in langs: str=str+"%s: %s - "%(j.capitalize(),langs[j])
    print(str)
    print("----------------------------------------------------------------")
    print("Events without duration:%s data.json untitled event:%s"%(len(not_found),len(untitled)))
    print("=============================================================<==")
    print("Not found", file=sys.stderr)
    print(json.dumps(not_found, indent=4), file=sys.stderr)
    #print("Untitled", file=sys.stderr)
    #print(json.dumps(untitled, indent=4), file=sys.stderr)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-serie', dest='serie', required = False)
    parser.add_argument('-year', dest='year', required = False)
    parser.add_argument('-source', dest='source', required = False, default = 'existing', help='[new|existing} Reads events from existing file and updates the processing state but not the metadata.')

    parser.add_argument('-debug', dest='debug', required = False, default = 'False', help='Verbose output')
    parser.add_argument('-rebuild', dest='rebuild', required = False, default = 'False', help='Download captions and rebuild data.json.')
    parser.add_argument('-update', dest='update', required = False, default='False', help='Update metadata if event exixts in TtaaS  (already uploaded).')

    parser.add_argument('-count', dest='processes', required = False, default = 6000, help='Number of items to process')
    parser.add_argument('-dr', dest='dry_run', required = False, default='False', help='Only test mode. Not further processing.')
    args = parser.parse_args()

    #def exit_handler():
    #    print('Exit!')
    
    #atexit.register(exit_handler)

    base_path = "/mnt/media_share/media_data"
    player_url="https://weblecture-player.web.cern.ch"
    events=[]
    if args.processes:
        total=int(args.processes)
    else:
        total=3
    count=0
    match=0
    filesize = 2147483647
    duration =21600
    score=0.85
    indicos=[]
    wrong=[]
    found=[]
    succeded=[]
    errors=[]
    captions=[]
    temp=[]
    purge=[]
    processed=[]
    links=[]
    linked=[]
    longer=[]
    acls=[]

    if (not(args.serie) and not(args.year)):
        print("-serie or -year arg required")
        exit()
    elif(args.serie and args.serie!=None and args.source=='new'):
        json_file=args.serie
        tlp_path = os.path.join("/mnt/media_share/media_tlp/prod/serie",args.serie)
        cmd="octools.py -c /etc/pycast/pycast.cfg -action legacy_get_events -serie %s"%(args.serie)
        #exit(0)
        output = os.popen(cmd).read()
        #print(output)
        purge = ast.literal_eval(output)
    elif(args.year and args.source=='new'):
        json_file=args.year
        tlp_path = os.path.join("/mnt/media_share/media_tlp/prod/year",args.year)  
        path = os.path.join(base_path, str(args.year))
        dirs=[]
        for subdir in os.scandir(path):
            if subdir.is_dir():
                if True: #count<=total: #.True: #count<=total: #True: #count<=20: 
                    indico_org = None
                    indico_id=ntpath.split(subdir)[len(ntpath.split(subdir))-1]
                    #print("%s -> %s"%(indico_id,subdir.path)      )
                    #indico_object = Indico(config_file)         
                    # Filter directory by removing special chars??
                    # Let's the get_ids function decide
                    #try:
                    #    indico_org = indico_id
                    #    indico_id = re.search('(.+?)([\-\._])', indico_id).group(1)
                    #except AttributeError:
                    #    found = '' # apply your error handling
                    event_type, event_id, session_id, contribution_id, subcontribution_id, msg = get_ids(indico_id)
                    if (args.debug=='True'):
                        print("Event Id:%s - %s"%(event_id,msg))
                    if (event_id):
                        indicos.append(event_id)
                        dirs.append([event_id,indico_id,event_type])
                    else:
                        wrong.append(indico_id)
                    count+=1
        #Remove duplicates to know how many different events are processed.
        #res = [*set(dirs)]
        indicos = list(set(indicos))
        #print(purge)
        wrong.append('00000')
        for i in dirs:
            match+=1
            # Check first if the event has been processed by a serie and skip appending
            # The foulder must match the i['category'] value
            #cmd="octools.py -c /etc/pycast/pycast.cfg -action legacy_get_event -mode simple -ev_id %s"%(i[0])
            # Great improvemen! Title from indico event/session/contribution/subcontribution
            # Also the description to search for the language
            # Valids -ev_id: 1223985, 851306c7sc3, 1051841s1, 1039087s9
            mode='auto'
            cmd="octools.py -c /etc/pycast/pycast.cfg -action legacy_get_contribution -ev_id %s -mode %s"%(i[1],mode)
            output = os.popen(cmd).read()
            if (args.debug=='True'):
                print(cmd)
                print(output)
            #if (output.find("'count': 0")):
            #    wrong.append(i[1])
            #else:
            if True: #not i[1].startswith('62928') and not i[1].startswith('86061') and not i[1].startswith('258552')  \
                #and not i[1].startswith('370005'): 
                try:
                    #json_acceptable_string = output.replace("'", "\"")
                    #data = json.loads(json_acceptable_string) #"62928" 86061c3sc0 258552c19sc0 309369 293382 309369
                    #data=output.replace("null","\"null\"").encode("ascii", "ignore").decode()
                    data=ast.literal_eval(output.strip())
                except ValueError as ex:
                    _exc_type, exc_value, exc_traceback = sys.exc_info()
                    print("ERROR: %r" % (exc_value))
                    # traceback.print_tb(exc_traceback)
                    last_tb = exc_traceback
                    while last_tb.tb_next:
                        last_tb = last_tb.tb_next
                    print("Error location: line=%d, col=%d" % (
                        last_tb.tb_frame.f_locals["node"].lineno,
                        last_tb.tb_frame.f_locals["node"].col_offset))  
                    msg="output exception!!"
                    wrong.append(i[1])
                    exit()
                if 'count' in data and data['count']==0:
                    msg="Count=0"
                    wrong.append(i[1])
                else:
                    msg="Count>0"
                    data['contribution_id']=i[1]
                    data['event_type']=i[2]
                    if 'category' in data:
                        path = "/mnt/media_share/media_tlp/prod/serie/%s"%(data['category'])
                        append=True
                        if os.path.exists(path):
                        #for subdir in os.listdir(path):
                            for file in os.listdir(path): #os.path.join(path, subdir)):
                                if file.startswith(args.year+'-'+i[1]):
                                    if (args.debug=='True'):
                                        print("Already processed: %s"%(file))
                                    append=False
                                    break
                            #if append==False:
                            #    break
                        if append:
                            purge.append(data)
                        else:
                            processed.append(i[1])
                    else:
                        purge.append(data)
            else:
                wrong.append(i[1])
            if (args.debug=='True'):
                print(msg)  
        if (args.debug=='True'):
            print(dirs)
            print(wrong)
            print(processed)
            print(purge)
        print("==>==============================================================")
        print("Total dirs found: %s Events:%s"%(count,len(indicos)))
        print("Full event info: %s"%(len(purge)))    
        print("Pre-processed:%s"%(len(processed)))
        print("Wrong dir pattern:%s"%(len(wrong)))
        print("==============================================================<==")            
        #exit()
    elif args.serie:
        json_file=args.serie
        tlp_path = os.path.join("/mnt/media_share/media_tlp/prod/serie",args.serie)
    elif args.year:
        json_file=args.year
        tlp_path = os.path.join("/mnt/media_share/media_tlp/prod/year",args.year) 
    else:
        print("No other options. You can not arrive here.")

    count=0
    match=0

    json_file_full=os.path.join(tlp_path,json_file)

    # Do not create new DB file. Use existing
    # Processing state and captions will be checked into main loop
    # Skip slow jobs: ttaas_langdetect, get-login, legacy_get_contribution for metadata extraction
    if args.source=="existing":
        # Get events from file
        print("==>==============================================================")
        print("Getting events from JSON source file:\n%s.json"%(json_file_full))
        if os.path.isfile(json_file_full+".json"):
            f = open(json_file_full+".json", "r")
            events=json.load(f)
            f.close()    
        else:
            print("Source file does not exist.")
            exit()
        print("==============================================================<==")

    else:
        for i in purge:
            count=count+1
            if 'creator' in i:
                i_creator=i['creator']
            else:
                i_creator='not@found'
            i_event_id=i['event_id']
            i_title=i['title']
            i_serie=str(i['category'])
            i_year=i['year']
            i_path = os.path.join(base_path, str(i_year))
            #print("%s-%s:%s"%(i_year,i_event_id, i_title))    
            # Clean the description to have a simple text:
            #octools does the cleanup before returning the event metadata
            i_description=str(i['full_description'])
            #i_description=str(i['full_description']).replace("\n","").replace("\t","").replace("\r","").replace("'",'')
            #i_description=BeautifulSoup(i_description, "lxml").text
            if True: #count<=20: #int(i['year'])<2020: # and 
                # Build the events object. Many contribs or sesisons with same prefix (event_id)
                if (args.serie):
                    for current_dir, subdirs, files in os.walk( i_path ):
                        for dir in subdirs:
                            if dir==i_event_id or dir.startswith("%ss"%i_event_id) or dir.startswith("%sc"%i_event_id):
                                failed=False
                                relative_path = os.path.join( current_dir, dir )
                                i_absolute_path = os.path.abspath( relative_path )
                                i_contribution_id=ntpath.split(i_absolute_path)[len(ntpath.split(i_absolute_path))-1]
                                year=i_absolute_path.split("/")[len(i_absolute_path.split("/"))-2]
                                i_event_type, event_id, session_id, contribution_id, subcontribution_id, msg = get_ids(i_contribution_id)
                                i_description=i['title']
                                i_db_id=i_contribution_id
                                events, succeded, errors, found, msg = \
                                    build_events(events, succeded, errors, found, i_event_type, i_serie, year, i_year, i_event_id, i_contribution_id, i_title, i_absolute_path, i_description, i_db_id, i_creator, filesize)
                                if (args.debug=='True'):
                                    print(msg)
                # Build the events object 
                elif (args.year):
                    i_event_type=i['event_type']
                    i_contribution_id=i['contribution_id']
                    #i_absolute_path=os.path.join( i_path, i_contribution_id )
                    i_absolute_path=os.path.join(base_path, str(args.year), i_contribution_id)
                    i_db_id=i['event_id']
                    if (i_event_type=='session'):
                        i_title=i['_title']
                        i_db_id=i['s_db_id']
                    elif (i_event_type=='contribution'):
                        i_title=i['_title']
                        i_db_id=i['_db_id']
                    elif (i_event_type=='subcontribution'):
                        i_title=i['__title']
                        i_db_id=i['__db_id'] 
                    # Be careful. Some events are not stored/published in the event year folder!!
                    #Mainly those that start with "a" : 2005 -> "/mnt/media_share/media_data/2006/a056410/data.json"
                    year=args.year
                    events, succeded, errors, found, msg = \
                        build_events(events, succeded, errors, found, i_event_type, i_serie, year, i_year, i_event_id, i_contribution_id, i_title, i_absolute_path, i_description, i_db_id, i_creator, filesize)
                    if (args.debug=='True'):
                        print(i_absolute_path)
                        print(msg)

        if (args.debug=='True'):                    
            print("Count:%s Match:%s"%(count, len(succeded)))
            print("data.json succeded:")
            for j in succeded:    
                print(j)
            print("data.json failed:")
            for k in errors:    
                print(k)    
            print("Captions found:%s"%(found))


    #For ingest automation. Main Processing Loop
    count = 0
    for j in events:
        if (args.debug=='True'):
            print(j)
        # Ensure 2020, 2021 and 2022 years are skipped
        if True: #int(j['year'])==2019: #True: #
            #if (args.debug=='True'):
            #    print("Captions len: %s"%(len(j['captions'])))        
            # Force upload/ingest: if (True): # ??
            #if len(j['captions'])==0:
            count = count+1
            if count>0 and count <=total:
                checkfile = os.path.join(tlp_path,"%s-%s.tlp"%(j['year'],j['contribution_id']))
                backupfile = os.path.join(tlp_path,'backup',"%s-%s.tlp"%(j['year'],j['contribution_id']))
                err_file = os.path.join(tlp_path,'errors',"%s-%s.err"%(j['year'],j['contribution_id']))       
                if (args.debug=='True'):
                    #print('No captions. Ingest or retrieve.')                    
                    print(checkfile)
                # Write here the ttaas media_id, ex: legacy_f7b3a75b-fe49-4ca4-940e-1e34522d5713
                if os.path.isfile(checkfile):
                    f = open(checkfile, "r")
                    upload = f.read()
                    f.close()
                    upload = json.loads(upload)
                    # Check the existing mediaId:
                    # This should replace the media_id by the new existing but it must also be replaced in the .tlp file!!!
                    if (upload['state']=='ALREADY_EXISTS'):
                        #data, processing_state=replace(data['existingMediaId'],checkfile,args.debug)
                        media_id=upload['existingMediaId']   
                    else:      
                        media_id=upload['mediaId']
                    # Update the file from TTaaS
                    data, processing_state=update(media_id, checkfile)


                    if (args.debug=='True'):                    
                        print('File already sent to TTaaS. mediaId: %s State: %s'%(data['mediaId'],data['state']))
                        print(data, flush=True)

                    j['media_id']=data['mediaId']
                    j['media_state']=data['state']
                    j['processing_state']=processing_state
                    msg=''
                    if (processing_state=='found'):
                        # Update?
                        if (args.update=='True'):
                            ttaas_update(j,debug=args.debug, dry_run=args.dry_run)
                        # Create the Indico link???
                        # Is there a way to check if it exists??
                        if (args.serie):
                            if len(j['captions'])==1: # New transcription! Create new link
                                url="%s/?year=%s&id=%s"%(player_url,j['year'],j['contribution_id'])
                                cmd='octools.py -c /etc/pycast/pycast.cfg -action indico_link -name "Video preview" -id %s -path "%s"'%(j['db_id'],url)
                                msg='Create indico link. Completed with one caption!: (%s:%s)?'%(j['db_id'],url)
                                link=dict(
                                    year = j['year'],
                                    contribution_id = j['contribution_id'],
                                    db_id=j['db_id'],
                                    url=url
                                )
                                links.append(link)
                            elif len(j['captions'])>=2: # Already transcripted. There shold have en and fr  
                                msg='Do NOT create indico link. Completed with two captions!: (%s). It should exists'%(j['db_id'])
                                url="%s/?year=%s&id=%s"%(player_url,j['year'],j['contribution_id'])
                                link=dict(
                                    year = j['year'],
                                    contribution_id = j['contribution_id'],
                                    db_id=j['db_id'],
                                    url=url
                                )
                                linked.append(link)                    
                        # Get captions (serie & year)
                        # We should append the captions retrieved to the events item !!
                        i_absolute_path=os.path.join(base_path,j['year'],j['contribution_id'])
                        if (len(j['captions'])==0) and (args.rebuild == 'True'): #True: #Be careful retrieving always!! 
                            cmd = "mllp.py -c /etc/pycast/pycast.cfg -action trs -tlp_id %s -dest_dir %s/%s -year=%s -contribution_id %s"%(media_id,j['year'],j['contribution_id'],j['year'],j['contribution_id'])
                            output = str(os.popen(cmd).read())
                            if (args.debug=='True'):                    
                                print('There are NOT captions. Retrieve captions!')
                                print(cmd)
                                print(output)
                                # Open the data.json file
                                j['captions']=get_captions(i_absolute_path)
                                print(j['captions'])
                        # This is not supposed to happend. In case the previous condition (len(j['captions'])==1)  is removed/replacer by if True.
                        elif len(j['captions'])==1:
                            if (args.rebuild == 'True'):
                                cmd = "mllp.py -c /etc/pycast/pycast.cfg -action trs -tlp_id %s -dest_dir %s/%s -year=%s -contribution_id %s"%(media_id,j['year'],j['contribution_id'],j['year'],j['contribution_id'])
                                output = str(os.popen(cmd).read())
                                if (args.debug=='True'):                    
                                    print('There is one lang captions: %s. Check if translation exists and/or ask for it.'%(j['captions'][0]['lang']))
                                    print(cmd)
                                    print(output)
                                j['captions']=get_captions(i_absolute_path)
                            else:
                                # Request translation (only if not rebuilding data.json):
                                ttaas_translate(j, args.debug, args.dry_run)
                        else:
                            if (args.debug=='True'):            
                                print('There are multiple captions. Do not download new captions!')
                    elif(processing_state=='error'):
                        msg='Error ingesting. Count and retry and ingest again (will get new mediaId).'
                        numerrors = error(err_file,method='get',max_retries=3)
                        if int(numerrors)<3:
                            j['media_id'], j['processing_state']=ttaas_ingest(tlp_path, checkfile,j,filesize=filesize,max_retries=3, backup=True, debug=args.debug, dry_run=args.dry_run)
                    elif(processing_state=='transcription-error'):
                        msg='Error processing. Retry if not retried before'
                        numerrors = error(err_file,method='get',max_retries=3)
                        if not os.path.isfile(backupfile):
                            j['media_id'], j['processing_state']=ttaas_ingest(tlp_path, checkfile,j,filesize=filesize,max_retries=3, backup=True, debug=args.debug, dry_run=args.dry_run)
                    else:
                        msg='Wait! Still rocessing... unknown state'
                    if (args.debug=='True'):
                        print(msg, flush=True)
                elif len(j['captions'])==0:
                    if j['duration']=="":
                        j['duration']=0
                    try:
                        if float(j['duration'])<duration:
                            if j['lang']['language']=='en':
                                msg='English. Always process.Score:%s.'%(j['lang']['score'])
                                j['media_id'], j['processing_state']=ttaas_ingest(tlp_path, checkfile, j, filesize,max_retries=3, backup=False, debug=args.debug, dry_run=args.dry_run)
                            elif j['lang']['language']=='fr' and j['lang']['score']>=score:
                                j['media_id'], j['processing_state']=ttaas_ingest(tlp_path, checkfile, j, filesize,max_retries=3, backup=False, debug=args.debug, dry_run=args.dry_run)
                                msg='French. Process if %s>%s.'%(j['lang']['score'],score)
                                #j['processing_state']="undispatched"
                            else:
                                if score<=0.5: # Not clear the detected language. Lets assume it is English??
                                    msg='Not english and not french. Not clear the dertcted language. Probably it is English??'
                                    j['processing_state']="undispatched"
                                else:
                                    msg='Not english and not french. skip since we dont have the model jet.'
                                    j['processing_state']="undispatched"
                        else:
                            msg='Too long video:%s'%(j['duration'])
                            j['processing_state']="undispatched"                    
                            longer.append(j['contribution_id'])
                        if (args.debug=='True'):
                            print(msg, flush=True)
                    except ValueError as err:
                        msg='Error getting duration:%s - %s'%(j['contribution_id'], j['duration'])
                        if (args.debug=='True'):
                            print(msg, flush=True)                                                   
                else:
                    j['processing_state']="skipped"               
                    if (args.debug=='True'):                   
                        print(j, flush=True)
            #elif len(j['captions'])==1:
            #    if (args.debug=='True'):            
            #        print('There is one lang captions: %s. Skip ingest. Skip download. Check if translation exists and/or ask for it.'%(j['captions'][0]['lang']))
            else:
                j['processing_state']="skipped"
                if (args.debug=='True'):            
                    print('There are multiple captions. Skip ingest')
            # Process ACLs
            if j['contribution_id'].startswith('a') or "_" in j['contribution_id']:
                acl=dict(
                    year = j['year'],
                    contribution_id = j['contribution_id']
                )            
                acls.append(acl)                

    # Events already processed
    print("==>==============================================================")
    print("Events processed. Save to default JSON file:\n%s%s"%(json_file_full,".json"))
    #print(events)
    # Save the JSON file, and backup the existing one:
    if not os.path.exists(tlp_path): os.makedirs(tlp_path)
    if os.path.isfile(json_file_full+".json"):
        rename= json_file_full + "-" + datetime.now().strftime("%Y-%m-%dT%H:%M:%S")+".json"         
        os.rename(json_file_full+".json", rename)
    f = open(json_file_full+".json", "w")
    f.write(json.dumps(events, indent=4))
    f.close() 
    print("==============================================================<==")

    print("==>==============================================================")
    print("Count:%s Match:%s"%(count, match))
    print("Succeded:%s Captions:%s"%(len(succeded), len(found)))
    print("Captions:%s"%(found))
    print("Wrong:%s"%(len(wrong)))
    print("Errors:%s"%(len(errors)))
    print("Long:%s"%(len(longer)))
    print("==============================================================<==")


    report(events, score)

    if (args.serie):
        print("==>==============================================================")
        print("Links:%s"%(len(links)))
        #if (args.debug='True'):
        for link in range(len(links)):
            cmd='octools.py -c /etc/pycast/pycast.cfg -action indico_link -name "Video preview" -id %s -path "%s";'%(links[link]['db_id'],links[link]['url'])
            print(cmd)
        print("Linked:%s"%(len(linked)))
        for link in range(len(linked)):
            print(linked[link]['url'])
        print(json.dumps(links, indent=4))
        print(json.dumps(linked, indent=4))
        print("==============================================================<==")    

    #Already applied. No need to regenerate
    #if (args.year and args.debug=='True'):
    #    print("ACLs:%s"%(len(acls)))    
    #    for acl in range(len(acls)):
    #        cmd='media.py -c /etc/pycast/pycast.cfg -action acl -dest_dir /mnt/media_share/media_data/%s/%s  -v -conf_dir /mnt/media_share/media_data/%s/config -conf_type ces;'%(acls[acl]['year'],acls[acl]['contribution_id'],acls[acl]['year'])
    #        print(cmd)


    #print(json.dumps(events, indent=4))
    #sys.stderr.write(json.dumps(errors, indent=4))
    print("Wrong dir", file=sys.stderr)
    print(json.dumps(wrong, indent=4), file=sys.stderr)
    print("Error processing", file=sys.stderr)
    print(json.dumps(errors, indent=4), file=sys.stderr)
    print("Long events", file=sys.stderr)
    print(json.dumps(longer, indent=4), file=sys.stderr)

if __name__ == "__main__":
   main()
   print(' '.join(sys.argv[1:]))