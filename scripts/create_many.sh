#!/bin/bash

n=1001;
max=1002;
app=2020;
dir=2020;
eg="webcast-support"
path="/mnt/media_share/media_data"

function usage()
{
    echo "Create directories and files. Add roles to an openid app endpoint (year)"
    echo ""
    echo "./create_many.sh"
    echo "--app=$app"
    echo "--egroup=$eg"
    echo "--from=$n"
    echo "--to=$max"
    echo "--path=$path"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --app)
            app=$VALUE
            dir=$VALUE
            ;;
        --egroup)
            eg=$VALUE
            ;;
        --from)
            n=$VALUE
            ;;
        --to)
            max=$VALUE
            ;;
        --path)
            path=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

if [ "$app" -lt 2020 ]
then
  echo "\$Application to 0000."
  app="0000"
fi

echo "DIR is $dir";
echo "APP is $acl";
echo "FROM is $n";
echo "TO is $max";
echo "PATH is $path";

while [ "$n" -le "$max" ]; do
  mkdir -p "$path/$dir/s$n"
  #cp "$path/cern.jpg" "$path/$dir/s$n/."
  echo "<Location /s$n >
        <RequireAll>
            AuthType openid-connect
            #Require ssl
            Require valid-user
            Require claim cern_roles:role_$n
        </RequireAll>
</Location>"  > "$path/$dir/s$n/s$n.conf"
  echo "<!doctype html>
<html>
  <head>
    <title>Dir s$n!</title>
  </head>
  <body>
    <p>Protected for role:<strong>role_$n</strong>.</p>
  </body>
</html>"  > "$path/$dir/s$n/index.html"
  openid.py  -c /etc/opencast/workflows/scripts/config.cfg -oid_app $app -oid_acl_add $n -oid_eg_add $eg
  n=`expr "$n" + 1`;
done
