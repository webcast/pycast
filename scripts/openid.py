#!/usr/bin/python3.6
""" Add an oid role """
import logging
import sys
import re
import os
import subprocess
import json
#from itestools import cesi
from cern.openid_class import Oid
import os
import inspect
import sys
import logging
import logging.config
import getopt
from argparse import ArgumentParser
from configparser import RawConfigParser
from os import path
import datetime
import io
import ntpath
import shutil
import yaml

# add '../' to system path
CMD_FOLDER = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
CMD_SUBFOLDER = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '../')))

if CMD_SUBFOLDER not in sys.path:
    sys.path.insert(0, CMD_SUBFOLDER)

_CONF_FILE = '../config.cfg'


def get_secret_from_teigi(application):
    """ Get the client_id secret from teigi"""
    # Let's do it with a system call (instead of depending on python2)
    secret = subprocess.check_output(["tbag", "show", "client-secret-teigi-key",
                                      "--hg", "streaming/opencast/%s" % application])
    secret_json = json.loads(secret)
    return secret_json['secret']

def check_oid_roles(config_file, cesiobj, application, acl, conf_file, args): #pylint: disable=unused-variable,unused-argument
    """ Check if the acl already has its oids """
    secret = get_secret_from_teigi(application)
    my_oid = Oid(config_file, application, secret)

    if not my_oid:
        return

    my_egroups = get_egroups_from_yaml(application, acl)
    if not my_egroups:
        return
    logging.debug(my_egroups)
    for my_role in my_egroups:
        logging.info("Checking the role %s", my_role)
        role_id = my_oid.get_role_id(my_role)
        if (not role_id or role_id==None):
            logging.error("Error creating the role %s", my_role)
            continue
        info = my_oid.get_role_groups(role_id)
        #First, let's delete the egroups that should not be there
        for egroup in info['data']:
            logging.debug("WHAT TO DO WITH %s", egroup['displayName'])
            if my_egroups[my_role].has_key(egroup['displayName']):
                logging.info("The group %s is there. Good!", egroup['displayName'])
                del my_egroups[my_role][egroup['displayName']]
            else:
                # we should delete it
                logging.info("We should delete the group")
                my_oid.delete_role_group(role_id, egroup['id'])
        # And now, let's add the missing egroups
        for egroup in my_egroups[my_role]:
            logging.info("We have to add %s", egroup)
            group_id = my_oid.get_group_id(egroup)
            if group_id:
                my_oid.create_role_group(role_id, group_id)
            else:
                logging.error("Error looking for the group %s", egroup)
        logging.info("All the groups have been added")

def checkout_it_es_repo():
    """ Checks out the repository of it_es """
    repo_dir = "/tmp/oid_it-puppet-hostgroup-it_es"
    git_url = "https://:@gitlab.cern.ch:8443/ai/it-puppet-hostgroup-it_es.git"
    if not os.path.isdir(repo_dir):
        try:
            git.Repo.clone_from(git_url, repo_dir)
        except Exception as my_ex:  # pylint: disable=broad-except
            logging.error("There was a problem cloning the repo:\n%s", my_ex)
            return

    logging.info("Repo checkout in %s", repo_dir)
    my_git = git.cmd.Git(repo_dir)
    my_git.pull()



def acl_add(config_file, app, acl, secret, egroups):
    """ Adds an acl to the an existing application """
    my_oid = Oid(config_file, app, secret)
    #role_name = "role_%s" % (acl)
    role_name = acl
    role_desc = acl
    role_id = my_oid.get_role_id(role_name, role_desc)
    for group in (egroups):
        group_id = my_oid.get_group_id(group)
        my_oid.create_role_group(role_id, group_id)


def main():
    global logger
    """ let's do it"""
    parser = ArgumentParser(description='OID args')
    parser.add_argument('-oid_app', dest='application', required = True, 
                                action='store', help="Application which the acl should be added")
    parser.add_argument('-cleanup', default=False, dest='cleanup', 
                                action='store_true', help="delete all the roles and groups")

    parser.add_argument('-oid_acl_add', dest='acl',
                                action='store', help="Add an acl to an existing application")
    parser.add_argument('-oid_acl_del', dest='delacl',
                                action='store', help="acl to delete from existing application")

    parser.add_argument('-oid_eg_add', dest='egroups', type=str, #nargs='+',
                                action='store',
                                help="Egroups for the acl (default:webcast-support)") #pylint: disable=line-too-long
    parser.add_argument('-oid_eg_del', dest='delegroups',
                                action='store', help="Egroup to delete from existing acl")

    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='print all the results')
    parser.add_argument('-dr', action='store_true', dest='dry_run', default=False,
                        help='Only test mode. Not further processing.')

    parser.add_argument('-c', dest='config_file', action='store', default=_CONF_FILE,
                        help='configuration file') 
    args = parser.parse_args()

    # check verbose
    if args.verbose is False:
            global verbose_print
            verbose_print = lambda *a: None      # do-nothing function

    # check config file
    config_file = args.config_file
    if not path.isfile(config_file):
            verbose_print('[ERROR] Config file: "%s" does not exists.' % (config_file))
            exit(1)

    # read logging configuration file path
    config = RawConfigParser()
    config.read(config_file)

    logging_conf_file = config.get('Logging', 'path')
    if path.isabs(logging_conf_file):
        logging_conf_file = path.abspath(logging_conf_file) 
    else:
        conf_base_dir = path.dirname(config_file)
        logging_conf_file = path.join(conf_base_dir, logging_conf_file)

    # check logging config file
    if not path.isfile(logging_conf_file):
        verbose_print('[Error] Logging config file "%s" not exists.' % (logging_conf_file))
        exit(1)

    with open(logging_conf_file, 'r') as f:
        config_logger = yaml.safe_load(f.read())
        logging.config.dictConfig(config_logger)

    logger = logging.getLogger(__name__)

    # conecting to Opencast server
    #opencast_object = Opencast(config_file)
    #indico_object = Indico(config_file)
    #cesi_obj = cesi.Cesi(args)

    # Dry-Run, test mode.
    if args.dry_run is True:
        # Simple arg checking
        logging.info(args.egroups)       
        if (args.acl):
            verbose_print('[INFO] (test-mode). acl "%s"' % (args.acl))
        else:
            verbose_print('[INFO] (test-mode)')
        return 0

    if args.application:
        #if (int(args.application)<2020):
        #    application="0000"
        #else:
        application=args.application
        secret = config.get('Oid', 'secret_%s' %(application))
        if args.delacl:
            my_oid = Oid(config_file, application, secret)
            #role_name = "role_%s" % (args.delacl)
            role_name = args.delacl
            # Role prefix: "role_"+role_name - first approach.
            role_id = my_oid.get_role_id(role_name, '')

            if args.delegroups and args.delegroups != "None":
                logging.info("Deleting egroups acl from a existing application, but not acl")
                # Convert comma separate values to list ["a","b","c"]
                egroups = [str(item) for item in args.delegroups.split(',')]
                for group in (egroups):
                    group_id = my_oid.get_group_id(group)
                    my_oid.create_role_group(role_id, group_id)
                    logging.info("Deleting egroup:%s from role of an existing application" %(group))
                    my_oid.delete_role_group(role_id, group_id)
            else:
                if (args.acl):
                    logging.info("Deleting acl before adding again. It renews all roles.")
                else:
                    logging.info("Deleting acl from a existing application.")
                logging.info("Deleting role %s from existing application %s" %(args.delacl, application))
                my_oid.delete_role(role_id)
        if args.acl:
            logging.info("Adding and acl to an existing application")
            if args.egroups:
                # Convert comma separate values to list ["a","b","c"]
                egroups = [str(item) for item in args.egroups.split(',')]
            else:
                egroups = ['webcast-support']
                logging.info("Warning: missing egroup. Defaulting to webcast-team")

            acl_add(config_file, application, args.acl, secret, egroups)
            #return add_acl_to_cluster(config_file, args.acl, args['clusters'][0],
            #                                args['egroups'])
        else:
            logging.error("Error: missing acl to add egroups or delete.")
    else:
        logging.error("Error: missing the application.")

    if args.cleanup:
        logging.info("Deleting all the roles and groups")
        for cluster in args['clusters']:
            secret = get_secret_from_teigi(cluster)
            my_oid = Oid(config_file, application, secret)
            for role in my_oid.get_roles()['data']:
                logging.info(role)
                my_oid.delete_role(role['id'])
        return 0

    #cesi_obj.check_acl_settings('.git/config', args, check_oid_roles)
    logging.info("Exit successfuly")

def log(myArgs):
   global logger
   logger.debug('Openid call: openid.py ' + myArgs)

if __name__ == "__main__":
#    sys.exit(main())
   main()
   log(' '.join(sys.argv[1:]))