#!/usr/bin/python3.6
import os
import inspect
import sys
import logging
import logging.config
import getopt
from argparse import ArgumentParser
from configparser import RawConfigParser
from xml.etree.ElementTree import parse
import json
from os import path, system
import datetime
import io
import ntpath
import shutil
from pkg_resources import ensure_directory
import yaml
from logstash_async.handler import AsynchronousLogstashHandler

namespaces = {'indico':'http://cern.ch/xsd/1.0/indico/','octerms':'http://cern.ch/oc/terms/'}

# add '../' to system path
CMD_FOLDER = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
CMD_SUBFOLDER = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], '../')))

if CMD_SUBFOLDER not in sys.path:
    sys.path.insert(0, CMD_SUBFOLDER)

from cern.opencast_class import Opencast
from cern.indico_class import Indico
from cern.ces_class import CES

_CONF_FILE = '../pycast.cfg'

def verbose_print(*args):
    # Print each argument separately so caller doesn't need to
    # stuff everything to be printed into a single string
    for arg in args:
        print(arg),
    print

def ensure_dir(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

def main():
    global logger
    parser = ArgumentParser(description='Indico args')
    parser.add_argument('-action', dest='action', required = True, 
                        help='Action to execute.')    
    parser.add_argument('-ev_id', dest='event_id', required = True, 
                        help='Opencast Event ID') 

    parser.add_argument('-data', action='store_true', dest='data', default=False,
                        help='Copy origin data files (data.json, .srt, ...). Gen data file (data.json file for player).')
    parser.add_argument('-conf_type', dest='conf_type', default='closed',
                        help='ACL rights ( open | closed (only opencast-admins) | ces ). ces is default.') 

    parser.add_argument('-asr', dest='asr', default='150',
                        help='ASR System to use.default = 150: CERN-Adapted English ASR System (Adapt-1L-id)')    
    parser.add_argument('-lang', dest='lang', default='en',
                        help='Language (en|fr|es)')
    parser.add_argument('-tlp_id', dest='tlp_id', default=None,
                        help='Translectures Event ID')                        

    parser.add_argument('-in_id', dest='indico_id', default=None,
                        help='Indico Event ID')
    parser.add_argument('-se_id', dest='series_id', default=None,
                        help='Series  ID')

    parser.add_argument('-in_ep', dest='indico_episode',
                        help='Indico episode catalog')
    parser.add_argument('-in_se', dest='indico_series',
                        help='Indico serie catalog')    

    parser.add_argument('-acl_json', dest='acl_json',
                        help='Indico ACL in JSON format. If -action==acl')
    parser.add_argument('-s', dest='new_state',
                        help='workflow state. If -action==state')
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='print all the results')
    parser.add_argument('-dr', action='store_true', dest='dry_run', default=False,
                        help='Only test mode. Not further processing.')

    parser.add_argument('-c', dest='config_file', action='store', default=_CONF_FILE,
                        help='configuration file')                    
    args = parser.parse_args()
    # check verbose
    if args.verbose is False:
            global verbose_print
            verbose_print = lambda *a: None      # do-nothing function

    # check config file
    config_file = args.config_file
    if not path.isfile(config_file):
            verbose_print('[ERROR] Config file: "%s" does not exists.' % (config_file))
            exit(1)

    # read logging configuration file path
    config = RawConfigParser()
    config.read(config_file)

    logging_conf_file = config.get('Logging', 'path')
    if path.isabs(logging_conf_file):
        logging_conf_file = path.abspath(logging_conf_file) 
    else:
        conf_base_dir = path.dirname(config_file)
        logging_conf_file = path.join(conf_base_dir, logging_conf_file)

    # check logging config file
    if not path.isfile(logging_conf_file):
        verbose_print('[Error] Logging config file "%s" not exists.' % (logging_conf_file))
        exit(1)

    with open(logging_conf_file, 'r') as f:
        config_logger = yaml.safe_load(f.read())
        logging.config.dictConfig(config_logger)

    logger = logging.getLogger(__name__)

    # conecting to Opencast server
    opencast_object = Opencast(config_file)
    indico_object = Indico(config_file)

    event_id = args.event_id
    # Dry-Run, test mode.
    if args.dry_run is True:
        # Simple arg checking
        logger.info(event_id)       
        if (event_id):
            verbose_print('[INFO] (test-mode). Video "%s"' % (event_id))
        else:
            verbose_print('[INFO] (test-mode)')
        return 0

    #################################################################################
    #####                                                                       #####
    #####                   Get indico_id   , contribution_id                   #####
    #####                                                                       #####
    #################################################################################

    series_id = args.series_id
    if (not(args.indico_id) or (args.indico_id == 'None')):
        indico_id = 'None'
        series_id = 'None'
        contribution_id = 'None'
        #########################################################################################################
        #####                                                                                               #####
        #####   Get indico_id from #{flavor(indico/episode)}: parsed indico-episode.xml file location       #####
        #####                                                                                               #####
        #########################################################################################################
        if (args.indico_episode):
            try:
                doc= parse(args.indico_episode)
                try:
                    root = doc.getroot()
                    indico_id = root.find('.//octerms:indico_id', namespaces).text
                    contribution_id = root.find('.//octerms:contribution_id', namespaces).text
                    series_id= root.find('.//octerms:series_id', namespaces).text
                    msg='Indico ID:'+indico_id+' Contribution ID:'+contribution_id+' & Series ID:'+ series_id +' through indico-episode.xml file provided by  #{flavor(indico/episode)}.'
                except:
                    logger.error('Can not parse indico_episode: #{flavor(indico/episode)}:'+args.indico_episode)
                    return            
            except:
                logger.error('File #{flavor(indico/episode)}:'+args.indico_episode+' not found')
                return
        #####################################################################################################################################
        #####                                                                                                                           #####
        #####   Get indico_id from parsed event #{id}: indico/episode metadata requested through the API: /api/events/#{id}/metadata    #####
        #####                                                                                                                           #####
        #####################################################################################################################################
        # 
        else:
            payload = {'_id':event_id, 'metadata_type':'indico/episode'}
            response = opencast_object.get_event_metadata(payload['_id'], payload['metadata_type'])
            if response['error'] is True:
                msg = 'Can not get event metadata %s: Error %s.' % (event_id, response['error-msg'])
                verbose_print('\t\t\t[ERROR] %s' % (msg))
                logger.error(msg)
            else:
                event = response['value']
                for dict in event:
                    if dict['id'] == 'indico_id':
                        indico_id=dict['value']
                    if dict['id'] == 'contribution_id':
                        contribution_id=dict['value']
                    if dict['id'] == 'series_id':
                        series_id=dict['value']                        
                msg='Indico ID:'+indico_id+' Contribution ID:'+contribution_id+' & Series ID:'+ series_id +' through provided event_id and /api/events/#{id}/metadata endpoint'
    
    #################################################################################
    #####                                                                       #####
    #####                    Indico Id arg provided                             #####
    #####                                                                       #####
    #################################################################################   
    else:
            indico_id = args.indico_id
            contribution_id = indico_id
            if (args.series_id):
                series_id = args.series_id
                msg='Indico ID:'+' Contribution ID:'+contribution_id+indico_id+' & Series ID:'+ series_id +' through provided indico_id & series_id args.'
            else:
                series_id = '000000'
                msg='Indico ID:'+indico_id+' Contribution ID:'+contribution_id+' through provided indico_id arg. Series_id not provided: default 000000.'
        
    #Unable to get indico_id: metadata file & opencast API & arg failed, 
    if(indico_id == None):
        logger.error('-indico_id arg not provided neither found through indico_episode.xml metadata file.')
        return 'Error: indico_id not found'
    else:
        logger.info(msg)

    #################################################################################
    #####                                                                       #####
    #####            Eport all files from Opencast System to CEPHs              #####
    #####                                                                       #####
    #################################################################################

    # Ready for exporting media and data files (ACL excluded)
    if (args.action == 'export'): 
        logger.info("-action %s parsed: export"%(args.action))
        payload = {'_id':event_id, 'with_publications':'True', 'with_metadata':'True'}
        response = opencast_object.get_event(payload['_id'], payload['with_publications'],payload['with_metadata'])

        #payload = {'id':event_id, 'json':'True', 'admin':'true', 'q':''}
        #response = opencast_object.get_episode(payload['id'], params=payload)
        if response['error'] is True:
            msg = 'Can not get event data %s: Error %s.' % (event_id, response['error-msg'])
            verbose_print('\t\t\t[ERROR] %s' % (msg))
            logger.error(msg)
            return(msg)
        else:
            msg = 'Event %s retrieved: %s.' % (event_id, response['error-msg'])
            logger.info(msg) 

            assets = opencast_object.get_asset(event_id)
            if assets['error'] is True:
                msg = 'Can not get event assets %s: Error %s.' % (event_id, assets['error-msg'])
                verbose_print('\t\t\t[ERROR] %s' % (msg))
                logger.error(msg)
                return(msg)
            else:
                msg = 'Assets %s retrieved: %s.' % (event_id, assets['error-msg'])
                logger.info(msg)
                #verbose_print('\t\t\t%s' % (msg))
                # Find resources:
                #event = json.loads(response['value'])
                event = response['value']
                asset = assets['value']
                year=indico_object.get_year(indico_object.get_metadata(event, 'dublincore', 'startDate'))
                media_dir = config.get('Media', 'dest_dir')+'/'+year
                ensure_dir(media_dir)
                ensure_dir(os.path.join(media_dir,'config'))                
                if (args.action == 'export'):
                    response = indico_object.export_json(config_file, contribution_id, event, asset, args.data)

                if response['error'] is True:
                    msg = 'Can not %s data for event:%s  Error %s.' % (args.action, event_id, response['error-msg'])
                    logger.error(msg)
                else:
                    msg = 'Event %s %sed: %s.' % (args.action, event_id, response['error-msg'])
                    logger.info(msg)
                    logger.info("Actions:%s\n"%(response['value']))

    elif (args.action == 'notify'):
        logging.info("Notification of Opencast action: " + args.new_state + " to CES")
        verbose_print("Notification of Opencast action: " + args.new_state + " to CES")
        ces_object = CES(config_file)
        response = ces_object.set_state(contribution_id, args.new_state, headers='yes')
        if response['error'] is True:
            msg = 'Can not notify CES %s: Error %s.' % (contribution_id, response['error-msg'])
            logging.error(msg)
        else:
            msg = 'CES has been notified %s: Msg %s.' % (contribution_id, response['value'])
            logging.info(msg) 

    elif (args.action == 'master')or (args.action == 'upload'):
        payload = {'_id':event_id, 'with_publications':'True', 'with_metadata':'True'}
        response = opencast_object.get_event(payload['_id'], payload['with_publications'],payload['with_metadata'])

        if response['error'] is True:
            msg = 'Can not get event data %s: Error %s.' % (event_id, response['error-msg'])
            verbose_print('\t\t\t[ERROR] %s' % (msg))
            logger.error(msg)
            return(msg)
        else:
            msg = 'Event %s retrieved: %s.' % (event_id, response['error-msg'])
            logger.info(msg) 
            event = response['value']
            title = indico_object.get_metadata(event, 'dublincore', 'title')
            contribution_id=indico_object.get_metadata(event, 'indico', 'contribution_id')
            year=indico_object.get_year(indico_object.get_metadata(event, 'dublincore', 'startDate'))
            dest_dir = config.get('Indico', 'master_dir')+'/'+year+'/'+contribution_id
            master_dir = config.get('Indico', 'master_dir')+'/'+year
            ensure_dir(master_dir)
            # Get the video resourcer through the asset aapi: /api/assets/episode/{event_id} API call
            # where type='presentation/source'",
            payload = {'_id':event_id}
            response2 = opencast_object.get_asset(payload['_id'])

            if response2['error'] is True:
                msg = 'Can not get event asset %s: Error %s.' % (event_id, response2['error-msg'])
                verbose_print('\t\t\t[ERROR] %s' % (msg))
                logger.error(msg)
                return(msg)
            else:
                msg = 'Event %s retrieved: %s.' % (event_id, response2['error-msg'])
                logger.info(msg) 
                asset = response2['value']
            org_file = 'Get_from_Opencast.mp4'
            # Get the master media file
            flavors = ['presentation','presenter']
            # try to get the tagged-trimmed first
            # subflavor='source'
            for flavor in flavors:
                subflavor='tagged-trimmed'
                error=False
                media_trimmed = indico_object.get_media(asset, flavor, subflavor, 'url')
                if media_trimmed['error'] is True:
                    msg = 'Event: %s. Can not get %s/%s asset data %s: Trying the source.' % (event_id,flavor,subflavor, media_trimmed['error-msg'])
                    logger.info(msg)
                    subflavor='tagged-source'
                    media_source = indico_object.get_media(asset, flavor, subflavor, 'url')
                    if media_source['error'] is True:
                        msg = 'Event: %s. Can not get %s/%s asset data %s: No trimmed or source file saved for %s.' % (event_id, flavor,subflavor, media_source['error-msg'], flavor)
                        logger.info(msg)
                        error=True
                    else:
                        url = media_source['value']
                                        
                else:
                    url = media_trimmed['value']
                if error==False:     
                    msg = 'Media %s retrieved from %s/%s: %s.' % (event_id, flavor, subflavor, url)
                    logger.info(msg)
                    head, tail = ntpath.split(url)
                    file=os.path.splitext(tail)[0]
                    extension=os.path.splitext(tail)[1]        
                    dest_file = flavor+extension
                    if (args.action == 'master'):
                        # Send the trimmed/source file to the master filesystem
                        response = opencast_object.get_file(url,filedir=dest_dir, filename=dest_file, fullpath=True)
                        if response['error'] is True:
                            msg = 'Can not download source file %s.' % (url)
                            logger.error(msg)
                            return # {'value' : msg, 'error' : True, 'error-msg' : contribution_id+':'+title}
                        else:
                            msg = "File copied:%s/%s"%(dest_dir,dest_file)
                            logger.info(msg)
                    elif (args.action == 'upload'):
                        # Send the trimmed/source file to the external URI
                        dest_file = "%s-%s-%s%s"%(year,contribution_id,flavor,extension)
                        response = opencast_object.get_file(url,filedir="/tmp", filename=dest_file, fullpath=True)
                        if response['error'] is True:
                            msg = 'Can not download source file %s.' % (url)
                            logger.error(msg)
                            return # {'value' : msg, 'error' : True, 'error-msg' : contribution_id+':'+title}
                        else:
                            msg = "File copied:%s/%s"%("/tmp",dest_file)
                            logger.info(msg)                        
                            cmd="media.py -c /etc/pycast/pycast.cfg -action upload -service Upload -org_dir %s/%s -v"%("/tmp",dest_file)
                            system(cmd)
                            msg = "File %s/%s uploaded to Service:%s"%("/tmp",dest_file,'Upload')
                            logger.info(msg)
            if (args.action == 'master'):
                # Get the extracted wav audio file
                media = indico_object.get_media(asset, 'audio', 'tagged-wav', 'url','')
                if media['error'] is True:
                    msg = 'Can not get audio/wav asset data %s: Error %s.' % (event_id, media['error-msg'])
                    logger.info(msg)
                    # Try with presenter
                else:
                    url = media['value']
                    msg = 'Media %s retrieved from %s/%s: %s.' % (event_id, flavor, subflavor, url)
                    logger.info(msg)
                    head, tail = ntpath.split(url)
                    file=os.path.splitext(tail)[0]
                    extension=os.path.splitext(tail)[1]        
                    dest_file = flavor+extension
                    response = opencast_object.get_file(url,filedir=dest_dir, filename=dest_file, fullpath=True)
                    if response['error'] is True:
                        msg = 'Can not download source file %s.' % (url)
                        logger.error(msg)
                        return # {'value' : msg, 'error' : True, 'error-msg' : contribution_id+':'+title}
                    else:
                        msg = "File copied:%s"%(dest_file)
                        logger.info(msg)            

    elif (args.action == 'aclOrg'):
            logger.info("-action aclOrg arg parsed:%s"%(args.action))
            #cmd="media.py -c %s -action transfer %s -dest_dir %s/%s -v -mlp"%(config_file,org_arg,args.dest_dir,info[2])
            #system(cmd)         
    #################################################################################
    #####                                                                       #####
    #####       Create ACL file (Apache .conf) for the media webserver          #####
    #####                                                                       #####
    #################################################################################

    # ACL management through openid and indico objects.
    # ACL exporting (media and data files excluded).
    elif (args.action == 'acl'):  
        # ACL JSON parsed. Get indico_id and year from event_id
        if (args.acl_json):
            with open(args.acl_json) as f:
                data = json.load(f)
            logging.info(data)
            logging.error("Get ACL from attached JSON file.")
            # Get groups from data json file and convet to comma separate value
            # Get event year from event json
            # Call openid.py with expected paramas: -oid_app, -oid_acl_add, -oid_eg_add
        else:
            logging.info("Get Event metadata from Opencast API REST.  Call media.py to generate the config file.")
            # Get Event Metadata from Opencast Event REST Services
            payload = {'_id':event_id, 'with_publications':'False', 'with_metadata':'True'}
            response = opencast_object.get_event(payload['_id'],payload['with_publications'], payload['with_metadata'])
            if response['error'] is True:
                msg = 'Can not get event metadata %s: Error %s.' % (event_id, response['error-msg'])
                verbose_print('\t\t\t[ERROR] %s' % (msg))
                logger.error(msg)
                return(msg)
            else:
                msg = 'Event %s retrieved: %s.' % (event_id, response['error-msg'])
                logger.info(msg) 
                event = response['value']
                title = indico_object.get_metadata(event, 'dublincore', 'title')
                #print(event['title'])
                #print(title)
                #print(indico_object.get_metadata(event, 'indico', 'indico_id'))
                contribution_id=indico_object.get_metadata(event, 'indico', 'contribution_id')
                year=indico_object.get_year(indico_object.get_metadata(event, 'dublincore', 'startDate'))
                logging.info("Contribution:%s-Year:%s"%(contribution_id,year))
                # media.py -c /etc/pycast/pycast.cfg -action acl -dest_dir 2020/837872c17
                dest_dir = config.get('Media', 'dest_dir')+'/'+year+'/'+contribution_id
                conf_dir = config.get('Media', 'dest_dir')+'/'+year+'/config'
                cmd="media.py -c /etc/pycast/pycast.cfg -action acl -dest_dir %s -v -mlp -s %s -conf_dir %s -conf_type %s"%(dest_dir, args.new_state, conf_dir, args.conf_type)
                system(cmd)


    #################################################################################
    #####                                                                       #####
    #####      Export all files to CEPHs for legacy reasons                     #####
    #####                                                                       #####
    #################################################################################

    elif (args.action == 'legacy'):  
        logging.info("Get Event metadata from Opencast API REST.  Call media.py to export al the media files.")
        # Get Event Metadata from Opencast Event REST Services
        payload = {'_id':event_id, 'with_publications':'False', 'with_metadata':'True'}
        response = opencast_object.get_event(payload['_id'],payload['with_publications'], payload['with_metadata'])
        if response['error'] is True:
            msg = 'Can not get event metadata %s: Error %s.' % (event_id, response['error-msg'])
            verbose_print('\t\t\t[ERROR] %s' % (msg))
            logger.error(msg)
            return(msg)
        else:
            msg = 'Event %s retrieved: %s.' % (event_id, response['error-msg'])
            logger.info(msg) 
            event = response['value']
            title = indico_object.get_metadata(event, 'dublincore', 'title')
            #print(event['title'])
            #print(title)
            #print(indico_object.get_metadata(event, 'indico', 'indico_id'))
            contribution_id=indico_object.get_metadata(event, 'indico', 'contribution_id')
            # Event Type:   It is possible to get ut from the Indico metadata, but also from param
            event_type=indico_object.get_metadata(event, 'indico', 'event_type')
            verbose_print('Event type from metadata = %s  '%(event_type))
            # Translate the event_type to a folder:
            result=indico_object.get_legacy_dir(event_type)
            legacy_dir=result['value']
            verbose_print(result['error-msg'])
            logger.info(result['error-msg'])
            if (legacy_dir!='None'):
                year=indico_object.get_year(indico_object.get_metadata(event, 'dublincore', 'startDate'))
                logging.info("eventType:%s-contribution_id:%s-year:%s"%(event_type,contribution_id,year))
                # media.py -c /etc/pycast/pycast.cfg -action legacy -dest_dir event_type_dir/2020/837872c17
                # The org_dir of the legacy is the dest_dir of the 'regular' CEPHfs Media export
                org_dir = config.get('Media', 'dest_dir')+'/'+year+'/'+contribution_id
                dest_dir = config.get('Media', 'legacy_path')+'/'+legacy_dir+'/'+year+'/'+ contribution_id # To complete with event_type (dir) before year
                cmd="media.py -c /etc/pycast/pycast.cfg -action legacy -org_dir %s -dest_dir %s -v -s %s"%(org_dir, dest_dir, args.new_state)
                verbose_print(cmd)
                system(cmd)


    #################################################################################
    #####                                                                       #####
    #####                                                                       #####
    #####                                                                       #####
    #################################################################################

    elif (args.action == 'mllp'):
        logger.info("-action parsed: mllp")
        payload = {'_id':event_id, 'with_publications':'True', 'with_metadata':'True'}
        response = opencast_object.get_event(payload['_id'], payload['with_publications'],payload['with_metadata'])

        if response['error'] is True:
            msg = 'Can not get event data %s: Error %s.' % (event_id, response['error-msg'])
            verbose_print('\t\t\t[ERROR] %s' % (msg))
            logger.error(msg)
            return(msg)
        else:
            msg = 'Event %s retrieved: %s.' % (event_id, response['error-msg'])
            logger.info(msg) 
            event = response['value']
            title = indico_object.get_metadata(event, 'dublincore', 'title')
            contribution_id=indico_object.get_metadata(event, 'indico', 'contribution_id')
            year=indico_object.get_year(indico_object.get_metadata(event, 'dublincore', 'startDate'))
            org_dir = config.get('Media', 'dest_dir')+'/'+year+'/'+contribution_id

            # Set the language
            if (args.lang and args.lang!='undefined'):
                language=args.lang
                asr = args.asr
            else:
                language = indico_object.get_metadata(event, 'dublincore', 'language')
                if (language=='eng'):
                    asr='150'
                elif language=='fra':
                    asr='103'
                elif language=='deu':
                    asr='111'
                else:
                    asr='150'

            # Get the video resourcer through the asset aapi: /api/assets/episode/{event_id} API call
            # where type='presentation/source'",
            # If not -asr parsed, get it from the Event metadata "language"            
            payload = {'_id':event_id}
            response2 = opencast_object.get_asset(payload['_id'])

            if response2['error'] is True:
                msg = 'Can not get event asset %s: Error %s.' % (event_id, response2['error-msg'])
                verbose_print('\t\t\t[ERROR] %s' % (msg))
                logger.error(msg)
                return(msg)
            else:
                msg = 'Event %s retrieved: %s.' % (event_id, response2['error-msg'])
                logger.info(msg) 
                asset = response2['value']
            org_file = 'Get_from_Opencast.mp4'
            # First try to get the video from the already published event: presentation/delivery 1080p quality video file            
            # <tag>1080p-quality</tag>

            flavor='presentation'
            subflavor='delivery'
            media = indico_object.get_media(asset, flavor, subflavor, 'url','1080p-quality')
            if media['error'] is True:
                msg = 'Can not get presentation/delivery asset data %s: Error %s.' % (event_id, media['error-msg'])
                logger.info(msg)
                # Try with presenter
                flavor='presenter'
                media = indico_object.get_media(asset, flavor, subflavor, 'url','1080p-quality')
                if media['error'] is True:
                    msg = 'Can not get presenter/delivery asset data %s: Error %s.' % (event_id, media['error-msg'])
                    logger.info(msg)
                    # Try with source
                    flavor='presentation'
                    subflavor='source'                   
                    media = indico_object.get_media(asset, flavor, subflavor, 'url')
                    if media['error'] is True:
                        msg = 'Can not get presentation/source asset data %s: Error %s.' % (event_id, media['error-msg'])
                        logger.info(msg)
                        flavor='presenter'
                        media = indico_object.get_media(asset, flavor, subflavor, 'url')
                        if media['error'] is True:
                            msg = 'Can not get presenter/source asset data %s: Error %s.' % (event_id, media['error-msg'])
                            logger.info(msg)
            if media['error'] is True:
                logger.error('All attempts to get a video failed.')
                return
            else:
                url = media['value']
                msg = 'Media %s retrieved from %s/%s: %s.' % (event_id, flavor, subflavor, url)
                logger.info(msg)
            
            # Download the file to temp folder
            dest_dir = '/tmp'
            head, tail = ntpath.split(url)
            file=os.path.splitext(tail)[0]
            extension=os.path.splitext(tail)[1]        
            dest_file = contribution_id+extension
            org_file = dest_dir+'/'+dest_file
            response = opencast_object.get_file(url,filedir=dest_dir, filename=dest_file, fullpath=True)
            if response['error'] is True:
                msg = 'Can not download source file %s: Error %s.' % (url, org_file)
                logger.error(msg)
                return # {'value' : msg, 'error' : True, 'error-msg' : contribution_id+':'+title}
            else:
                msg = "File copied:%s"%(org_file)
                logger.info(msg)

            title = 'Opencast-'+year+'-'+contribution_id
            verbose_print(title)
            cmd="mllp.py -c /etc/pycast/pycast.cfg -action ingest -lang %s -asr %s -year %s -tlp_id %s -org_dir %s -org_file %s -title %s"%(language, asr, year, contribution_id, dest_dir, dest_file, title)
            verbose_print(cmd)
            system(cmd)

    #################################################################################
    #####                                                                       #####
    #####                                                                       #####
    #####                                                                       #####
    #################################################################################

    elif (args.action == 'trs'):
        logger.info("-action parsed: trs")
        payload = {'_id':event_id, 'with_publications':'True', 'with_metadata':'True'}
        response = opencast_object.get_event(payload['_id'], payload['with_publications'],payload['with_metadata'])

        if response['error'] is True:
            msg = 'Can not get event data %s: Error %s.' % (event_id, response['error-msg'])
            verbose_print('\t\t\t[ERROR] %s' % (msg))
            logger.error(msg)
            return(msg)
        else:
            msg = 'Event %s retrieved: %s.' % (event_id, response['error-msg'])
            logger.info(msg) 
            event = response['value']
            title = indico_object.get_metadata(event, 'dublincore', 'title')
            contribution_id=indico_object.get_metadata(event, 'indico', 'contribution_id')
            year=indico_object.get_year(indico_object.get_metadata(event, 'dublincore', 'startDate'))
            dest_dir = config.get('Media', 'dest_dir')+'/'+year+'/'+contribution_id
            title = 'Opencast-'+year+'-'+contribution_id
            if (args.tlp_id):
                tlp_id = args.tlp_id
            else:  
                tlp_id = contribution_id
            cmd="mllp.py -c /etc/pycast/pycast.cfg -action trs -year %s -tlp_id %s -contribution_id %s -dest_dir %s"%(year, tlp_id, contribution_id, dest_dir)
            logger.info(cmd)
            system(cmd)
            ces_object = CES(config_file)
            response = ces_object.set_state(contribution_id, args.new_state, headers='yes')
            if response['error'] is True:
                msg = 'Can not notify CES %s: Error %s.' % (contribution_id, response['error-msg'])
                logging.error(msg)
            else:
                msg = 'CES has been notified %s: Msg %s.' % (contribution_id, response['value'])
                logging.info(msg)  
                
    #################################################################################
    #####                                                                       #####
    #####                                                                       #####
    #####                                                                       #####
    #################################################################################

    # Create Indico Series from indico_series file-path param
    series_id = None
    if (args.indico_series):
        doc= parse(args.indico_series)
        root = doc.getroot()
        series_id = root.find('.//octerms:indico_id', namespaces).text
        title= root.find('.//octerms:title', namespaces).text
        verbose_print('Serie:'+series_id+':'+title)

        # Create the serie

        # Apply series ACL
        #if (args.acl_json):
        #    with open(args.acl_json) as f:
        #        data = json.load(f)
        #        #print(data)
        #        # Update Series ACL
        #        update = {'_id':event_id, 'data':data}
        #        response = opencast_object.udpate_event_acl(update['_id'], data)
        #        response['error-msg'] = 'test'
        #        if response['error'] is True:
        #                msg = 'Can not update indico %s ACL : event_id %s: Error %s.' % (series_id, event_id, response['error-msg'])
        #                verbose_print('\t\t\t[ERROR] %s' % (msg))
        #                logger.error(msg)
        #        else:
        #                msg = 'Updated indico %s ACL : event_id %s. %s.' % (series_id, event_id, response['error-msg'])
        #                verbose_print('\t\t\t%s' % (msg))
        #                logger.info(msg)

def log(myArgs):
   global logger
   logger.debug('Opencast call to: indico.py ' + myArgs)

if __name__ == "__main__":
   main()
   log(' '.join(sys.argv[1:]))

