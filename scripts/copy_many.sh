#!/bin/bash

# Copy files:
#   copy_many.sh --org=/mnt/import_share/2001 --dst=/mnt/media_share/media_data/2001 --id=412113c1 --max=2
# Avoid copy:
#   copy_many.sh --org=/mnt/import_share/2020 --dst=/mnt/media_share/media_data/2020 --max=10
# Create roles in Open_Id app:
#   copy_many.sh --org=/mnt/import_share/2020 --dst=/mnt/media_share/media_data/2020 --app=apo --max=10
#   copy_many.sh --org=/mnt/import_share/2019 --dst=/mnt/media_share/media_data/2019 --max=2000 --app=0000



id="0"
max=2000
org="0"
dst="0"
copy="0"
app="0"
mlp="0"
acl="0"

function usage()
{
    echo "Copy media resources; generate apache .conf file and data.json"
    echo ""
    echo "./copy_many.sh"
    echo "--id=$id"
    echo "--max=$max"
    echo "--org=$org"
    echo "--dst=$dst"
    echo "--copy=$copy"
    echo "--app=$app"
    echo "--acl=$acl"    
    echo "--mlp=$mlp"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --id)
            id=$VALUE
            ;;
        --max)
            max=$VALUE
            ;;            
        --org)
            org=$VALUE
            ;;
        --dst)
            dst=$VALUE
            ;;
        --copy)
            copy=$VALUE
            ;;
        --app)
            app=$VALUE
            ;;
        --acl)
            acl=$VALUE
            ;;            
        --mlp)
            mlp=$VALUE
            ;;           
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

echo "ID is $id";
echo "MAX is $max";
echo "ORG is $org";
echo "DEST is $dst";
echo "COPY is $copy";
echo "APP is $app";
echo "ACL is $acl";
echo "MLP is $mlp";

#while [ "$n" -le "$num" ]; do

if [ "$app" == "0" ]; then
  app_arg=""
else
  app_arg="-app $app"
fi

if [ "$mlp" == "0" ]; then
  mlp_arg=""
else
  mlp_arg="-mlp"
fi

n=0
if [ "$org" == "0" ]; then
  dir=$dst
else
  dir=$org
fi

if [ "$id" == "0" ]; then
  for entry in `ls $dir`; do
    if [ "$n" -gt "$max" ]; then
      break
    fi
    if [ "$org" == "0" ]; then
      org_arg=" -data"
    else
      if [ "$copy" == "0" ]; then
        org_arg="-org_dir $org/$entry"
      else
        org_arg="-org_dir $org/$entry -data"
      fi
    fi
    media.py -c /etc/pycast/pycast.cfg  -action transfer $org_arg -dest_dir $dst/$entry -v -conf_dir $dst/config $app_arg $mlp_arg
    if [ "$acl" -ne "0" ]; then
      media.py -c /etc/pycast/pycast.cfg  -action acl -conf_type ces $org_arg -dest_dir $dst/$entry -v -conf_dir $dst/config $app_arg $mlp_arg
    fi    
    n=`expr "$n" + 1`;
    echo "$n:$entry"
  done
else
    if [ "$org" == "0" ]; then
      org_arg=" -data"
    else
      if [ "$copy" == "0" ]; then
        org_arg="-org_dir $org/$id"
      else
        org_arg="-org_dir $org/$id -data"
      fi
    fi
    media.py -c /etc/pycast/pycast.cfg  -action transfer $org_arg -dest_dir $dst/$id -v -conf_dir $dst/config $app_arg $mlp_arg
    if [ "$acl" -ne "0" ]; then
      media.py -c /etc/pycast/pycast.cfg  -action acl -conf_type ces $org_arg -dest_dir $dst/$id -v -conf_dir $dst/config $app_arg $mlp_arg
    fi
fi
#done
