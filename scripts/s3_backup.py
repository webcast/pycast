#!/usr/bin/python

import errno
import json
import os
import subprocess
import sys
import time
from datetime import datetime


import click
import requests


CREDENTIALS_FILE = '/root/.config/rclone/rclone.conf'
LOG_FILEPATH = '/mnt/opencast_share/opencast_logs'
#CREDENTIALS_FILE = '/Users/rgaspar/.config/rclone/rclone.conf'
BACKUP_TARGET_S3 = 's3'


def rclone(local_filesystem, backup_target, bucket, verbose, dry_run, filetolog):
    target_dir = backup_target[1] + bucket + local_filesystem
    if verbose:
        click.echo('    backing up to {}'.format(target_dir))
    args = ['rclone', 'copy', local_filesystem, target_dir]
    args.append('--fast-list')
    if backup_target[0] == BACKUP_TARGET_S3:
        args += [
            '--checksum',
            '--s3-upload-cutoff', '1G',
            '--s3-chunk-size', '256M',
            '--transfers', '2',
            '--checkers', '4'
        ]
    if verbose > 1:
        args += ['-v'] * (verbose - 1)
        # enable progress display only while on a tty; if we write to a file or
        # cronjob output it's extremely spammy
        if sys.stdout.isatty():
            args.append('-P')
    if dry_run:
        args.append('-n')
    if filetolog:
        filename = local_filesystem.replace('/','')
        filename += '_' + datetime.now().strftime('%y-%m-%dP%H-%M') + '.log'
        if not os.path.exists(LOG_FILEPATH):
            filename = os.path.join('/tmp', filename)
        else:
            filename = os.path.join(LOG_FILEPATH, filename) 
        args.append('--log-file={}'.format(filename))
    try:
        subprocess.check_call(args)
    except subprocess.CalledProcessError:
        click.echo('    WARNING: rclone exited with non-zero status code')


class BackupTargetType(click.ParamType):
    name = 'target'

    def convert(self, value, param, ctx):
        try:
            fstype, path = value.split(':', 1)
        except ValueError:
            self.fail('backup target must have the format type:path', param, ctx)
        if fstype == 's3':
            if ':' in path:
                self.fail('rclone remote must not contain a colon', param, ctx)
            remote = path + ':'
            remotes = subprocess.check_output(['rclone', 'listremotes'], encoding='UTF-8').splitlines()
            if remote not in remotes:
                self.fail('rclone remote does not exist', param, ctx)
            return BACKUP_TARGET_S3, remote
        else:
            self.fail('path type must be s3', param, ctx)


@click.command()
@click.argument('backup-target', type=BackupTargetType())
@click.option('-l', '--local_filesystem', required=True, help='Local content directory to backup')
@click.option('-b', '--bucket', type=str, required=True, help='Destination bucket in remote')
@click.option('-v', '--verbose', count=True, help='Enable verbose output (repeat for higher verbosity)')
@click.option('-q', '--quiet', is_flag=True, help='Disable generally-useless output (regardless of verbosity)')
@click.option('-n', '--dry_run', is_flag=True, help='Do not actually copy files')
@click.option('-s', '--subdirectories', is_flag=True, help='Copy directory by directory and only directories')
@click.option('-f', '--filetolog', is_flag=True, help='It generates a log file located at /mnt/opencast_share/opencast_logs')
def main(backup_target, local_filesystem, bucket, verbose, quiet, dry_run, subdirectories, filetolog):
    """This script copies data from S3 to a rclone remote.

    """

    start = time.time()
    if not os.path.exists(CREDENTIALS_FILE):
        click.echo(f'{CREDENTIALS_FILE} does not exist, we cant proceed!')
        return
    if not os.path.exists(local_filesystem):
        click.echo(f'{local_filesystem} does not exist, we cant proceed!')
        return
    if verbose and not quiet:
        click.echo('processing filesystem from argument list')
    if subdirectories:
        list_subfolders_with_paths = [f.path for f in os.scandir(local_filesystem) if f.is_dir()]
        for directory in list_subfolders_with_paths:
            rclone(directory, backup_target, bucket, verbose, dry_run, filetolog)
    else:
        rclone(local_filesystem, backup_target, bucket, verbose, dry_run, filetolog)
    if verbose or not quiet:
        duration = time.time() - start
        click.echo('finished in {:.02f}s'.format(duration))


if __name__ == '__main__':
    main()
