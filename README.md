# PyCast [![License](https://img.shields.io/github/license/pycast/pycast.svg)](https://gitlab.cern.ch/webcast/pycast/blob/master/LICENSE.txt) [![Available on PyPI](https://img.shields.io/badge/pypi-v0.0.1-orange)](https://pypi.python.org/pypi/pycast/) [![Made at CERN!](https://img.shields.io/badge/CERN-Open%20Source-%232980b9.svg)](https://home.cern)

## What does Pycast do?
Pycast's **main features** are: Opencast Tools for Webcast.

Python Versions
---------------

PyCast requires Python ≥ 3.6. Older versions of Python will not work.

Documentation
-------------

For a detailed installation guide, take a look at the `PyCast documentation`.

Quick Install
-------------


```

    pip3 install git+https://gitlab.cern.ch/webcast/pycast.git@version
    Or
    git clone https://gitlab.cern.ch/webcast/pycast /etc/pycast
    cd /etc/pycast
    pip3 install -r requirements.txt
    vi /etc/pycast/pycast.cfg <-- Edit the configuration
```
Update
-------------


Opencast: https://opencast.org
PyCast documentation: https://gitlab.cern.ch/webcast/pycast/tree/master/README.md