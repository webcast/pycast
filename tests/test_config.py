# -*- coding: utf-8 -*-

'''
Tests for PyCast configuration
'''

import unittest

from pycast import config


class TestPyCastConfig(unittest.TestCase):

    def test_check(self):
        config.config()['server']['insecure'] = True
        config.config()['server']['certificate'] = '/xxx'
        with self.assertRaises(IOError):
            config.check()