# -*- coding: utf-8 -*-

'''
Tests for pycast
'''

import unittest
import tempfile
import os

from pycast import config


class TestPyCast(unittest.TestCase):

    dbfile = None

    def setUp(self):
        cfg = './etc/pycast.cfg'
        config.update_configuration(cfg)

        self.fd, self.dbfile = tempfile.mkstemp()
        config.config()['PyCast']['database'] = 'sqlite:///' + self.dbfile

    def tearDown(self):
        os.close(self.fd)
        os.remove(self.dbfile)


